# abstraction-and-reasoning-challenge

## Brief explanation
I implemented a DSL based on "operation" on images.  
Next, I implemented a search engine that searches for the correct DSL combination for a task.  
The following is a brief description of the DSL and the search engine.

## DSL structure
- OperationSet
  - This is a list of UniformOperation, ColorOperation and MultiColorChannelOperation.
  - e.g.
    ```
    OperationSet(operations=[ColorOperation(color_selection=FixedSingleColorSelection(color=Color.GREEN), mask_conversions=HolesSelection(connectivity=PixelConnectivity.FOUR_DIRECTION), mask_operation=FixedColorMaskFill(color=Color.YELLOW))]))
    ```
    <img src="./example_image/arc_example.png" width="320">
- A: UniformOperation
  - Operations resulting in a uniform change throughout the image (independent on pixels).
  - e.g. Padding, Resize, Flip, Rotate.
- B: ColorOperation
  - This operation consists of ColorSelection, MaskConversion and MaskOperation.
  - B1: ColorSelection
    - Extract color(s) and recognize it as a one mask.
    - e.g. FixedSingleColorSelection, SingleColorSelection, MultiColorSelection.
  - B2: MaskConversion
    - Convert the result of ColorSelection.
    - e.g. SquareObjectsSelection, HolesSelection, ContourSelection,,,(too many).
  - B3: MaskOperation
    - An operation that actually applies to an image.
    - e.g. FixedColorMaskFill, SingleColorMaskFill, MaskCoordsCrop.
- C: MultiColorChannelOperation
  - This operation consists of ColorChannelSelection, MaskConversion and ChannelMergeOperation.
  - The range of tasks that could be solved with this operation was few against my expectation.
  - C1: ColorChannelSelection
    - Extract colors and recognize it as a color_mask_pairs.
    - e.g. WithOutMostCommonColorChannelSelection.
  - C2 MaskConversion
    - Same as B2.
  - C3: ChannelMergeOperation
    - Merge color_mask_pairs into one image.
    - e.g. ColorChannelOverrideOperation.
- D: PartitionOperation
  - This operation consists of ColorChannelSelection, MaskConversion and ChannelMergeOperation.
  - D1: PartitionSelection
    - Divide a single image into multiple images.
    - e.g. LinePartition, IntegerDivisionPartition.
  - D2: PartitionMergeOperation
    - Merge multiple images into a single image.
- DSL executor
  - see OperationSetExecutor, TaskOperationSetExecutor.

## Search Engine(OperationSet solver)
- I develop two kind of search engine which finds the answer `OperationSet` which satisfies the given task.
- A: NodeBaseSearchEngine
  - The strategy is like combination of beam search and A*.
  - The rough strategy is described below.
  - 1: Apply UniformOperation, ColorOperation or MultiColorChannelOperation to original_task and get many applied_new_tasks.
  - 2: Evaluate applied_new_tasks in a certain evaluation function(described below).
    - Evaluation function includes the penalty related to the length(depth) of `OperationSet`. This is what I meant by `A*`.
  - 3: Recognize improved applied_new_tasks as new task.
    - Actually, top n applied_new_tasks are recognized as applied_new_tasks. This is what I meant by `beam search`.
  - 4: Repeat 1-3 using the priority queue.
- B: TreeBaseSearchEngine
  - This is yet not so good.
  - Trying to implement Genetic Programming.
  - Implement mutation, but not yet crossover.
  - Simple random mutation tends to local minimum.

## Evaluation Function
- A: TaskFeature
  - Extract the features of task.
- B: DistanceEvaluator
  - This is the base evaluation function. Smaller is better.
- C: HandMadeNodeEvaluator
  - This is the final evaluation function.
  - Impose penalty to the result of DistanceEvaluator.
- D: NodeEvaluatorSchedules
  - This is a little twist to avoid falling into a local minimum solution.
    - I guess human beings are doing similar.
  - For the first 20 sec, penalty is very big. This is like BreadthFirstSearch.
  - For the next 20 sec, penalty is moderate. This is like BestFirstSearch.
  - For the final 20 sec, penalty is small. This is like DepthFirstSearch.

## How to choose 3 answer
- Very dirty source code...
- Some bugs found already.
- There has to be an easier, better way.
  - e.g. Mutate answer_operation_set at one point?
1. NodeBaseSearchEngine try to search answer using cache to save time(visited_node_hashes).
1. The engine finds answer.
1. Take out the node_trees in the cache.
1. Find node_trees which have same train-arrays as answer, but different test-arrays as answer.
1. Found node_trees can be recognize alternative answer.

## How to run locally
- Clone git.
- Put dataset into "abstraction_and_reasoning_challenge/input".
- Run "abstraction_and_reasoning_challenge/run.py".

## How to run kaggle kernel
- kernel_generator.py
- This script collects entire source codes and generate kaggle_kernel_runnable source code.
- Currently, this might not work due to local import...
- 1. Run kernel_generator.py
  2. comment out local import. "from abstraction_and_reasoning_challenge ~~ "
  3. comment out "from ruamel import yaml"

## Configs / Parameters
- config.py
  - RunConfig is very important.
- param_config.py

## Answer memo
- answer_summary.txt
  - The program writes result_log for me.
  - `correct_count: Counter({None: 278, True: 105, False: 17})`
    - This means:
      - Answer and the result was correct: 105
      - Answer but the result was wrong: 17
      - Can not Answer: 278
- answer_taxonomy.csv
  - Under development.
  - Categorize train tasks and define `GIVE_UP_TASKS` to save time.
  - This csv is maneged by `TaskTaxonomy` or manually.
- answer_storage.txt
  - Store the list of answer_dsl.
  - Maybe we can use this for ml approach?

