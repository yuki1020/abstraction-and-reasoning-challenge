# abstraction-and-reasoning-challenge

## High priority TODOs
- develop more efficient search engine

## DSL TODOs
- rectangular selection
- find useful function in the libraries.
  - opencv, numpy, skimage
- more OperationInconsistencyException?
- https://www.kaggle.com/paulorzp/20-tasks-tiles-and-symmetry
- bg color function from anastasia?

## Search Engine TODOs
- More efficient pruning
- Restrict max depth?
- Continue searching until find 3 answers.
- How to decide 3 answers if more than 3 answers found?

## Evaluation Function TODOs
- find more effective task_feature
  - cosine similarity?
  - hit and miss?
- hit and miss transformation histogram depends strongly on background color.
  - remove it?

## Application TODOs
- put test output submission under git version control?
- Check update dataset?
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/131021

## Performance TODOs
- Hit and miss transformation is too slow. It spends about half of the execution time!
  - try skimage instead of cv2?
- Try numba.

## Idea / Keywords
- program synthesis
- inductive programming
- genetic programing
- multi-purpose optimization genetic programing
- pareto genetic programing
- heuristic way to escape from local minimum.
- cellular automata
- ortools
- SMT solver
- data augmentation(make our new original tasks)
- relational deep learning
- geometric deep learning
- graph neural network
- few shot training
- Gaussian Texture Inpainting opencv, Texture Inpainting fft, inpainting image, periodic image, Texture Inpainting fft

## Bug?
- NoMaskConversion and visited_node_hashes?

## refactor
- use np.ravel() instead of chain.from_iterable
- use np.unique() instead of built-in set

## I've tried and failed. But there's still a chance it could work.
- DeepCoder approach?
  - abstraction_and_reasoning_challenge/src/ml/operation_element_inclusion_prediction.py
- Evaluation function by machine learning.
  - abstraction_and_reasoning_challenge/src/ml/data_generation.py
  - abstraction_and_reasoning_challenge/src/ml/train_ml.py
- solve "SYMMETRY" or "REPEAT" pattern tasks by FFT.
  - I thought I can find specific frequency, but it's difficult...
  - abstraction_and_reasoning_challenge/poc/fft_test.py
  - FFTCompletion

## links
- link summary
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130479
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130467
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130794
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130364
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130881
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130862
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130778
- Cellular Automata
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/131196
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/131195
- Task Taxonomy / Task Features
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/130493
  - https://www.kaggle.com/c/abstraction-and-reasoning-challenge/discussion/131238
  - https://www.kaggle.com/danielbecker/arcifier
- DeepCoder
  - https://www.microsoft.com/en-us/research/wp-content/uploads/2017/03/main.pdf
  - http://mabonki0725.hatenablog.com/entry/2017/10/22/132747 (Japanese)
- Inpainting
  - https://codegolf.stackexchange.com/questions/70483/patch-the-image
  - http://labs.eecs.tottori-u.ac.jp/sd/Member/oyamada/OpenCV/html/py_tutorials/py_photo/py_inpainting/py_inpainting.html
  - http://www.ipol.im/pub/art/2017/198/article_lr.pdf
  - https://scikit-image.org/docs/dev/auto_examples/filters/plot_inpaint.html#sphx-glr-auto-examples-filters-plot-inpaint-py
  - https://scikit-image.org/docs/dev/auto_examples/filters/plot_denoise_wavelet.html#sphx-glr-auto-examples-filters-plot-denoise-wavelet-py
- ?
  - https://github.com/SamuelGabriel/DSLApproachToARC

## Tasks that were once solved, but are now unsolvable.
```
# optimize cost parameter
1f85a75f_0, correct:  True, node:   1415, sec:     3, depth: 1, operation_set: OperationSet(operations=[ColorOperation(color_selection=SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), mask_conversions=[ConnectDotSelection(axis=Axis.HORIZONTAL, edge_type=LineEdgeType.EdgeExclude, fill_type=FillType.NotOverride)], mask_operation=MaskCoordsCrop())])
c909285e_0, correct:  True, node:   1101, sec:     2, depth: 1, operation_set: OperationSet(operations=[ColorOperation(color_selection=SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), mask_conversions=[ConnectDotSelection(axis=Axis.VERTICAL, edge_type=LineEdgeType.EdgeExclude, fill_type=FillType.NotOverride)], mask_operation=MaskCoordsCrop())])
f5b8619d_0, correct:  True, node:  18399, sec:    45, depth: 5, operation_set: OperationSet(operations=[Padding(padding_mode=PaddingMode.REPEAT, direction=Direction.BOTTOM, k=1), Padding(padding_mode=PaddingMode.REPEAT, direction=Direction.RIGHT, k=1), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.BLACK), mask_conversions=[ContourSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], mask_operation=FixedColorMaskFill(color=Color.SKY)), ColorOperation(color_selection=MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), mask_conversions=[SplitLineSelection(axis=Axis.VERTICAL)], mask_operation=FixedColorMaskFill(color=Color.BLACK)), ColorOperation(color_selection=MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON), mask_conversions=[ConnectDotSelection(axis=Axis.VERTICAL, edge_type=LineEdgeType.EdgeInclude, fill_type=FillType.NotOverride)], mask_operation=FixedColorMaskFill(color=Color.SKY))])

# ?
c0f76784_0, correct:  True, node:   0000, sec:    00, depth: 3, operation_set: OperationSet(operations=[ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON), [HolesSelection(connectivity=PixelConnectivity.FOUR_DIRECTION)], FixedColorMaskFill(color=Color.MAGENTA)),ColorOperation(SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), [ObjectsMaxMinSelection(max_or_min=MaxOrMin.MAX, object_feature=ObjectFeature.AREA, connectivity=PixelConnectivity.FOUR_DIRECTION)], FixedColorMaskFill(color=Color.SKY)),ColorOperation(SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), [ObjectsMaxMinSelection(max_or_min=MaxOrMin.MAX, object_feature=ObjectFeature.AREA, connectivity=PixelConnectivity.FOUR_DIRECTION)], FixedColorMaskFill(color=Color.ORANGE))])
810b9b61_0, correct:  True, node:   0000, sec:    00, depth: 3, operation_set: OperationSet(operations=[ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON), [HolesSelection(connectivity=PixelConnectivity.FOUR_DIRECTION)], FixedColorMaskFill(color=Color.GREEN)),ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), [ContourSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_INCLUDE)], FixedColorMaskFill(color=Color.GREEN)),ColorOperation(FixedSingleColorSelection(color=Color.GREEN), [ObjectInnerSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], SingleColorMaskFill(single_color_selection_mode=SingleColorSelectionMode.MOST_COMMON))])
b6afb2da_0, correct:  True, node:   0000, sec:    00, depth: 3, operation_set: OperationSet(operations=[ColorOperation(FixedSingleColorSelection(color=Color.GRAY), [ObjectInnerSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], FixedColorMaskFill(color=Color.RED)),ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), [ContourSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, image_edge_type=ImageEdgeType.EDGE_INCLUDE)], FixedColorMaskFill(color=Color.YELLOW)),ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), [ContourOuterSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, hole_include=HoleInclude.INCLUDE)], FixedColorMaskFill(color=Color.BLUE))])
b60334d2_0, correct:  True, node:   0000, sec:    00, depth: 4, operation_set: OperationSet(operations=[ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON), [NoAdditionalSelection()], FixedColorMaskFill(color=Color.BLUE)),ColorOperation(FixedSingleColorSelection(color=Color.BLUE), [ContourOuterSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, hole_include=HoleInclude.INCLUDE)], FixedColorMaskFill(color=Color.GRAY)),ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), [ContourSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, image_edge_type=ImageEdgeType.EDGE_INCLUDE)],SingleColorMaskFill(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON)),ColorOperation(MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON), [ObjectInnerSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], FixedColorMaskFill(color=Color.BLACK))])
178fcbfb_0, correct:  True, node:   6256, sec:    32, depth: 5, operation_set: OperationSet(operations=[ColorOperation(color_selection=FixedSingleColorSelection(color=Color.GREEN), additional_selections=[DotExistLineSelection(axis=Axis.HORIZONTAL)], mask_operation=FixedColorMaskFill(color=Color.GREEN)), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.BLUE), additional_selections=[DotExistLineSelection(axis=Axis.HORIZONTAL)], mask_operation=FixedColorMaskFill(color=Color.BLUE)), ColorOperation(color_selection=SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), additional_selections=[DotExistLineSelection(axis=Axis.VERTICAL)], mask_operation=SingleColorMaskFill(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON)), ColorOperation(color_selection=MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON), additional_selections=[ObjectInnerSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, image_edge_type=ImageEdgeType.EDGE_INCLUDE)], mask_operation=FixedColorMaskFill(color=Color.GREEN)), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.BLUE), additional_selections=[ConnectDotSelection(axis=Axis.HORIZONTAL, edge_type=LineEdgeType.EdgeExclude, fill_type=FillType.Override)], mask_operation=FixedColorMaskFill(color=Color.BLUE))])
ae4f1146_0, correct:  True, node:    603, sec:     1, depth: 1, operation_set: OperationSet(operations=[ColorOperation(color_selection=FixedSingleColorSelection(color=Color.BLUE), mask_conversions=[ObjectsMaxMinSelection(true_or_false=TrueOrFalse.TRUE, max_or_min=MaxOrMin.MAX, object_feature=ObjectFeature.AREA, connectivity=PixelConnectivity.EIGHT_DIRECTION)], mask_operation=MaskCoordsCrop())])
516b51b7_0, correct:  True, node:   4877, sec:    22, depth: 3, operation_set: OperationSet(operations=[ColorOperation(color_selection=FixedSingleColorSelection(color=Color.BLUE), mask_conversions=[ObjectInnerSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], mask_operation=FixedColorMaskFill(color=Color.RED)), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.RED), mask_conversions=[ObjectInnerSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], mask_operation=FixedColorMaskFill(color=Color.GREEN)), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.GREEN), mask_conversions=[ObjectInnerSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, image_edge_type=ImageEdgeType.EDGE_INCLUDE)], mask_operation=FixedColorMaskFill(color=Color.RED))])

# seed?
5117e062_0, correct:  True, node:  11340, sec:    27, depth: 3, operation_set: OperationSet(operations=[ColorOperation(color_selection=MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), mask_conversions=[ContourSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, image_edge_type=ImageEdgeType.EDGE_INCLUDE)], mask_operation=MaskCoordsCrop()), ColorOperation(color_selection=MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), mask_conversions=[HolesSelection(connectivity=PixelConnectivity.FOUR_DIRECTION)], mask_operation=FixedColorMaskFill(color=Color.YELLOW)), ColorOperation(color_selection=MultiColorSelection(multi_color_selection_mode=MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON), mask_conversions=[ConnectDotSelection(axis=Axis.HORIZONTAL, edge_type=LineEdgeType.EdgeExclude, fill_type=FillType.Override)], mask_operation=SingleColorMaskFill(single_color_selection_mode=SingleColorSelectionMode.MOST_COMMON))])
694f12f3_0, correct:  True, node:   1489, sec:     2, depth: 2, operation_set: OperationSet(operations=[ColorOperation(color_selection=FixedSingleColorSelection(color=Color.YELLOW), mask_conversions=[ObjectInnerSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, image_edge_type=ImageEdgeType.EDGE_EXCLUDE)], mask_operation=FixedColorMaskFill(color=Color.RED)), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.RED), mask_conversions=[ObjectsMaxMinSelection(true_or_false=TrueOrFalse.TRUE, max_or_min=MaxOrMin.MIN, object_feature=ObjectFeature.AREA, connectivity=PixelConnectivity.FOUR_DIRECTION)], mask_operation=FixedColorMaskFill(color=Color.BLUE))])
6c434453_0, correct:  True, node:   9494, sec:    22, depth: 3, operation_set: OperationSet(operations=[ColorOperation(color_selection=SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), mask_conversions=[HolesSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION)], mask_operation=FixedColorMaskFill(color=Color.RED)), ColorOperation(color_selection=FixedSingleColorSelection(color=Color.RED), mask_conversions=[ContourOuterSelection(connectivity=PixelConnectivity.EIGHT_DIRECTION, hole_include=HoleInclude.EXCLUDE)], mask_operation=FixedColorMaskFill(color=Color.BLACK)), ColorOperation(color_selection=SingleColorSelection(single_color_selection_mode=SingleColorSelectionMode.LEAST_COMMON), mask_conversions=[ContourOuterSelection(connectivity=PixelConnectivity.FOUR_DIRECTION, hole_include=HoleInclude.EXCLUDE)], mask_operation=FixedColorMaskFill(color=Color.RED))])

# dsl string expression is old style...
a87f7484 sec:     3, correct: True, depth: 2, operation_set: [(ColorSelection(color=<Color.LEAST: -22>), MaskFill(color=<Color.BLACK: 0>))]_[(ColorSelection(color=<Color.SECOND_MOST: -21>), MaskCoordsCrop())]
3de23699 sec:     8, correct: True, depth: 3, operation_set: [ConnectDots_color_-22]_[(HolesSelection(color=<Color.BLACK: 0>), MaskFill(color=<Color.SECOND_MOST: -21>))]_[(HolesSelection(color=<Color.ANY_WITHOUT_MOST: -11>), MaskCrop())]
```

