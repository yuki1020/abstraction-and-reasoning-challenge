# warnings.filterwarnings('error')
from collections import defaultdict

import pandas as pd
import shutil
from dataclasses import asdict
from pandas import DataFrame

from abstraction_and_reasoning_challenge.config import RunConfig, DebugConfig, KernelPathConfig, PathConfig
from abstraction_and_reasoning_challenge.param_config import AllParameter, DistanceEvaluatorParameter, NodeBaseSearchEngineParameter
from abstraction_and_reasoning_challenge.src.answer_storage.answer_storage import load_answer_storage
from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import create_task_feature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.operation.color_selection import MultiColorSelection
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import MultiColorSelectionMode
from abstraction_and_reasoning_challenge.src.domain.operation.util.str_to_operation_set_obj import str_to_operation_set
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.base import DepthSearchPattern
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.handmade_evaluator import DistanceEvaluator, HandMadeNodeEvaluator
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.waiting_node import ColorSelectionWaitingNode
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task_result import AnsweredSearchResults
from abstraction_and_reasoning_challenge.src.domain.task_solver import solve_tasks, optimize_tree_base_search, optimize_node_base_search
from abstraction_and_reasoning_challenge.src.enums import RunMode
from abstraction_and_reasoning_challenge.src.loader.task_loader import TaskLoader
from abstraction_and_reasoning_challenge.src.ml.data_generation import save_ml_training_data
from abstraction_and_reasoning_challenge.src.ml.operation_element_inclusion_prediction import train_operation_element_inclusion_prediction
from abstraction_and_reasoning_challenge.src.ml.train_ml import train_ml
from abstraction_and_reasoning_challenge.src.taxonomy.task_taxonomy import TaskTaxonomy
from abstraction_and_reasoning_challenge.src.utils import setup_df_display_options, initialize_path
from abstraction_and_reasoning_challenge.src.visualize.visualize import plot_task_with_operation_set, plot_task_with_result_set

setup_df_display_options()


def run():
    if debug_run():
        return

    initialize_path()

    if RunConfig.RUN_MODE == RunMode.LOCAL_RUN:
        load_answer_storage()  # debug validate
        tt = TaskTaxonomy()
        solve_tasks(tt.filter_tasks(TaskLoader().get_training_tasks()), AllParameter(), output_summary_path=PathConfig.OPERATION_ANSWER_MEMO_ROOT / 'answer_summary_train.txt', copy_wrong_answers_root_tag='train', add_answer_storage=True, save_submission=True)
        solve_tasks(tt.filter_tasks(TaskLoader().get_evaluation_tasks()), AllParameter(), output_summary_path=PathConfig.OPERATION_ANSWER_MEMO_ROOT / 'answer_summary_eval.txt', copy_wrong_answers_root_tag='eval', add_answer_storage=False, save_submission=False)
    elif RunConfig.RUN_MODE == RunMode.LOCAL_RUN_ALL:
        solve_tasks(TaskLoader().get_training_tasks(), AllParameter(), output_summary_path=PathConfig.OPERATION_ANSWER_MEMO_ROOT / 'answer_summary_train.txt', copy_wrong_answers_root_tag='train', add_answer_storage=True, save_submission=True)
        solve_tasks(TaskLoader().get_evaluation_tasks(), AllParameter(), output_summary_path=PathConfig.OPERATION_ANSWER_MEMO_ROOT / 'answer_summary_eval.txt', copy_wrong_answers_root_tag='eval', add_answer_storage=False, save_submission=False)
        solve_tasks(TaskLoader().get_test_tasks(), AllParameter(), save_submission=True)
    elif RunConfig.RUN_MODE == RunMode.KERNEL_EMULATION:
        solve_tasks(TaskLoader().get_test_tasks(), AllParameter(), save_submission=True)
    elif RunConfig.RUN_MODE == RunMode.NODE_BASE_SEARCH_OPTIMIZATION:
        optimize_node_base_search(TaskLoader().get_training_tasks())
    elif RunConfig.RUN_MODE == RunMode.TREE_BASE_SEARCH_OPTIMIZATION:
        optimize_tree_base_search(TaskLoader().get_training_tasks())
    elif RunConfig.RUN_MODE == RunMode.LOCAL_DATA_GENERATION:
        for t in TaskLoader().get_training_tasks():
            print(t.name)
            save_ml_training_data(t)
    elif RunConfig.RUN_MODE == RunMode.LOCAL_ML_TRAIN:
        train_ml()
    elif RunConfig.RUN_MODE == RunMode.TRAIN_OPERATION_ELEMENT_INCLUSION_PREDICTION:
        train_operation_element_inclusion_prediction()
    elif RunConfig.RUN_MODE == RunMode.KERNEL:
        if RunConfig.RUN_ONLY_PRIVATE_LB and not TaskLoader().is_private_lb_run():
            print('This is kernel public run. Skipped.')
            shutil.copy(str(KernelPathConfig.SAMPLE_SUBMISSION), KernelPathConfig.SUBMISSION)
            return
        else:
            print('This is private private run. Not skipped.')
            solve_tasks(TaskLoader().get_test_tasks(), AllParameter(), save_submission=True)
    else:
        raise ValueError(RunConfig.RUN_MODE)
    print('end')


def debug_run():
    print('start')
    if DebugConfig.OPERATION_DEBUG_TASK_NAME:
        operation_set = str_to_operation_set(DebugConfig.OPERATION_DEBUG_OPERATION_SET)
        print(operation_set)
        task = TaskLoader().get_task(DebugConfig.OPERATION_DEBUG_TASK_NAME)
        applied_task = TaskOperationSetExecutor().execute(task, operation_set)

        original_task_feature = create_task_feature(task, task)
        applied_task_feature = create_task_feature(task, applied_task)

        original_df = DataFrame(asdict(original_task_feature), index=['index']).T
        applied_df = DataFrame(asdict(applied_task_feature), index=['index']).T
        merged_feature_df = pd.merge(original_df, applied_df, left_index=True, right_index=True,
                                     suffixes=['original_', 'appplied_'])

        original_waiting_node = ColorSelectionWaitingNode(None, task, task, original_task_feature, OperationSet([]), MultiColorSelection(MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON))
        # original_waiting_node2 = MaskConversionWaitingNode(None, None, task, original_task_feature, OperationSet([]), SingleColorSelection(SingleColorSelectionMode.LEAST_COMMON))
        applied_waiting_node = ColorSelectionWaitingNode(None, task, applied_task, applied_task_feature, operation_set, MultiColorSelection(MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON))
        print(merged_feature_df)

        print('distance')
        print(DistanceEvaluator(DistanceEvaluatorParameter()).evaluate_task_feature(original_task_feature))
        print(DistanceEvaluator(DistanceEvaluatorParameter()).evaluate_task_feature(applied_task_feature))

        print('breadth cost')
        HandMadeNodeEvaluator(DepthSearchPattern.BREADTH_FIRST, defaultdict(lambda: 1), NodeBaseSearchEngineParameter(), DistanceEvaluatorParameter()).evaluate_nodes([original_waiting_node, original_waiting_node])
        HandMadeNodeEvaluator(DepthSearchPattern.BREADTH_FIRST, defaultdict(lambda: 1), NodeBaseSearchEngineParameter(), DistanceEvaluatorParameter()).evaluate_nodes([original_waiting_node, applied_waiting_node])
        print(original_waiting_node.cache_pred_distance)
        print(applied_waiting_node.cache_pred_distance)

        print('normal cost')
        HandMadeNodeEvaluator(DepthSearchPattern.NORMAL, defaultdict(lambda: 1), NodeBaseSearchEngineParameter(), DistanceEvaluatorParameter()).evaluate_nodes([original_waiting_node, original_waiting_node])
        HandMadeNodeEvaluator(DepthSearchPattern.NORMAL, defaultdict(lambda: 1), NodeBaseSearchEngineParameter(), DistanceEvaluatorParameter()).evaluate_nodes([original_waiting_node, applied_waiting_node])
        print(original_waiting_node.cache_pred_distance)
        print(applied_waiting_node.cache_pred_distance)

        print('depth cost')
        HandMadeNodeEvaluator(DepthSearchPattern.DEPTH_FIRST, defaultdict(lambda: 1), NodeBaseSearchEngineParameter(), DistanceEvaluatorParameter()).evaluate_nodes([original_waiting_node, original_waiting_node])
        HandMadeNodeEvaluator(DepthSearchPattern.DEPTH_FIRST, defaultdict(lambda: 1), NodeBaseSearchEngineParameter(), DistanceEvaluatorParameter()).evaluate_nodes([original_waiting_node, applied_waiting_node])
        print(original_waiting_node.cache_pred_distance)
        print(applied_waiting_node.cache_pred_distance)

        plot_task_with_operation_set(task, operation_set, show=True, save_path=None)
        return True

    if DebugConfig.SOLVE_DEBUG_TASK_NAME:
        task = TaskLoader().get_task(DebugConfig.SOLVE_DEBUG_TASK_NAME)
        engine_result = solve_tasks([task], AllParameter(), add_answer_storage=True, verbose=True)[0]

        if isinstance(engine_result, AnsweredSearchResults):
            plot_task_with_result_set(task, engine_result, show=True, save_path=None)
        return True

    if DebugConfig.TRAIN_DATA_GENERATION_DEBUG_TASK_NAME:
        task = TaskLoader().get_task(DebugConfig.TRAIN_DATA_GENERATION_DEBUG_TASK_NAME)
        save_ml_training_data(task)
        train_ml(task)
        return True

    return False


def performance_run():
    # from line_profiler import LineProfiler
    # from python_utils.src.library.print_line_profiler import print_stats
    # from abstraction_and_reasoning_challenge import run as run_module
    # from abstraction_and_reasoning_challenge.src.domain import task_solver
    # from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions import handmade_evaluator
    # from abstraction_and_reasoning_challenge.src.domain.search_engine.node import waiting_node
    # from abstraction_and_reasoning_challenge.src.domain.search_engine.node_processor import waiting_node_processor
    # from abstraction_and_reasoning_challenge.src.domain.feature import task_feature
    # from abstraction_and_reasoning_challenge.src.domain.search_engine.engine import node_base_search_engine
    # from abstraction_and_reasoning_challenge.src.domain.search_engine.engine import tree_base_search_engine
    #
    # profiler = LineProfiler()
    # profiler.add_module(run_module)
    # profiler.add_module(task_solver)
    # profiler.add_module(handmade_evaluator)
    # profiler.add_module(waiting_node)
    # profiler.add_module(waiting_node_processor)
    # profiler.add_module(task_feature)
    # profiler.add_module(node_base_search_engine)
    # profiler.add_module(tree_base_search_engine)
    #
    # profiler.runcall(run)
    # # profiler.print_stats()
    # stats = profiler.get_stats()
    # print_stats(stats, strip_seconds_limit=0., cost_sort=True)
    pass


if __name__ == '__main__':
    performance_profiling_mode = False

    if performance_profiling_mode:
        performance_run()
        print('performance end')
    else:
        run()
