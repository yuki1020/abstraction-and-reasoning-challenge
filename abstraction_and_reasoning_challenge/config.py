from pathlib import Path

from abstraction_and_reasoning_challenge.src.domain.search_engine.engine.enums import EngineSchedulePattern, EngineType
from abstraction_and_reasoning_challenge.src.enums import RunMode, TaskRange


class RunConfig:
    RUN_MODE = RunMode.KERNEL  # Usually, use "LOCAL_RUN" or "KERNEL"
    TASK_RANGE = TaskRange.ALL  # Limit the range to save time.
    ENGINE_TYPE = EngineType.NODE_BASED_SEARCH_ENGINE
    ENGINE_SCHEDULE_PATTERN = EngineSchedulePattern.HAND_MADE
    USE_ML_GUIDE = False  # Calculate the probability of inclusion of each DSL elements.
    ONE_DEPTH_EVAL_GUIDE = False
    RUN_ONLY_PRIVATE_LB = False  # Skip public kernel run to save time.

    _KERNEL_N_JOB = 4
    _LOCAL_N_JOB = 5
    N_JOB = _KERNEL_N_JOB if RUN_MODE == RunMode.KERNEL else _LOCAL_N_JOB


class DebugConfig:
    OPERATION_DEBUG_TASK_NAME = ''  # dae9d2b5
    OPERATION_DEBUG_OPERATION_SET = ''
    # solve debug
    SOLVE_DEBUG_TASK_NAME = ''  # dae9d2b5

    # train_data_generator debug
    TRAIN_DATA_GENERATION_DEBUG_TASK_NAME = ''


class PathConfig:
    ROOT: Path = Path('') if RunConfig.RUN_MODE == RunMode.KERNEL else Path(__file__).parent

    # input
    INPUT_ROOT: Path = ROOT / 'input'
    TRAIN_ROOT: Path = INPUT_ROOT / 'training'  # training_and_evaluation
    EVALUATION_ROOT: Path = INPUT_ROOT / 'evaluation'
    TEST_ROOT: Path = INPUT_ROOT / 'test'
    SAMPLE_SUBMISSION: Path = INPUT_ROOT / 'sample_submission.csv'

    # output
    OUTPUT_SUBMISSION: Path = ROOT / 'output' / 'submission.csv'

    # answer_memo
    OPERATION_ANSWER_MEMO_ROOT: Path = ROOT / 'answer_memo'
    OPERATION_ANSWER_TAXONOMY_YAML: Path = OPERATION_ANSWER_MEMO_ROOT / 'answer_taxonomy.yaml'
    OPERATION_ANSWER_TAXONOMY_IMAGE_ROOT: Path = OPERATION_ANSWER_MEMO_ROOT / 'answer_taxonomy'
    OPERATION_ANSWER_STORAGE: Path = OPERATION_ANSWER_MEMO_ROOT / 'answer_storage.txt'
    WRONG_ANSWERS_ROOT: Path = OPERATION_ANSWER_MEMO_ROOT / 'wrong_answers'

    # kernel
    KERNEL_SCRIPT_PATH: Path = ROOT / 'kernel' / 'kernel_script.py'

    # run
    LOG_ROOT: Path = ROOT / 'log'

    # ml_model
    SAVED_MODEL: Path = ROOT / 'saved_model'
    NODE_EVALUATOR_FEATURES = SAVED_MODEL / 'features.pkl'
    NODE_EVALUATOR_CATEGORICAL_FEATURES = SAVED_MODEL / 'categorical_features.pkl'
    NODE_EVALUATOR_MODEL = SAVED_MODEL / 'model.pkl'
    NODE_EVALUATOR_ORDINAL_ENCODER = SAVED_MODEL / 'ordinal_encoder.pkl'
    NODE_EVALUATOR_SAMPLE_DF = SAVED_MODEL / 'sample_df.pkl'

    OPERATION_ELEMENT_INCLUSION_MODEL_ROOT = SAVED_MODEL / 'operation_element_inclusion'
    OPERATION_ELEMENT_INCLUSION_MODEL = OPERATION_ELEMENT_INCLUSION_MODEL_ROOT / 'model.pkl'
    OPERATION_ELEMENT_INCLUSION_MODEL_TARGET_COLUMNS = OPERATION_ELEMENT_INCLUSION_MODEL_ROOT / 'target_columns.pkl'
    OPERATION_ELEMENT_INCLUSION_MODEL_FEATURE_COLUMNS = OPERATION_ELEMENT_INCLUSION_MODEL_ROOT / 'feature_columns.pkl'

    # ml_training_data
    LABELED_TRAINING_DATA_ROOT = ROOT / 'training'


class KernelPathConfig:
    INPUT_ROOT = Path('/kaggle/input/abstraction-and-reasoning-challenge/')
    TRAIN_ROOT: Path = INPUT_ROOT / 'training'
    EVALUATION_ROOT: Path = INPUT_ROOT / 'evaluation'
    TEST_ROOT: Path = INPUT_ROOT / 'test'
    SAMPLE_SUBMISSION: Path = INPUT_ROOT / 'sample_submission.csv'
    SUBMISSION = 'submission.csv'
