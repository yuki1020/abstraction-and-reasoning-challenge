import pickle

from itertools import chain
from pandas import DataFrame

from abstraction_and_reasoning_challenge.config import PathConfig
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node_base_search_engine import NodeBaseSearchEngine
from abstraction_and_reasoning_challenge.src.domain.search_engine.random_search_engine.random_search_engine import RandomNodeTreeCreateEngine
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.domain.task.task import Task


def save_ml_training_data(task: Task, verbose: bool = False):
    # 正解データ
    correct_node_trees, exception, _ = NodeBaseSearchEngine(answer_limit_num=60).search(task, verbose)

    print('search engine end')
    if exception is not None:
        print('answer not found')
        return

    correct_node_trees = [t for t in correct_node_trees if AnswerMatcher.is_train_test_all_match_if_operated(task, t.to_operation_set())]

    if len(correct_node_trees) == 0:
        print('answer not found')
        return

    correct_waiting_nodes = list(chain.from_iterable([t.waiting_nodes() for t in correct_node_trees]))
    correct_feature_dicts = [n.evaluation_features() for n in correct_waiting_nodes]
    correct_feature_dict_tuples = set(tuple(sorted(d.items())) for d in correct_feature_dicts)
    correct_df = DataFrame(dict(t) for t in correct_feature_dict_tuples)

    # 不正解データ
    trees = RandomNodeTreeCreateEngine(timeout_sec=120).search(task, verbose)
    print('random tree generated')
    waiting_nodes = list(chain.from_iterable([t.waiting_nodes() for t in trees]))
    feature_dicts = [n.evaluation_features() for n in waiting_nodes]

    feature_dict_tuples = set(tuple(sorted(d.items())) for d in feature_dicts)
    feature_dict_tuples = feature_dict_tuples - correct_feature_dict_tuples
    wrong_df = DataFrame(dict(t) for t in feature_dict_tuples)

    # ラベル付け
    correct_df['label'] = 1
    wrong_df['label'] = 0
    all_df = correct_df.append(wrong_df, sort=False)

    PathConfig.LABELED_TRAINING_DATA_ROOT.mkdir(parents=True, exist_ok=True)
    pickle.dump(all_df, (PathConfig.LABELED_TRAINING_DATA_ROOT / f'{task.name}.pkl').open(mode='wb'))
    print('save')
