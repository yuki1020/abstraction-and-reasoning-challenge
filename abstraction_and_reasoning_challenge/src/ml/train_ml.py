import pickle

import category_encoders
import numpy as np
import pandas as pd
from lightgbm import LGBMClassifier
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.model_selection import KFold
from tqdm import tqdm

from abstraction_and_reasoning_challenge.config import PathConfig, RunConfig


def train_ml():
    x, y, feature_columns, categorical_features = prepare_train_data()
    train_lgbm(x, y, feature_columns, categorical_features)


def prepare_train_data():
    dfs = []
    for pickle_path in PathConfig.LABELED_TRAINING_DATA_ROOT.iterdir():
        print(pickle_path)
        dfs.append(pickle.load((pickle_path.open(mode='rb'))))
    all_df = pd.concat(dfs, ignore_index=True, sort=False)

    print(f'label1: {len(all_df[all_df["label"] == 1])}_label0: {len(all_df[all_df["label"] == 0])}')

    # TODO Should we not use dsl properties(too detailed) features?
    not_used_feature_columns = {
        'label', 'depth', 'color', 'angle', 'direction',
        'multi_color_selection_mode', 'single_color_selection_mode',
        'edge_type', 'fill_type', 'flip_mode', 'k', 'ratio', 'padding_mode'
    }
    feature_columns = sorted(set(all_df.columns) - not_used_feature_columns)
    all_df = all_df[feature_columns + ['label']]

    # process categorical
    categorical_features = list(filter(lambda s: s in feature_columns, map(str, all_df.select_dtypes(include='object').columns)))

    for c_f in categorical_features:
        all_df[c_f] = all_df[c_f].fillna('None')
        all_df[c_f] = all_df[c_f].apply(str)
        # all_df[c_f] = all_df[c_f].astype(str)
        # all_df[c_f] = all_df[c_f].apply(lambda v: str(v))
        # all_df[c_f] = all_df[c_f].astype('category')

    oe = category_encoders.OrdinalEncoder()
    all_df[categorical_features] = oe.fit_transform(all_df[categorical_features])

    # oe = category_encoders.OneHotEncoder(cols=[categorical_features])
    # all_df = oe.fit_transform(all_df)

    # all_df = all_df.fillna(-1)

    # Relabel 0-labeled data in the neighborhood of 1 to 1
    print('relabelling')
    small_is_better_features = [
        'mean_diff_color_cell_ratio',
        'mean_excess_color_num',
        'mean_lack_color_num',
        'mean_horizontal_diff_input_arr_line_num',
        'mean_horizontal_diff_output_arr_line_num',
        'mean_horizontal_edge_sum_diff',
        'mean_horizontal_edge_sum_diff_ratio',
        'mean_vertical_diff_input_arr_line_num',
        'mean_vertical_diff_output_arr_line_num',
        'mean_vertical_edge_sum_diff',
        'mean_vertical_edge_sum_diff_ratio',
    ]

    for index, r in tqdm(all_df[all_df['label'] == 1].iterrows()):
        temp_feature = sorted(set(feature_columns) - set(small_is_better_features))

        near_rows = all_df[(all_df[temp_feature] == r[temp_feature]).all(axis=1)]
        can_label_1 = near_rows[(near_rows[small_is_better_features] <= r[small_is_better_features]).all(axis=1)]

        all_df.loc[can_label_1.index.values, 'label'] = 1

    print(f'label1: {len(all_df[all_df["label"] == 1])}_label0: {len(all_df[all_df["label"] == 0])}')

    print(f'feature_columns:')
    for f in feature_columns:
        print(f)

    x = all_df[feature_columns]
    y = all_df['label']
    print(f'len(x): {len(x)}, 1_labelled_len: {len(y[y == 1])}')
    # x, y = RandomUnderSampler(sampling_strategy=0.01).fit_resample(x, y)
    # x, y = EditedNearestNeighbours(sampling_strategy=0.01, n_jobs=RunConfig.N_JOB).fit_resample(x, y)
    print(f'len(x): {len(x)}, 1_labelled_len: {len(y[y == 1])}')

    # visualize
    # scaled_x = StandardScaler().fit_transform(x)
    # x_reduced = PCA(n_components=2).fit_transform(scaled_x)
    # plt.scatter(x_reduced[y == 1, 0], x_reduced[y == 1, 1], alpha=0.1)
    # plt.scatter(x_reduced[:, 0], x_reduced[:, 1], c=y, alpha=0.1)
    # plt.show()
    # plt.close()

    pickle.dump(all_df, PathConfig.NODE_EVALUATOR_SAMPLE_DF.open(mode='wb'))
    pickle.dump(oe, PathConfig.NODE_EVALUATOR_ORDINAL_ENCODER.open(mode='wb'))
    pickle.dump(feature_columns, PathConfig.NODE_EVALUATOR_FEATURES.open(mode='wb'))
    pickle.dump(categorical_features, PathConfig.NODE_EVALUATOR_CATEGORICAL_FEATURES.open(mode='wb'))

    return x, y, feature_columns, categorical_features


def train_lg(feature_columns, x, y):
    model = LogisticRegression(class_weight='balanced', n_jobs=RunConfig.N_JOB)
    model.fit(x, y)

    pred_y = model.predict_proba(x)
    print(pred_y)
    # cb = CatBoostClassifier(loss_function='Logloss', class_weights=[0.1, 1], cat_features=categorical_features)
    # cb.fit(x, y)
    # pred_y = cb.predict_proba(x)

    PathConfig.SAVED_MODEL.mkdir(parents=True, exist_ok=True)
    pickle.dump(model, PathConfig.NODE_EVALUATOR_MODEL.open(mode='wb'))

    del model

    print(x)
    print(y)
    print(pred_y)

    # cb = CatBoostClassifier()
    # cb.load_model(str(PathConfig.NODE_EVALUATOR_MODEL), format="cbm")
    model = pickle.load(PathConfig.NODE_EVALUATOR_MODEL.open(mode='rb'))
    pred_y = model.predict_proba(x)

    print(pred_y)

    coefs = np.abs(model.coef_[0])

    for c, f in zip(coefs, feature_columns):
        print(f'{f}_{c}')


def train_lgbm(x, y, feature_columns, categorical_features):
    lgbm_params = {
        'silent': False, 'n_jobs': RunConfig.N_JOB,
        'class_weight': 'balanced', 'max_depth': 3, 'learning_rate': 0.2,
    }

    best_iterations = []
    folds = KFold(shuffle=False, n_splits=3)
    for n_fold, (train_index, valid_index) in enumerate(folds.split(x, y)):
        train_x, train_y = x.iloc[train_index], y.iloc[train_index]
        valid_x, valid_y = x.iloc[valid_index], y.iloc[valid_index]

        model = LGBMClassifier(n_estimators=1000, **lgbm_params)
        model.fit(train_x, train_y, eval_set=[(valid_x, valid_y), (train_x, train_y)],
                  early_stopping_rounds=10, categorical_feature=categorical_features,
                  verbose=True)
        best_iterations.append(model.best_iteration_)

    print(best_iterations)

    model = LGBMClassifier(n_estimators=min(best_iterations), **lgbm_params)
    model.fit(x, y, verbose=True, categorical_feature=categorical_features)

    pred_y = model.predict_proba(x)
    print(pred_y)

    PathConfig.SAVED_MODEL.mkdir(parents=True, exist_ok=True)
    pickle.dump(model, PathConfig.NODE_EVALUATOR_MODEL.open(mode='wb'))

    del model

    print(x)
    print(y)
    print(pred_y)

    model = pickle.load(PathConfig.NODE_EVALUATOR_MODEL.open(mode='rb'))
    pred_y = model.predict_proba(x)

    print(pred_y)

    importance = pd.DataFrame(model.feature_importances_, index=feature_columns, columns=['importance'])

    print(importance)


def train_test_model(feature_columns, x, y):
    try_cv = False

    if try_cv:
        folds = KFold(shuffle=True)
        for n_fold, (train_index, valid_index) in enumerate(folds.split(x, y)):
            train_x, train_y = x.iloc[train_index], y.iloc[train_index]
            valid_x, valid_y = x.iloc[valid_index], y.iloc[valid_index]

            model = LGBMClassifier(class_weight='balanced', learning_rate=0.2, n_jobs=RunConfig.N_JOB, n_estimators=1000,
                                   silent=False)
            model.fit(train_x, train_y, eval_set=[(valid_x, valid_y), (train_x, train_y)],
                      early_stopping_rounds=10,
                      verbose=True)

    # model = MLPClassifier(hidden_layer_sizes=(20, 20, 10))
    model = RidgeClassifier(class_weight='balanced')
    # model = LinearSVC(class_weight='balanced')
    # model = LGBMClassifier(class_weight='balanced', learning_rate=0.2, n_estimators=50,
    #                        silent=False)
    model.fit(x, y)

    pred_y = model.predict(x)
    print(pred_y)

    PathConfig.SAVED_MODEL.mkdir(parents=True, exist_ok=True)
    pickle.dump(model, PathConfig.NODE_EVALUATOR_MODEL.open(mode='wb'))

    del model

    print(x)
    print(y)
    print(pred_y)

    model = pickle.load(PathConfig.NODE_EVALUATOR_MODEL.open(mode='rb'))
    pred_y = model.predict_proba(x)

    print(pred_y)

    importance = pd.DataFrame(model.feature_importances_, index=feature_columns, columns=['importance'])

    print(importance)
