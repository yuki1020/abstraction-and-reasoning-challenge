import pickle

import shutil
from dataclasses import asdict
from itertools import chain
from pandas import DataFrame
from sklearn.neural_network import MLPClassifier
from typing import Dict

from abstraction_and_reasoning_challenge.config import PathConfig
from abstraction_and_reasoning_challenge.src.answer_storage.answer_storage import load_answer_storage, save_answer_storage
from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import create_task_feature, TaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import get_all_operation_element_classes, get_all_operation_classes, OperationSet
from abstraction_and_reasoning_challenge.src.loader.task_loader import TaskLoader


def train_operation_element_inclusion_prediction():
    storage = load_answer_storage()
    save_answer_storage(storage)
    storage = storage.get_only_correct_answer_storage()
    print(storage.get_text())

    type_classes = [c.__name__ for c in get_all_operation_classes()]
    subclasses = [c.__name__ for c in get_all_operation_element_classes()]
    record_dicts = []

    for task_name, elements in storage.get_task_grouped_elements():
        pseudo_operation_set = OperationSet(list(chain.from_iterable([e.operation_set.operations for e in elements])))

        task = TaskLoader().get_task(task_name)
        task_feature = create_task_feature(task)

        operation_type_classes = [o_s_t.__name__ for o_s_t in pseudo_operation_set.types()]
        type_answer_dict = {c: c in operation_type_classes for c in type_classes}

        operation_element_classes = [o_s_e.__class__.__name__ for o_s_e in pseudo_operation_set.elements()]
        element_answer_dict = {c: c in operation_element_classes for c in subclasses}

        record_dicts.append({**asdict(task_feature), **type_answer_dict, **element_answer_dict})

    df = DataFrame(record_dicts)
    df = df.fillna(10)  # TODO do not use magic number

    target_columns = type_classes + subclasses
    feature_columns = list(set(df.columns) - set(target_columns))

    x = df[feature_columns]
    y = df[target_columns]

    print(x)
    print(y)

    model = MLPClassifier(
        # early_stopping=True, validation_fraction=0.3, n_iter_no_change=50,
        hidden_layer_sizes=(50,), solver='sgd', learning_rate_init=0.003, max_iter=40,
        verbose=True,
    )
    model.fit(x, y)

    shutil.rmtree(PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL_ROOT, ignore_errors=True)
    PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL_ROOT.mkdir(parents=True, exist_ok=True)
    pickle.dump(model, PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL.open(mode='wb'))
    pickle.dump(feature_columns, PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL_FEATURE_COLUMNS.open(mode='wb'))
    pickle.dump(target_columns, PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL_TARGET_COLUMNS.open(mode='wb'))

    temp_dicts = []
    for e in sorted(storage.elements, key=lambda e: e.task_name):
        task = TaskLoader().get_task(e.task_name)
        task_feature = create_task_feature(task)
        pred_dict = predict_operation_element_inclusion(task_feature)

        operation_type_classes = [o_s_t.__name__ for o_s_t in e.operation_set.types()]
        operation_element_classes = [o_s_e.__class__.__name__ for o_s_e in e.operation_set.elements()]
        element_answer_dict = {c: c in operation_type_classes + operation_element_classes for c in type_classes + subclasses}
        temp_dicts.append(element_answer_dict)
        temp_dicts.append(pred_dict)

    temp_df = DataFrame(temp_dicts)
    print(temp_df)


def predict_operation_element_inclusion(task_feature: TaskFeature) -> Dict[str, float]:
    model: MLPClassifier = pickle.load(PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL.open(mode='rb'))
    feature_columns = pickle.load(PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL_FEATURE_COLUMNS.open(mode='rb'))
    target_columns = pickle.load(PathConfig.OPERATION_ELEMENT_INCLUSION_MODEL_TARGET_COLUMNS.open(mode='rb'))

    df = DataFrame([asdict(task_feature)])
    df = df.fillna(10)

    x = df[feature_columns]
    y = model.predict_proba(x)[0]

    return {c: p for c, p in zip(target_columns, y)}
