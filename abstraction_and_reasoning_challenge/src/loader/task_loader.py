import json

from pathlib import Path
from typing import List

from abstraction_and_reasoning_challenge.config import PathConfig, RunConfig, KernelPathConfig
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.enums import RunMode, TaskRange


class TaskLoader:

    def get_task(self, name: str) -> Task:
        try:
            return self._get_task(PathConfig.TRAIN_ROOT / f'{name}.json')
        except FileNotFoundError:
            return self._get_task(PathConfig.EVALUATION_ROOT / f'{name}.json')

    def get_training_tasks(self):
        if RunConfig.RUN_MODE == RunMode.KERNEL:
            return self._get_tasks(KernelPathConfig.TRAIN_ROOT)
        else:
            return self._get_tasks(PathConfig.TRAIN_ROOT)

    def get_evaluation_tasks(self):
        if RunConfig.RUN_MODE == RunMode.KERNEL:
            return self._get_tasks(KernelPathConfig.EVALUATION_ROOT)
        else:
            return self._get_tasks(PathConfig.EVALUATION_ROOT)

    def get_test_tasks(self):
        if RunConfig.RUN_MODE == RunMode.KERNEL:
            return self._get_tasks(KernelPathConfig.TEST_ROOT)
        else:
            return self._get_tasks(PathConfig.TEST_ROOT)

    def _get_tasks(self, root_path: Path) -> List[Task]:
        return [self._get_task(json_path) for json_path in root_path.iterdir()]

    def _get_task(self, path: Path) -> Task:
        with open(str(path), 'r') as f:
            return Task.of(path.stem, json.load(f))

    def is_private_lb_run(self) -> bool:
        eval_tasks = self._get_tasks(KernelPathConfig.EVALUATION_ROOT)
        test_tasks = self._get_tasks(KernelPathConfig.TEST_ROOT)

        eval_names = [t.name for t in eval_tasks]
        if any(filter(lambda t: t.name in eval_names, test_tasks)):
            return False
        else:
            return True
