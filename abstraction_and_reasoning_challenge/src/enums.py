from enum import unique, Enum, auto


@unique
class RunMode(Enum):
    LOCAL_RUN_ALL = auto()
    LOCAL_RUN = auto()
    TREE_BASE_SEARCH_OPTIMIZATION = auto()
    NODE_BASE_SEARCH_OPTIMIZATION = auto()
    LOCAL_DATA_GENERATION = auto()
    LOCAL_ML_TRAIN = auto()
    TRAIN_OPERATION_ELEMENT_INCLUSION_PREDICTION = auto()
    KERNEL = auto()
    KERNEL_EMULATION = auto()


@unique
class TaskRange(Enum):
    ALL = auto()
    CAN_ANSWER_ONLY = auto()
    EXCLUDE_GIVE_UPS = auto()
