import shutil
from itertools import chain
from ruamel import yaml
from tqdm import tqdm
from typing import List, Dict

from abstraction_and_reasoning_challenge.config import PathConfig, RunConfig
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.enums import TaskRange
from abstraction_and_reasoning_challenge.src.visualize.visualize import plot_task

CATEGORIES = [
    'PARTITION',
    'SYMMETRY',
    'REPEAT',
    'DENOISE',
    'SIMPLIFICATION',
    'NUMBER',
    'RANKING',
    'SHAPE',
    'FIND_FIT',
    'LINE',
    'OBJECT_TRANSFORM',
    'OBJECT_MOVE',
    'JIGSAW_PUZZLE',
    'COLOR',
    'PASTE',
    'GUIDE',
    'META',
    'OTHERS',
    'ONCE_ANSWERED',
]

GIVE_UPS = [
    'SYMMETRY',
    'REPEAT',
    'DENOISE',
    'SIMPLIFICATION',
    'NUMBER',
    'RANKING',
    'SHAPE',
    'FIND_FIT',
    'OBJECT_MOVE',
    'JIGSAW_PUZZLE',
    'COLOR',
    'PASTE',
    'GUIDE',
    'META',
]


class TaskTaxonomy:

    def __init__(self):
        with open(str(PathConfig.OPERATION_ANSWER_TAXONOMY_YAML), 'r') as f:
            yaml_dict = yaml.load(f, Loader=yaml.Loader)
        self.trains: Dict[str, List[str]] = yaml_dict['1_train']
        self.evals: Dict[str, List[str]] = yaml_dict['2_eval']
        self.check()

    def check(self):
        assert len(self.trains) == len(self.evals) == 400

        for task_name, categories in {**self.trains, **self.evals}.items():
            assert len(categories) == len(set(categories))
            for category in categories:
                assert category in CATEGORIES, category

        json_task_names = {path.stem for path in chain.from_iterable([PathConfig.TRAIN_ROOT.iterdir(), PathConfig.EVALUATION_ROOT.iterdir()])}
        df_task_names = set(list(self.trains.keys()) + list(self.evals.keys()))
        assert json_task_names - df_task_names == set(), json_task_names - df_task_names
        assert df_task_names - json_task_names == set(), df_task_names - json_task_names

    def show_stats(self):
        print('=== train stats ====')
        for c in CATEGORIES:
            num = len(list(filter(lambda v: c in v, self.trains.values())))
            print(f'{c}: {num}')

        print('\n=== eval stats ====')
        for c in CATEGORIES:
            num = len(list(filter(lambda v: c in v, self.evals.values())))
            print(f'{c}: {num}')

    def save_yaml(self):
        self.check()

        with open(str(PathConfig.OPERATION_ANSWER_TAXONOMY_YAML), 'w') as f:
            yaml.dump({'1_train': self.trains,
                       '2_eval': self.evals}, f)

    def save_categorized_fig(self):
        from abstraction_and_reasoning_challenge.src.loader.task_loader import TaskLoader  # TODO fix local import？
        shutil.rmtree(PathConfig.OPERATION_ANSWER_TAXONOMY_IMAGE_ROOT)

        for (task_name, categories), tag in tqdm(list(zip(list(self.trains.items()) + list(self.evals.items()), ['train'] * len(self.trains) + ['evals'] * len(self.evals)))):
            if categories == []:
                task = TaskLoader().get_task(task_name)
                plot_task(task, show=False, save_path=PathConfig.OPERATION_ANSWER_TAXONOMY_IMAGE_ROOT / tag / 'not_categorized' / f'{task_name}.png')
            for c in categories:
                task = TaskLoader().get_task(task_name)
                plot_task(task, show=False, save_path=PathConfig.OPERATION_ANSWER_TAXONOMY_IMAGE_ROOT / tag / c / f'{task_name}.png')

    def get_give_up_task_names(self) -> List[str]:
        can_answers = self.get_can_answer_task_names()
        give_up_task_names = []
        for task_name, categories in {**self.trains, **self.evals}.items():
            if task_name in can_answers:
                continue
            for c in categories:
                if c in GIVE_UPS:
                    give_up_task_names.append(task_name)
                    break

        return give_up_task_names

    def get_can_answer_task_names(self) -> List[str]:
        return [task_name for task_name, categories in {**self.trains, **self.evals}.items() if 'ONCE_ANSWERED' in categories]

    def filter_tasks(self, tasks: List[Task]) -> List[Task]:
        if RunConfig.TASK_RANGE == TaskRange.ALL:
            return tasks
        elif RunConfig.TASK_RANGE == TaskRange.EXCLUDE_GIVE_UPS:
            return list(filter(lambda t: t.name not in self.get_give_up_task_names(), tasks))
        elif RunConfig.TASK_RANGE == TaskRange.CAN_ANSWER_ONLY:
            return list(filter(lambda t: t.name in self.get_can_answer_task_names(), tasks))
        else:
            raise NotImplementedError()

