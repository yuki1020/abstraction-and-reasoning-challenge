from heapq import heapify, heappop, heappush, heappushpop

from typing import TypeVar, List

T = TypeVar('T')


class PriorityQueue:
    def __init__(self, heap: List[T]):
        self.heap = heap
        heapify(self.heap)

    def refresh(self):
        heapify(self.heap)

    def push(self, item: T):
        heappush(self.heap, item)

    def pop_min(self) -> T:
        return heappop(self.heap)

    def pop_mins(self) -> List[T]:
        min_item = self.pop_min()
        results = [min_item]
        for _ in range(len(self.heap)):
            item = self.pop_min()
            if item <= min_item:
                results.append(item)
            else:
                self.push(item)
                return results

        return results

    def pop_mins_or_as_least_n(self, n: int) -> List[T]:
        results = []

        while len(results) < n:
            if len(self) == 0:
                break
            results += self.pop_mins()

        return results

    def push_pop(self, item: T) -> T:
        return heappushpop(self.heap, item)

    def __len__(self) -> int:
        return len(self.heap)

    def sorted_list(self) -> List[T]:
        return sorted(self.heap)
