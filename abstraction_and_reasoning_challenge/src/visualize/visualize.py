import numpy as np
from matplotlib import colors, pyplot as plt
from pathlib import Path
from typing import Optional

from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.domain.task.task_result import AnsweredSearchResults


def plot_one(ax, arr: np.ndarray, i, train_or_test, input_or_output):
    cmap = colors.ListedColormap(
        ['#000000', '#0074D9', '#FF4136', '#2ECC40', '#FFDC00',
         '#AAAAAA', '#F012BE', '#FF851B', '#7FDBFF', '#870C25'])
    norm = colors.Normalize(vmin=0, vmax=9)

    ax.imshow(arr, cmap=cmap, norm=norm)
    ax.grid(True, which='both', color='lightgrey', linewidth=0.5)
    ax.set_yticks([x - 0.5 for x in range(1 + len(arr))])
    ax.set_xticks([x - 0.5 for x in range(1 + len(arr[0]))])
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_title(train_or_test + ' ' + input_or_output)


def plot_task(task: Task, show: bool, save_path: Optional[Path]):
    input_output_num = len(task.train + task.test)
    total_row = 2
    fig, axs = plt.subplots(total_row, input_output_num, figsize=(2 * input_output_num, 2 * total_row))
    for i, (input_output, tag) in enumerate(zip(task.train + task.test, ['train'] * len(task.train) + ['test'] * len(task.test))):
        plot_one(axs[0, i], input_output.input_arr, i, tag, 'input')
        plot_one(axs[1, i], input_output.output_arr, i, tag, 'output')
    plt.tight_layout()

    if save_path:
        save_path.parent.mkdir(parents=True, exist_ok=True)
        plt.savefig(save_path)
    if show:
        plt.show()
    plt.close()


def plot_task_with_operation_set(task: Task, operation_set: OperationSet, show: bool, save_path: Optional[Path]):
    input_output_num = len(task.train + task.test)
    total_row = 3

    applied_task = TaskOperationSetExecutor().execute(task, operation_set)
    fig, axs = plt.subplots(total_row, input_output_num, figsize=(3 * input_output_num, 3 * total_row))
    for i, (raw_io, applied_io) in enumerate(zip(task.train + task.test, applied_task.train + applied_task.test)):
        plot_one(axs[0, i], raw_io.input_arr, i, 'train?', 'input')
        plot_one(axs[1, i], raw_io.output_arr, i, 'train?', 'output')
        plot_one(axs[2, i], applied_io.input_arr, i, 'train?', 'operated')
    plt.tight_layout()

    if save_path:
        save_path.parent.mkdir(parents=True, exist_ok=True)
        plt.savefig(save_path)
    if show:
        plt.show()
    plt.close()


def plot_task_with_result_set(task: Task, search_results: AnsweredSearchResults, show: bool, save_path: Optional[Path]):
    input_output_num = len(task.train + task.test)
    total_row = 2 + len(search_results.results)

    applied_tasks = [TaskOperationSetExecutor().execute(task, r.operation_set) for r in search_results.results]
    fig, axs = plt.subplots(total_row, input_output_num, figsize=(3 * input_output_num, 3 * total_row))
    for i, input_output in enumerate(task.train + task.test):
        plot_one(axs[0, i], input_output.input_arr, i, 'train?', 'input')
        plot_one(axs[1, i], input_output.output_arr, i, 'train?', 'output')
    for i, t in enumerate(applied_tasks):
        for j, input_output in enumerate(t.train + t.test):
            plot_one(axs[i + 2, j], input_output.input_arr, i, 'train?', 'input')
    plt.tight_layout()

    if save_path:
        save_path.parent.mkdir(parents=True, exist_ok=True)
        plt.savefig(save_path)
    if show:
        plt.show()
    plt.close()
