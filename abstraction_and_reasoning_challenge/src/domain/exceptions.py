from dataclasses import dataclass

from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet


@dataclass
class AnswerFoundException(Exception):
    operation_set: OperationSet


class NoImprovementException(Exception):
    MESSAGE = 'No improve'


class MaxDepthExceededException(Exception):
    MESSAGE = 'Max depth'


class MaxNodeExceededException(Exception):
    MESSAGE = 'Max node'


class TimeoutException(Exception):
    MESSAGE = 'Timeout'


@dataclass
class OperationInconsistencyException(Exception):
    message: str = ''
