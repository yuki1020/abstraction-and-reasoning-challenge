import cv2
import numpy as np
from dataclasses import dataclass
from typing import Optional, List

from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color
from abstraction_and_reasoning_challenge.src.domain.task.task import Task, ColorSelectedTask, MaskConvertedTask
from abstraction_and_reasoning_challenge.src.utils import nan_mean, mean


@dataclass
class ImageFeature:
    height: int
    width: int
    colors: List[Color]
    hit_and_miss_histogram: List[int]
    # most_common_color: Color
    vertical_edge_num: int
    horizontal_edge_num: int

    # TODO number of object
    # TODO number of color except for most_common_color


def create_image_feature(arr: np.ndarray) -> ImageFeature:
    return ImageFeature(
        height=arr.shape[0],
        width=arr.shape[1],
        colors=[Color.of(v) for v in ColorSelectionUtil().get_colors(arr)],
        hit_and_miss_histogram=calculate_hit_and_miss_histogram(arr),
        # most_common_color=ColorSelectionUtil().select_single_color(arr, SingleColorSelectionMode.MOST_COMMON),
        vertical_edge_num=np.count_nonzero(arr[1:] - arr[:-1]),  # faster than np.diff(arr, axis=0)
        horizontal_edge_num=np.count_nonzero(arr[:, 1:] - arr[:, :-1]),  # faster than np.diff(arr, axis=1)
    )


@dataclass
class ImageDiffFeature:
    dim_height_increase: int
    dim_width_increase: int
    dim_height_integer_multiple: bool
    dim_width_integer_multiple: bool
    dim_height_diff: int
    dim_width_diff: int
    dim_height_equal: bool
    dim_width_equal: bool
    lack_color_num: int
    excess_color_num: int
    hit_and_miss_histogram_diff: int
    # vertical_diff_input_arr_line_num: Optional[int]
    # horizontal_diff_input_arr_line_num: Optional[int]
    # vertical_diff_output_arr_line_num: Optional[int]
    # horizontal_diff_output_arr_line_num: Optional[int]
    vertical_edge_sum_diff: int
    horizontal_edge_sum_diff: int
    vertical_edge_sum_diff_ratio: float
    horizontal_edge_sum_diff_ratio: float
    diff_color_cell_ratio: Optional[float]  # None if different image size.
    diff_cell_where_no_need_to_change_count_ratio: Optional[float]  # None if different image size.
    wrong_change_cell_where_need_to_change_count_ratio: Optional[float]  # None if different image size.

    # TODO cell_diff_num_except_for_most_common_color

    def same_dim(self) -> bool:
        return self.dim_height_equal and self.dim_width_equal


def create_image_diff_feature(original_input_arr: np.ndarray, input_arr: np.ndarray, output_arr: np.ndarray) -> ImageDiffFeature:
    util = FeatureUtil()
    in_feature = create_image_feature(input_arr)
    out_feature = create_image_feature(output_arr)
    return ImageDiffFeature(
        dim_height_increase=out_feature.height - in_feature.height,
        dim_width_increase=out_feature.width - in_feature.width,
        dim_height_integer_multiple=(out_feature.height / in_feature.height).is_integer() or (in_feature.height / out_feature.height).is_integer(),
        dim_width_integer_multiple=(out_feature.width / in_feature.width).is_integer() or (in_feature.width / out_feature.width).is_integer(),
        dim_height_diff=abs(out_feature.height - in_feature.height),
        dim_width_diff=abs(out_feature.width - in_feature.width),
        dim_height_equal=out_feature.height == in_feature.height,
        dim_width_equal=out_feature.width == in_feature.width,
        lack_color_num=len(set(out_feature.colors) - set(in_feature.colors)),
        excess_color_num=len(set(in_feature.colors) - set(out_feature.colors)),
        hit_and_miss_histogram_diff=sum(abs(i_c - o_c) for i_c, o_c in zip(in_feature.hit_and_miss_histogram, out_feature.hit_and_miss_histogram)),
        # vertical_diff_input_arr_line_num=util._vertical_diff_input_arr_line_num(input_arr, output_arr),
        # horizontal_diff_input_arr_line_num=util._horizontal_diff_input_arr_line_num(input_arr, output_arr),
        # vertical_diff_output_arr_line_num=util._vertical_diff_output_arr_line_num(input_arr, output_arr),
        # horizontal_diff_output_arr_line_num=util._horizontal_diff_output_arr_line_num(input_arr, output_arr),
        vertical_edge_sum_diff=abs(out_feature.vertical_edge_num - in_feature.vertical_edge_num),
        horizontal_edge_sum_diff=abs(out_feature.horizontal_edge_num - in_feature.horizontal_edge_num),
        vertical_edge_sum_diff_ratio=abs(out_feature.vertical_edge_num - in_feature.vertical_edge_num) / in_feature.width,
        horizontal_edge_sum_diff_ratio=abs(out_feature.horizontal_edge_num - in_feature.horizontal_edge_num) / in_feature.height,
        diff_color_cell_ratio=util._diff_cell_count_ratio(input_arr, output_arr),
        diff_cell_where_no_need_to_change_count_ratio=util._diff_cell_where_no_need_to_change_count_ratio(original_input_arr, input_arr, output_arr),
        wrong_change_cell_where_need_to_change_count_ratio=util._wrong_change_cell_where_need_to_change_count_ratio(original_input_arr, input_arr, output_arr)
    )


@dataclass
class TaskFeature:
    # image_diff_features: List[ImageDiffFeature]
    same_dim_between_input_output: bool
    same_height_dim_between_input_output: bool
    same_width_dim_between_input_output: bool
    all_dim_height_increased: bool
    all_dim_height_decreased: bool
    all_dim_width_increased: bool
    all_dim_width_decreased: bool
    all_dim_height_integer_multiple: bool
    all_dim_width_integer_multiple: bool
    mean_lack_color_num: float
    mean_excess_color_num: float
    mean_hit_and_miss_histogram_diff: float
    # mean_vertical_diff_input_arr_line_num: Optional[float]
    # mean_horizontal_diff_input_arr_line_num: Optional[float]
    # mean_vertical_diff_output_arr_line_num: Optional[float]
    # mean_horizontal_diff_output_arr_line_num: Optional[float]
    mean_vertical_edge_sum_diff: float
    mean_horizontal_edge_sum_diff: float
    mean_vertical_edge_sum_diff_ratio: float
    mean_horizontal_edge_sum_diff_ratio: float
    mean_diff_color_cell_ratio: Optional[float]  # None if different image size.
    mean_diff_cell_where_no_need_to_change_count_ratio: Optional[float]  # None if different image size.
    mean_wrong_change_cell_where_need_to_change_count_ratio: Optional[float]  # None if different image size.

    # all_same_dim: bool
    # input_same_dim: bool
    # output_same_dim: bool
    # input_color_num: int
    # output_color_num: int
    # all_color_num: int


def create_task_feature(original_task: Task, task: Task) -> TaskFeature:
    diff_features = [create_image_diff_feature(o_io.input_arr, io.input_arr, io.output_arr) for o_io, io in zip(original_task.train, task.train)]
    return TaskFeature(
        # image_diff_features=image_diff_features,
        same_dim_between_input_output=all(f.same_dim() for f in diff_features),
        same_height_dim_between_input_output=all(f.dim_height_equal for f in diff_features),
        same_width_dim_between_input_output=all(f.dim_width_equal for f in diff_features),
        all_dim_height_increased=all(f.dim_height_increase > 0 for f in diff_features),
        all_dim_height_decreased=all(f.dim_height_increase < 0 for f in diff_features),
        all_dim_width_increased=all(f.dim_width_increase > 0 for f in diff_features),
        all_dim_width_decreased=all(f.dim_width_increase < 0 for f in diff_features),
        all_dim_height_integer_multiple=all(f.dim_height_integer_multiple for f in diff_features),
        all_dim_width_integer_multiple=all(f.dim_width_integer_multiple for f in diff_features),
        mean_lack_color_num=mean([f.lack_color_num for f in diff_features]),
        mean_excess_color_num=mean([f.excess_color_num for f in diff_features]),
        mean_hit_and_miss_histogram_diff=mean([f.hit_and_miss_histogram_diff for f in diff_features]),
        # mean_vertical_diff_input_arr_line_num=nan_mean(f.vertical_diff_input_arr_line_num for f in diff_features),
        # mean_horizontal_diff_input_arr_line_num=nan_mean(f.horizontal_diff_input_arr_line_num for f in diff_features),
        # mean_vertical_diff_output_arr_line_num=nan_mean(f.vertical_diff_output_arr_line_num for f in diff_features),
        # mean_horizontal_diff_output_arr_line_num=nan_mean(f.horizontal_diff_output_arr_line_num for f in diff_features),
        mean_vertical_edge_sum_diff=mean([f.vertical_edge_sum_diff for f in diff_features]),
        mean_horizontal_edge_sum_diff=mean([f.horizontal_edge_sum_diff for f in diff_features]),
        mean_vertical_edge_sum_diff_ratio=mean([f.vertical_edge_sum_diff_ratio for f in diff_features]),
        mean_horizontal_edge_sum_diff_ratio=mean([f.horizontal_edge_sum_diff_ratio for f in diff_features]),
        mean_diff_color_cell_ratio=nan_mean(f.diff_color_cell_ratio for f in diff_features),
        mean_diff_cell_where_no_need_to_change_count_ratio=nan_mean(f.diff_cell_where_no_need_to_change_count_ratio for f in diff_features),
        mean_wrong_change_cell_where_need_to_change_count_ratio=nan_mean(f.wrong_change_cell_where_need_to_change_count_ratio for f in diff_features),
    )


@dataclass
class ColorSelectedTaskFeature:
    task_feature: TaskFeature


def create_color_selected_task_feature(original_task: Task, color_selected_task: ColorSelectedTask, task_feature: TaskFeature = None) -> ColorSelectedTaskFeature:
    if task_feature is None:
        task_feature = create_task_feature(original_task, color_selected_task)
    return ColorSelectedTaskFeature(task_feature)


@dataclass
class MaskConvertedTaskFeature:
    task_feature: TaskFeature
    possible_improve_ratios: List[Optional[float]]


def create_mask_conversion_task_feature(original_task: Task, mask_converted_task: MaskConvertedTask, task_feature: TaskFeature = None) -> MaskConvertedTaskFeature:
    if task_feature is None:
        task_feature = create_task_feature(original_task, mask_converted_task)
    possible_improve_ratios = [_calculate_possible_improve_ratio(io.input_arr, io.output_arr, m) for io, m in zip(mask_converted_task.train, mask_converted_task.train_masks)]
    return MaskConvertedTaskFeature(
        task_feature=task_feature,
        possible_improve_ratios=possible_improve_ratios,
    )


def _calculate_possible_improve_ratio(input_arr: np.ndarray, output_arr: np.ndarray, mask: np.ndarray) -> Optional[float]:
    if input_arr.shape != output_arr.shape:
        return None
    diff_arr = np.not_equal(input_arr, output_arr)
    if not diff_arr.all():
        return 1.0
    else:
        selected_diff_arr = np.logical_and(diff_arr, mask)
        return 1 - selected_diff_arr.sum() / diff_arr.sum()


class FeatureUtil:
    def _horizontal_diff_input_arr_line_num(self, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if input_arr.shape[1] != output_arr.shape[1]:
            return None
        return abs(input_arr.shape[0] - np.array([(output_arr == h_l).all(axis=1) for h_l in input_arr]).any(axis=1).sum())

    def _horizontal_diff_output_arr_line_num(self, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if input_arr.shape[1] != output_arr.shape[1]:
            return None
        return abs(output_arr.shape[0] - np.array([(output_arr == h_l).all(axis=1) for h_l in input_arr]).any(axis=0).sum())

    def _vertical_diff_input_arr_line_num(self, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if input_arr.shape[0] != output_arr.shape[0]:
            return None
        return self._horizontal_diff_input_arr_line_num(input_arr.T, output_arr.T)

    def _vertical_diff_output_arr_line_num(self, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if input_arr.shape[0] != output_arr.shape[0]:
            return None
        return self._horizontal_diff_output_arr_line_num(input_arr.T, output_arr.T)

    def _diff_cell_count_ratio(self, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if input_arr.shape != output_arr.shape:
            return None
        diff_arr = np.not_equal(input_arr, output_arr)
        return diff_arr.sum() / diff_arr.size

    def _diff_cell_where_no_need_to_change_count_ratio(self, original_input_arr: np.ndarray, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if not original_input_arr.shape == input_arr.shape == output_arr.shape:
            return None
        no_need_to_change_mask = np.equal(original_input_arr, output_arr)
        diff_cell = np.not_equal(input_arr, output_arr)
        diff_cell_where_no_need_to_change = diff_cell[no_need_to_change_mask]
        return diff_cell_where_no_need_to_change.sum() / original_input_arr.size

    def _wrong_change_cell_where_need_to_change_count_ratio(self, original_input_arr: np.ndarray, input_arr: np.ndarray, output_arr: np.ndarray) -> Optional[float]:
        if not original_input_arr.shape == input_arr.shape == output_arr.shape:
            return None
        need_to_change_mask = np.not_equal(original_input_arr, output_arr)
        change_mask = np.not_equal(original_input_arr, input_arr)
        wrong_mask = np.not_equal(input_arr, output_arr)

        wrong_change_cell_where_need_to_change_mask = need_to_change_mask & change_mask & wrong_mask

        return wrong_change_cell_where_need_to_change_mask.sum() / original_input_arr.size


# @lru_cache(maxsize=1)
def get_hit_and_miss_kernels():
    return [
        # right top
        np.array([
            [0, -1, -1],
            [1, 1, -1],
            [0, 1, 0],
        ], dtype=np.int8),
        # right bottom
        np.array([
            [0, 1, 0],
            [1, 1, -1],
            [0, -1, -1],
        ], dtype=np.int8),
        # left bottom
        np.array([
            [0, 1, 0],
            [-1, 1, 1],
            [-1, -1, 0],
        ], dtype=np.int8),
        # left top
        np.array([
            [-1, -1, 0],
            [-1, 1, 1],
            [0, 1, 0],
        ], dtype=np.int8),

        # right protrusion
        np.array([
            [0, -1, -1],
            [0, 1, -1],
            [0, -1, -1],
        ], dtype=np.int8),
        # bottom protrusion
        np.array([
            [0, 0, 0],
            [-1, 1, -1],
            [-1, -1, -1],
        ], dtype=np.int8),
        # left protrusion
        np.array([
            [-1, -1, 0],
            [-1, 1, 0],
            [-1, -1, 0],
        ], dtype=np.int8),
        # top protrusion
        np.array([
            [-1, -1, -1],
            [-1, 1, -1],
            [0, 0, 0],
        ], dtype=np.int8),
        # TODO implement others?
    ]


def calculate_hit_and_miss_histogram(arr: np.ndarray):
    kernels = get_hit_and_miss_kernels()

    exist_colors = np.unique(arr)

    counts = []
    for color in range(10):
        if color not in exist_colors:
            for k in kernels:
                counts.append(0)
        else:
            for k in kernels:
                color_hit = (arr == color).astype(np.uint8)
                hit_and_miss_result = cv2.morphologyEx(color_hit, cv2.MORPH_HITMISS, k)
                counts.append(int(hit_and_miss_result.sum()))

    # counts = []
    # for operation_elements in kernels:
    #     for color in range(10):
    #         if color not in exist_colors:
    #             counts.append(0)
    #         else:
    #             color_hit = (arr == color).astype(np.uint8)
    #             hit_and_miss_result = cv2.morphologyEx(color_hit, cv2.MORPH_HITMISS, operation_elements)
    #             counts.append(int(hit_and_miss_result.sum()))

    return counts
