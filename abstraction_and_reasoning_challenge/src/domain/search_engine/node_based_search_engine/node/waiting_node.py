from dataclasses import dataclass, asdict
from typing import Dict, Any

from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import TaskFeature, ColorSelectedTaskFeature, MaskConvertedTaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet, UniformOperation, ColorSelection, MaskConversion, MaskOperation, ColorChannelSelection, ChannelMergeOperation, PartitionSelection, PartitionMergeOperation
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import WaitingNode
from abstraction_and_reasoning_challenge.src.domain.task.task import Task, ColorSelectedTask, MaskConvertedTask, ColorChannelSelectedTask, ColorChannelMaskConvertedTask, PartitionSelectionTask


@dataclass
class UniformOperationWaitingNode(WaitingNode):
    original_task: Task
    task: Task
    task_feature: TaskFeature
    base_operation_set: OperationSet
    next_operation: UniformOperation

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, next_ope: {self.next_operation}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)

    def evaluation_features(self) -> Dict[str, Any]:
        return {
            'node_class': self.__class__.__name__,
            'depth': len(self.base_operation_set.operations),
            **asdict(self.task_feature),
            'next_operation': self.next_operation.__class__.__name__,
            **asdict(self.next_operation)
        }


@dataclass
class ColorSelectionWaitingNode(WaitingNode):
    original_task: Task
    task: Task
    task_feature: TaskFeature
    base_operation_set: OperationSet
    next_selection: ColorSelection

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, next_selection: {self.next_selection}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)

    def evaluation_features(self) -> Dict[str, Any]:
        return {
            'node_class': self.__class__.__name__,
            'depth': len(self.base_operation_set.operations),
            **asdict(self.task_feature),
            'next_selection': self.next_selection.__class__.__name__,
            **asdict(self.next_selection)
        }


@dataclass
class MaskConversionWaitingNode(WaitingNode):
    original_task: Task
    color_selected_task: ColorSelectedTask
    color_selected_task_feature: ColorSelectedTaskFeature
    base_operation_set: OperationSet
    color_selection: ColorSelection
    next_mask_conversion: MaskConversion

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, color_selection: {self.color_selection}, next_add_selection: {self.next_mask_conversion}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)

    def evaluation_features(self) -> Dict[str, Any]:
        return {
            'node_class': self.__class__.__name__,
            'depth': len(self.base_operation_set.operations),
            **asdict(self.color_selected_task_feature.task_feature),
            'next_mask_conversion': self.next_mask_conversion.__class__.__name__,
            **asdict(self.next_mask_conversion)
        }


@dataclass
class MaskOperationSelectionWaitingNode(WaitingNode):
    original_task: Task
    mask_converted_task: MaskConvertedTask
    mask_converted_task_feature: MaskConvertedTaskFeature
    base_operation_set: OperationSet
    color_selection: ColorSelection
    mask_conversion: MaskConversion
    next_mask_operation: MaskOperation

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, color_selection: {self.color_selection}, add_selection: {self.mask_conversion}, next_mask_ope: {self.next_mask_operation}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)

    def evaluation_features(self) -> Dict[str, Any]:
        return {
            'node_class': self.__class__.__name__,
            'depth': len(self.base_operation_set.operations),
            **asdict(self.mask_converted_task_feature.task_feature),
            'next_mask_operation': self.next_mask_operation.__class__.__name__,
            **asdict(self.next_mask_operation)
        }


@dataclass
class ColorChannelSelectionOperationWaitingNode(WaitingNode):
    original_task: Task
    task: Task
    task_feature: TaskFeature
    base_operation_set: OperationSet
    next_color_channel_selection: ColorChannelSelection

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, next_color_channeling: {self.next_color_channel_selection}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)

    def evaluation_features(self) -> Dict[str, Any]:
        return {
            'node_class': self.__class__.__name__,
            'depth': len(self.base_operation_set.operations),
            **asdict(self.task_feature),
            'next_operation': self.next_color_channel_selection.__class__.__name__,
            **asdict(self.next_color_channel_selection)
        }


@dataclass
class ColorChannelMaskConversionWaitingNode(WaitingNode):
    original_task: Task
    task: ColorChannelSelectedTask
    task_feature: TaskFeature
    base_operation_set: OperationSet
    color_channel_selection: ColorChannelSelection
    next_mask_conversion: MaskConversion

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, color_channel_selection: {self.color_channel_selection}, next_mask_conversion: {self.next_mask_conversion}, next_mask_ope: {self.next_mask_conversion}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)


@dataclass
class ColorChannelMergeWaitingNode(WaitingNode):
    original_task: Task
    task: ColorChannelMaskConvertedTask
    task_feature: TaskFeature
    base_operation_set: OperationSet
    color_channel_selection: ColorChannelSelection
    mask_conversion: MaskConversion
    next_merge_operation: ChannelMergeOperation

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, color_channel_selection: {self.color_channel_selection}, mask_conversion: {self.mask_conversion}, next_merge_operation: {self.next_merge_operation}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)


@dataclass
class PartitionSelectionWaitingNode(WaitingNode):
    original_task: Task
    task: Task
    task_feature: TaskFeature
    base_operation_set: OperationSet
    next_partition_selection: PartitionSelection

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, next_partition_sel: {self.next_partition_selection}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)


@dataclass
class PartitionMergeWaitingNode(WaitingNode):
    original_task: Task
    task: PartitionSelectionTask
    task_feature: TaskFeature
    base_operation_set: OperationSet
    partition_selection: PartitionSelection
    next_partition_merge_operation: PartitionMergeOperation

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}+1, class: {self.__class__.__name__}, ' \
               f'ope_set: {self.base_operation_set}, partition_sel: {self.partition_selection}, partition_merge: {self.next_partition_merge_operation}'

    def depth(self) -> int:
        return len(self.base_operation_set.operations)
