import numpy as np
from dataclasses import dataclass
from itertools import chain

from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import TaskFeature, ColorSelectedTaskFeature, MaskConvertedTaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet, ColorSelection, MaskConversion, ColorChannelSelection, PartitionSelection
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import CompletedNode
from abstraction_and_reasoning_challenge.src.domain.task.task import Task, ColorSelectedTask, MaskConvertedTask, ColorChannelSelectedTask, ColorChannelMaskConvertedTask, PartitionSelectionTask


@dataclass
class UniformOperationCompletedNode(CompletedNode):
    original_task: Task
    task: Task
    task_feature: TaskFeature
    base_operation_set: OperationSet

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}, class: {self.__class__.__name__}, ope_set: {self.base_operation_set}'

    def train_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(map(lambda io: np_to_str(io.input_arr), self.task.train)))

    def all_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(map(lambda io: np_to_str(io.input_arr), self.task.train + self.task.test)))


@dataclass
class ColorSelectionCompletedNode(CompletedNode):
    original_task: Task
    color_selected_task: ColorSelectedTask
    color_selected_task_feature: ColorSelectedTaskFeature
    base_operation_set: OperationSet
    color_selection: ColorSelection

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}, class: {self.__class__.__name__}, ' \
               f'base_ope: {self.base_operation_set}, color_sele: {self.color_selection}'

    def train_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.color_selected_task.train),
                        map(lambda t: np_to_str(t), self.color_selected_task.train_masks)
                    )))

    def all_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.color_selected_task.train + self.color_selected_task.test),
                        map(lambda t: np_to_str(t), self.color_selected_task.train_masks + self.color_selected_task.test_masks)
                    )))


@dataclass
class MaskConversionCompletedNode(CompletedNode):
    original_task: Task
    mask_converted_task: MaskConvertedTask
    mask_converted_task_feature: MaskConvertedTaskFeature
    base_operation_set: OperationSet
    color_selection: ColorSelection
    mask_conversion: MaskConversion

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}, class: {self.__class__.__name__}, ' \
               f'base_ope: {self.base_operation_set}, color_sele: {self.color_selection}, add_sele: {self.mask_conversion}'

    def train_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.mask_converted_task.train),
                        map(lambda t: np_to_str(t), self.mask_converted_task.train_masks)
                    )))

    def all_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.mask_converted_task.train + self.mask_converted_task.test),
                        map(lambda t: np_to_str(t), self.mask_converted_task.train_masks + self.mask_converted_task.test_masks)
                    )))


@dataclass
class ColorChannelSelectionCompletedNode(CompletedNode):
    original_task: Task
    task: ColorChannelSelectedTask
    feature: TaskFeature
    base_operation_set: OperationSet
    color_channel_selection: ColorChannelSelection

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}, class: {self.__class__.__name__}, ' \
               f'base_ope: {self.base_operation_set}, color_sele: {self.color_channel_selection}'

    def train_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.task.train),
                        chain.from_iterable([(to_bytes(c), np_to_str(m)) for p_l in self.task.train_color_mask_pairs for c, m in p_l]),
                    )))

    def all_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.task.test),
                        chain.from_iterable([(to_bytes(c), np_to_str(m)) for p_l in self.task.train_color_mask_pairs + self.task.test_color_mask_pairs for c, m in p_l]),
                    )))


@dataclass
class ColorChannelMaskConversionCompletedNode(CompletedNode):
    original_task: Task
    task: ColorChannelMaskConvertedTask
    feature: TaskFeature
    base_operation_set: OperationSet
    color_selection: ColorChannelSelection
    mask_conversion: MaskConversion

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}, class: {self.__class__.__name__}, ' \
               f'base_ope: {self.base_operation_set}, mask_conversion: {self.mask_conversion}'

    def train_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.task.train),
                        chain.from_iterable([(to_bytes(c), np_to_str(m)) for p_l in self.task.train_original_color_mask_pairs for c, m in p_l]),
                        chain.from_iterable([(to_bytes(c), np_to_str(m)) for p_l in self.task.train_color_mask_pairs for c, m in p_l]),
                    )))

    def all_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.task.test),
                        chain.from_iterable([(to_bytes(c), np_to_str(m)) for p_l in self.task.train_original_color_mask_pairs + self.task.test_original_color_mask_pairs for c, m in p_l]),
                        chain.from_iterable([(to_bytes(c), np_to_str(m)) for p_l in self.task.train_color_mask_pairs + self.task.test_color_mask_pairs for c, m in p_l]),
                    )))


@dataclass
class PartitionSelectionCompletedNode(CompletedNode):
    original_task: Task
    task: PartitionSelectionTask
    feature: TaskFeature
    base_operation_set: OperationSet
    partition_selection: PartitionSelection

    def __str__(self):
        return f'depth: {len(self.base_operation_set.operations)}, class: {self.__class__.__name__}, ' \
               f'base_ope: {self.base_operation_set}, partition_selection: {self.partition_selection}'

    def train_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.task.train),
                        [to_bytes(v) for v in self.task.train_partitioned_arrays_original_location_masks]),
                    ))

    def all_arr_hash(self) -> int:
        return hash(bytes(self.__class__.__name__, encoding='utf-8') +
                    b'_'.join(chain(
                        map(lambda io: np_to_str(io.input_arr), self.task.test),
                        [to_bytes(v) for v in self.task.train_partitioned_arrays_original_location_masks + self.task.test_partitioned_arrays_original_location_masks]),
                    ))


def np_to_str(arr: np.ndarray) -> bytes:
    return arr.tostring()


def to_bytes(obj):
    return bytes(str(obj), encoding='utf-8')
