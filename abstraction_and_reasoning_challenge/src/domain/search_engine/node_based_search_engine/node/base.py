from dataclasses import dataclass
from typing import Dict, Any


class Node:

    def __repr__(self):
        return str(self)


@dataclass
class WaitingNode(Node):
    # This node will be added to priority queue.

    parent_completed_node: 'CompletedNode'
    cache_pred_distance = None

    def evaluation_features(self) -> Dict[str, Any]:
        raise NotImplementedError()

    def depth(self) -> int:
        raise NotImplementedError()

    def __le__(self, other: 'WaitingNode') -> bool:
        return self.cache_pred_distance <= other.cache_pred_distance

    def __lt__(self, other: 'WaitingNode') -> bool:
        return self.cache_pred_distance < other.cache_pred_distance


@dataclass()
class CompletedNode(Node):
    # This node won't be added to priority queue. This is processed immediately and converted to next List[WaitingNode].

    parent_waiting_node: 'WaitingNode'

    def train_arr_hash(self) -> int:
        raise NotImplementedError()

    def all_arr_hash(self) -> int:
        raise NotImplementedError()

# TODO should define RootNode?
