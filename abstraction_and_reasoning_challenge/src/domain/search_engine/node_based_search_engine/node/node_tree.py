import copy

from dataclasses import dataclass
from typing import List

from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet, UniformOperation, ColorOperation, MultiColorChannelOperation, PartitionOperation
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import CompletedNode, WaitingNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.completed_node import UniformOperationCompletedNode, ColorSelectionCompletedNode, MaskConversionCompletedNode, ColorChannelSelectionCompletedNode, ColorChannelMaskConversionCompletedNode, \
    PartitionSelectionCompletedNode


@dataclass(frozen=True)
class NodeTree:
    completed_nodes: List[CompletedNode]

    def __str__(self):
        return '\n'.join(map(str, self.completed_nodes))

    @classmethod
    def of(cls, completed_node: CompletedNode) -> 'NodeTree':
        completed_nodes = []

        current_node = completed_node
        while True:
            if isinstance(current_node, CompletedNode):
                completed_nodes.append(current_node)
                current_node = current_node.parent_waiting_node
            elif isinstance(current_node, WaitingNode):
                current_node = current_node.parent_completed_node
            elif current_node is None:
                # TODO root node
                break
            else:
                raise NotImplementedError()
        return cls(list(reversed(completed_nodes)))

    @classmethod
    def replaced_new_node_tree(cls, node_tree: 'NodeTree', node_depth: int, node: CompletedNode) -> 'NodeTree':
        copied_list = copy.copy(node_tree.completed_nodes)
        copied_list[node_depth] = node
        return cls(copied_list)

    def to_operation_set(self) -> OperationSet:
        # TODO found a bug related to MultiColorChannelOperation.
        try:
            operations = []
            temp_color_selection = None
            temp_mask_conversion = None
            temp_color_channel_selection = None
            temp_partition_selection = None

            # TODO too dirty.

            assert len(self.completed_nodes[0].base_operation_set.operations) == 0, self.completed_nodes[0]
            for n in self.completed_nodes[1:]:  # first element is root.
                if isinstance(n, UniformOperationCompletedNode):
                    if isinstance(n.base_operation_set.operations[-1], UniformOperation):
                        operations.append(n.base_operation_set.operations[-1])
                    else:
                        if temp_color_selection is not None:
                            operations.append(ColorOperation(temp_color_selection, temp_mask_conversion, n.base_operation_set.operations[-1].mask_operation))
                            temp_color_selection = None
                            temp_mask_conversion = None
                            temp_color_channel_selection = None
                            temp_partition_selection = None
                        elif temp_color_channel_selection is not None:
                            operations.append(MultiColorChannelOperation(temp_color_channel_selection, temp_mask_conversion, n.base_operation_set.operations[-1].channel_merge_operation))
                            temp_color_selection = None
                            temp_mask_conversion = None
                            temp_color_channel_selection = None
                            temp_partition_selection = None
                        elif temp_partition_selection is not None:
                            operations.append(PartitionOperation(temp_partition_selection, n.base_operation_set.operations[-1].partition_merge_operation))
                            temp_color_selection = None
                            temp_mask_conversion = None
                            temp_color_channel_selection = None
                            temp_partition_selection = None
                        else:
                            raise NotImplementedError()
                elif isinstance(n, ColorSelectionCompletedNode):
                    temp_color_selection = n.color_selection
                elif isinstance(n, MaskConversionCompletedNode):
                    temp_mask_conversion = n.mask_conversion
                elif isinstance(n, ColorChannelMaskConversionCompletedNode):
                    temp_mask_conversion = n.mask_conversion
                elif isinstance(n, ColorChannelSelectionCompletedNode):
                    temp_color_channel_selection = n.color_channel_selection
                elif isinstance(n, PartitionSelectionCompletedNode):
                    temp_partition_selection = n.partition_selection
                else:
                    raise ValueError()

            return OperationSet(operations)
        except Exception as e:
            print(f'error: {e}')
        return OperationSet([])

    def waiting_nodes(self) -> List[WaitingNode]:
        return list(filter(lambda n: n is not None, map(lambda n: n.parent_waiting_node, self.completed_nodes)))
