from typing import Union

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import create_task_feature, create_color_selected_task_feature, create_mask_conversion_task_feature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet, ColorOperation, MultiColorChannelOperation, PartitionOperation
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import CompletedNode, WaitingNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.completed_node import UniformOperationCompletedNode, ColorSelectionCompletedNode, MaskConversionCompletedNode, ColorChannelSelectionCompletedNode, ColorChannelMaskConversionCompletedNode, \
    PartitionSelectionCompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.waiting_node import UniformOperationWaitingNode, ColorSelectionWaitingNode, MaskConversionWaitingNode, MaskOperationSelectionWaitingNode, ColorChannelSelectionOperationWaitingNode, \
    ColorChannelMaskConversionWaitingNode, \
    ColorChannelMergeWaitingNode, PartitionSelectionWaitingNode, PartitionMergeWaitingNode
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.domain.task.task import ColorSelectedTask, MaskConvertedTask, Task
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor, ColorSelectionExecutor, MaskConversionExecutor, MaskOperationExecutor, ColorChannelSelectionExecutor, ColorChannelMaskConversionSelectionExecutor, ColorChannelMergeExecutor, \
    PartitionSelectionExecutor, PartitionMergeExecutor


class WaitingNodeProcessor:

    def process(self, node: WaitingNode) -> Union[CompletedNode, OperationInconsistencyException]:
        mapping = {
            UniformOperationWaitingNode: UniformOperationWaitingNodeProcessor(),
            ColorSelectionWaitingNode: ColorSelectionWaitingNodeProcessor(),
            MaskConversionWaitingNode: MaskConversionWaitingNodeProcessor(),
            MaskOperationSelectionWaitingNode: MaskOperationSelectionWaitingNodeProcessor(),
            ColorChannelSelectionOperationWaitingNode: ColorChannelSelectionOperationWaitingNodeProcessor(),
            ColorChannelMaskConversionWaitingNode: ColorChannelMaskConversionWaitingNodeProcessor(),
            ColorChannelMergeWaitingNode: ColorChannelMergeWaitingNodeProcessor(),
            PartitionSelectionWaitingNode: PartitionSelectionWaitingNodeProcessor(),
            PartitionMergeWaitingNode: PartitionMergeWaitingNodeProcessor(),
        }

        try:
            processor = mapping[node.__class__]
            return processor.process(node)
        except OperationInconsistencyException as e:
            return e


class UniformOperationWaitingNodeProcessor:

    def process(self, node: UniformOperationWaitingNode) -> UniformOperationCompletedNode:
        new_task = TaskOperationSetExecutor().execute(node.task, OperationSet([node.next_operation]))
        if self.can_skip(node.task, new_task):
            raise OperationInconsistencyException(f'can skip')
        new_task_feature = create_task_feature(node.original_task, new_task)
        new_base_operation_set = OperationSet(node.base_operation_set.operations + [node.next_operation])

        return UniformOperationCompletedNode(node, node.original_task, new_task, new_task_feature, new_base_operation_set)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        # TODO use OperationInconsistencyException?
        if all(AnswerMatcher.is_match_arr(prev_io.input_arr, next_io.input_arr) for prev_io, next_io in zip(prev_task.train, next_task.train)):
            # no effect
            return True
        else:
            return False


class ColorSelectionWaitingNodeProcessor:

    def process(self, node: ColorSelectionWaitingNode) -> ColorSelectionCompletedNode:
        color_selected_task = ColorSelectionExecutor.execute(node.task, node.next_selection)
        if self.can_skip(color_selected_task):
            raise OperationInconsistencyException(f'can skip')
        color_selected_task_feature = create_color_selected_task_feature(node.original_task, color_selected_task, node.task_feature)
        return ColorSelectionCompletedNode(node, node.original_task, color_selected_task,
                                           color_selected_task_feature, node.base_operation_set, node.next_selection)

    def can_skip(self, color_selected_task: ColorSelectedTask) -> bool:
        # TODO use OperationInconsistencyException?
        if not any(m.any() for m in color_selected_task.train_masks):
            # if no mask was generated, skip.
            return True
        elif all(m.all() for m in color_selected_task.train_masks):
            # if mask covers all region, skip.
            return True
        else:
            return False


class MaskConversionWaitingNodeProcessor:

    def process(self, node: MaskConversionWaitingNode) -> MaskConversionCompletedNode:
        mask_converted_task = MaskConversionExecutor.execute(node.color_selected_task, node.next_mask_conversion)
        if self.can_skip(mask_converted_task):
            raise OperationInconsistencyException(f'can skip')
        mask_converted_task_feature = create_mask_conversion_task_feature(node.original_task, mask_converted_task, node.color_selected_task_feature.task_feature)

        return MaskConversionCompletedNode(node, node.original_task, mask_converted_task, mask_converted_task_feature,
                                           node.base_operation_set, node.color_selection, node.next_mask_conversion)

    def can_skip(self, mask_converted_task: MaskConvertedTask) -> bool:
        if not any(m.any() for m in mask_converted_task.train_masks):
            # if no mask was generated, skip.
            return True
        elif all(m.all() for m in mask_converted_task.train_masks):
            # if mask covers all region, skip.
            return True
        else:
            return False


class MaskOperationSelectionWaitingNodeProcessor:

    def process(self, node: MaskOperationSelectionWaitingNode) -> UniformOperationCompletedNode:
        new_task = MaskOperationExecutor.execute(node.mask_converted_task, node.next_mask_operation)
        if self.can_skip(node.mask_converted_task, new_task):
            raise OperationInconsistencyException(f'can skip')
        new_task_feature = create_task_feature(node.original_task, new_task)
        new_base_operation_set = OperationSet(node.base_operation_set.operations +
                                              [ColorOperation(node.color_selection, node.mask_conversion, node.next_mask_operation)])

        return UniformOperationCompletedNode(node, node.original_task, new_task, new_task_feature, new_base_operation_set)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        if all(AnswerMatcher.is_match_arr(prev_io.input_arr, next_io.input_arr) for prev_io, next_io in zip(prev_task.train, next_task.train)):
            # no effect
            return True
        else:
            return False


class ColorChannelSelectionOperationWaitingNodeProcessor:

    def process(self, node: ColorChannelSelectionOperationWaitingNode) -> ColorChannelSelectionCompletedNode:
        new_task = ColorChannelSelectionExecutor().execute(node.task, node.next_color_channel_selection)
        if self.can_skip(node.task, new_task):
            raise OperationInconsistencyException(f'can skip')

        # reuse old feature.
        return ColorChannelSelectionCompletedNode(node, node.original_task, new_task, node.task_feature, node.base_operation_set, node.next_color_channel_selection)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        # TODO imple
        return False


class ColorChannelMaskConversionWaitingNodeProcessor:

    def process(self, node: ColorChannelMaskConversionWaitingNode) -> ColorChannelMaskConversionCompletedNode:
        new_task = ColorChannelMaskConversionSelectionExecutor().execute(node.task, node.next_mask_conversion)
        if self.can_skip(node.task, new_task):
            raise OperationInconsistencyException(f'can skip')

        # reuse old feature.
        return ColorChannelMaskConversionCompletedNode(node, node.original_task, new_task, node.task_feature, node.base_operation_set, node.color_channel_selection, node.next_mask_conversion)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        # TODO imple
        return False


class ColorChannelMergeWaitingNodeProcessor:

    def process(self, node: ColorChannelMergeWaitingNode) -> UniformOperationCompletedNode:
        new_task = ColorChannelMergeExecutor.execute(node.task, node.next_merge_operation)
        if self.can_skip(node.task, new_task):
            raise OperationInconsistencyException(f'can skip')

        new_task_feature = create_task_feature(node.original_task, new_task)
        new_base_operation_set = OperationSet(node.base_operation_set.operations +
                                              [MultiColorChannelOperation(node.color_channel_selection, node.mask_conversion, node.next_merge_operation)])

        return UniformOperationCompletedNode(node, node.original_task, new_task, new_task_feature, new_base_operation_set)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        # TODO imple
        return False


class PartitionSelectionWaitingNodeProcessor:

    def process(self, node: PartitionSelectionWaitingNode) -> PartitionSelectionCompletedNode:
        new_task = PartitionSelectionExecutor().execute(node.task, node.next_partition_selection)
        if self.can_skip(node.task, new_task):
            raise OperationInconsistencyException(f'can skip')

        # reuse old feature.
        return PartitionSelectionCompletedNode(node, node.original_task, new_task, node.task_feature, node.base_operation_set, node.next_partition_selection)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        # TODO imple
        return False


class PartitionMergeWaitingNodeProcessor:
    def process(self, node: PartitionMergeWaitingNode) -> UniformOperationCompletedNode:
        new_task = PartitionMergeExecutor().execute(node.task, node.next_partition_merge_operation)

        if self.can_skip(node.task, new_task):
            raise OperationInconsistencyException(f'can skip')

        new_task_feature = create_task_feature(node.original_task, new_task)
        new_base_operation_set = OperationSet(node.base_operation_set.operations + [PartitionOperation(node.partition_selection, node.next_partition_merge_operation)])

        return UniformOperationCompletedNode(node, node.original_task, new_task, new_task_feature, new_base_operation_set)

    def can_skip(self, prev_task: Task, next_task: Task) -> bool:
        # TODO imple
        return False
