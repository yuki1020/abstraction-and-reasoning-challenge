from collections import defaultdict

from dataclasses import dataclass
from typing import Union

from abstraction_and_reasoning_challenge.config import RunConfig
from abstraction_and_reasoning_challenge.param_config import AllParameter
from abstraction_and_reasoning_challenge.src.domain.exceptions import NoImprovementException, TimeoutException, MaxNodeExceededException, OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import create_task_feature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.search_engine.engine.node_evaluation_schedule import get_schedule, NodeEvaluatorSchedules
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.handmade_evaluator import HandMadeNodeEvaluator, EvaluatorGuideByOneDepthEval
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.completed_node_processor import CompletedNodeProcessor
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.completed_node import UniformOperationCompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node_tree_processer import get_alternative_operation_sets
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.waiting_node_processor import WaitingNodeProcessor
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task_result import AnsweredSearchResult, NotAnsweredSearchResult, AnsweredSearchResults
from abstraction_and_reasoning_challenge.src.ml.operation_element_inclusion_prediction import predict_operation_element_inclusion
from abstraction_and_reasoning_challenge.src.priority_queue.priority_queue import PriorityQueue
from abstraction_and_reasoning_challenge.src.utils import Timer


@dataclass
class NodeBaseSearchEngine:
    MAX_NODE = 100000000
    answer_limit_num: int = 3

    def search(self, task: Task, params: AllParameter, verbose: bool = False) -> Union[AnsweredSearchResults, NotAnsweredSearchResult]:
        task_feature = create_task_feature(task, task)
        if RunConfig.USE_ML_GUIDE:
            operation_element_prob_dict = predict_operation_element_inclusion(task_feature)
        else:
            operation_element_prob_dict = defaultdict(lambda: 1)

        schedules: NodeEvaluatorSchedules = get_schedule(operation_element_prob_dict, params.node_base_engine_param, params.distance_evaluator_param)
        node_evaluator = schedules.pop_evaluator()

        root_node = UniformOperationCompletedNode(None, task, task, task_feature, OperationSet([]))
        first_waiting_nodes = CompletedNodeProcessor.process(root_node)
        node_evaluator.evaluate_nodes(first_waiting_nodes)
        zero_depth_pq = PriorityQueue([*first_waiting_nodes])
        pq = PriorityQueue([])
        zero_depth_completed_nodes = []
        zero_depth_operation_eval_dict = {}
        visited_node_hashes = defaultdict(dict)  # If same array is found, cache to save time.

        if verbose:
            print('search zero depth nodes')
        with Timer() as timer:
            for node_i in range(self.MAX_NODE):
                if len(zero_depth_pq) == 0:
                    break

                waiting_new_nodes = []
                for same_cost_node_i, waiting_node in enumerate(zero_depth_pq.pop_mins_or_as_least_n(params.node_base_engine_param.pq_pop_mins_or_as_least_n)):
                    completed_node = WaitingNodeProcessor().process(waiting_node)

                    if isinstance(completed_node, Exception):
                        if verbose:
                            print(f'skipped: {completed_node}')
                        continue

                    if isinstance(completed_node, UniformOperationCompletedNode):
                        zero_depth_completed_nodes.append(completed_node)
                        zero_depth_operation_eval_dict[completed_node.base_operation_set.operations[0]] = node_evaluator.evaluate_base_distance_for_completed_node(completed_node)
                        continue

                    temp_waiting_new_nodes = CompletedNodeProcessor.process(completed_node)
                    node_evaluator.evaluate_nodes(temp_waiting_new_nodes)

                    waiting_new_nodes += temp_waiting_new_nodes

                for n in waiting_new_nodes:
                    zero_depth_pq.push(n)

            one_depth_answer_operations = [k for k, v in zero_depth_operation_eval_dict.items() if v == 0]
            if one_depth_answer_operations:
                answers = []
                result_applied_tasks = []
                for o in one_depth_answer_operations:
                    try:
                        applied_task = TaskOperationSetExecutor().execute(task, OperationSet([o]))
                    except OperationInconsistencyException:
                        continue
                    if any(applied_task.test_arr_hash() == t.test_arr_hash() for t in result_applied_tasks):
                        continue
                    result_applied_tasks.append(applied_task)
                    answers.append(AnsweredSearchResult(OperationSet([o])))
                answers = answers[:3]
                return AnsweredSearchResults(task, answers, timer.second(), 0, node_i)
            zero_depth_search_time = timer.second()

        for completed_node in zero_depth_completed_nodes:
            train_node_hash = completed_node.train_arr_hash()
            all_node_hash = completed_node.all_arr_hash()
            if train_node_hash in visited_node_hashes:
                if verbose:
                    print(f'hash skipped. same node: {"_".join(map(str, (f"{k}:{v}" for k, v in visited_node_hashes[train_node_hash].items())))}')
                visited_node_hashes[train_node_hash][all_node_hash] = completed_node
                continue
            visited_node_hashes[train_node_hash][all_node_hash] = completed_node

            temp_waiting_new_nodes = CompletedNodeProcessor.process(completed_node)
            node_evaluator.evaluate_nodes(temp_waiting_new_nodes)
            for n in temp_waiting_new_nodes:
                pq.push(n)

        root_node_eval = node_evaluator.evaluate_base_distance_for_completed_node(root_node)
        if RunConfig.ONE_DEPTH_EVAL_GUIDE:
            evaluator_guide = EvaluatorGuideByOneDepthEval(root_node_eval, zero_depth_operation_eval_dict, default_value_for_missing=1)
            node_evaluator.evaluator_guide = evaluator_guide

        if verbose:
            print('search none-zero depth nodes')
        searched_total_node = 0
        with Timer() as timer:
            for node_i in range(self.MAX_NODE):
                if len(pq) == 0:
                    return NotAnsweredSearchResult(task, NoImprovementException(), timer.second(), searched_total_node)

                waiting_new_nodes = []
                for same_cost_node_i, waiting_node in enumerate(pq.pop_mins_or_as_least_n(params.node_base_engine_param.pq_pop_mins_or_as_least_n)):
                    if verbose:
                        print(f'total_node: {searched_total_node}, node: {node_i}_{same_cost_node_i}, pq_len: {len(pq)}, cost: {waiting_node.cache_pred_distance}, {waiting_node}')

                    searched_total_node += 1
                    completed_node = WaitingNodeProcessor().process(waiting_node)

                    if isinstance(completed_node, Exception):
                        if verbose:
                            print(f'skipped: {completed_node}')
                        continue

                    if isinstance(completed_node, UniformOperationCompletedNode):
                        if AnswerMatcher.is_train_all_match(completed_node.task):
                            answers = []
                            for t in get_alternative_operation_sets(task, completed_node, visited_node_hashes, verbose):
                                answers.append(AnsweredSearchResult(t.to_operation_set()))
                                if len(answers) == 3:
                                    break
                            return AnsweredSearchResults(task, answers, zero_depth_search_time, timer.second(), searched_total_node)

                    train_node_hash = completed_node.train_arr_hash()
                    all_node_hash = completed_node.all_arr_hash()
                    if train_node_hash in visited_node_hashes:
                        if verbose:
                            print(f'hash skipped. same node: {"_".join(map(str, (f"{k}:{v}" for k, v in visited_node_hashes[train_node_hash].items())))}')
                        visited_node_hashes[train_node_hash][all_node_hash] = completed_node
                        continue
                    visited_node_hashes[train_node_hash][all_node_hash] = completed_node

                    temp_waiting_new_nodes = CompletedNodeProcessor.process(completed_node)
                    node_evaluator.evaluate_nodes(temp_waiting_new_nodes)

                    waiting_new_nodes += temp_waiting_new_nodes

                    if timer.second() > schedules.timeout_sec():
                        return NotAnsweredSearchResult(task, TimeoutException(), timer.second(), searched_total_node)

                for n in waiting_new_nodes:
                    pq.push(n)

                if timer.second() > schedules.next_timing():
                    if verbose:
                        print('=========================== evaluator switch!!! ===========================')
                    node_evaluator = schedules.pop_evaluator()
                    if node_evaluator is None:
                        return NotAnsweredSearchResult(task, TimeoutException(), timer.second(), searched_total_node)
                    if RunConfig.ONE_DEPTH_EVAL_GUIDE:
                        node_evaluator.evaluator_guide = evaluator_guide
                    node_evaluator.evaluate_nodes(pq.heap)
                    pq.refresh()

        return NotAnsweredSearchResult(task, MaxNodeExceededException(), timer.second(), searched_total_node)
