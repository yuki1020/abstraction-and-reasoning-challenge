from itertools import product, chain
from typing import List
from typing import Union

from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import TaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import ColorSelection, MaskOperation, MaskConversion, ChannelMergeOperation, ColorChannelSelection, PartitionMergeOperation, PartitionSelection
from abstraction_and_reasoning_challenge.src.domain.operation.channel_merge_operation import ColorChannelOverrideOperation
from abstraction_and_reasoning_challenge.src.domain.operation.color_channel_selection import WithOutMostCommonColorChannelSelection
from abstraction_and_reasoning_challenge.src.domain.operation.color_selection import FixedSingleColorSelection, SingleColorSelection, MultiColorSelection
from abstraction_and_reasoning_challenge.src.domain.operation.image_to_int import CellNum, UniqueColorNum, UniqueColorNumMinusOne
from abstraction_and_reasoning_challenge.src.domain.operation.mask_conversion import HolesSelection, ConnectDotSelection, SquareObjectsSelection, NoMaskConversion, ContourSelection, SplitLineSelection, DotExistLineSelection, ObjectsMaxMinSelection, ObjectInnerSelection, \
    ContourOuterSelection, OldObjectsMaxMinSelection, ObjectsTouchingEdgeSelection
from abstraction_and_reasoning_challenge.src.domain.operation.mask_operation import MaskCoordsCrop, FixedColorMaskFill, SingleColorMaskFill
from abstraction_and_reasoning_challenge.src.domain.operation.partition_merge_operation import AnySelectionMerge, NotSelectionMerge, AllSelectionMerge, ModifiedXorSelectionMerge, NaturalArrayOrderedOverrideMerge, DiagonalArrayOrderedOverrideMerge, SpiralArrayOrderedOverrideMerge, \
    UniquelySelectedArrayExtraction, RestoreOnlySelectedArray, ExtractOneValueFromPartitionedArray, ExtractOneValueFromSelectedPartitionedArray
from abstraction_and_reasoning_challenge.src.domain.operation.partition_selection import IntegerDivisionPartition, LinePartition, GeneralizedLinePartition, ColorNumIntegerDivisionPartition, DynamicIntegerDivisionPartition
from abstraction_and_reasoning_challenge.src.domain.operation.partitioned_array_selection import UniqueColorNumberSelection, ColoredCellNumberSelection, SameShapeNumSelection, SymmetrySelection
from abstraction_and_reasoning_challenge.src.domain.operation.uniform_operation import Flip, Rotate, Padding, Resize, LineDeletion, DynamicResize, DynamicPadding, SmallResize, DynamicSmallResize
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color, SingleColorSelectionMode, MultiColorSelectionMode, PixelConnectivity, ObjectFeature, MaxOrMin, ImageEdgeType, HoleInclude, TrueOrFalse, Corner, SpiralDirection, BackGroundColorSelectionMode, AxisV2
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import FlipMode, Axis, Direction, PaddingMode
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import LineEdgeType, FillType
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import CompletedNode, WaitingNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.completed_node import UniformOperationCompletedNode, ColorSelectionCompletedNode, MaskConversionCompletedNode, ColorChannelSelectionCompletedNode, ColorChannelMaskConversionCompletedNode, \
    PartitionSelectionCompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.waiting_node import UniformOperationWaitingNode, ColorSelectionWaitingNode, MaskConversionWaitingNode, MaskOperationSelectionWaitingNode, ColorChannelMaskConversionWaitingNode, \
    ColorChannelMergeWaitingNode, \
    ColorChannelSelectionOperationWaitingNode, PartitionMergeWaitingNode, PartitionSelectionWaitingNode
from abstraction_and_reasoning_challenge.src.domain.task.task import Task


class CompletedNodeProcessor:

    @staticmethod
    def process(node: CompletedNode) -> List[WaitingNode]:
        mapping = {
            UniformOperationCompletedNode: OperationCompletedNodeProcessor,
            ColorSelectionCompletedNode: ColorSelectionCompletedNodeProcessor,
            MaskConversionCompletedNode: MaskConversionCompletedNodeProcessor,
            ColorChannelSelectionCompletedNode: ColorChannelSelectionCompletedNodeProcessor,
            ColorChannelMaskConversionCompletedNode: ColorChannelMaskConversionCompletedNodeProcessor,
            PartitionSelectionCompletedNode: PartitionSelectionCompletedNodeProcessor,
        }

        processor = mapping[node.__class__]
        return processor.process(node)


class OperationCompletedNodeProcessor:

    @classmethod
    def process(cls, node: UniformOperationCompletedNode) -> List[Union[UniformOperationWaitingNode, ColorSelectionWaitingNode, ColorChannelSelectionOperationWaitingNode]]:
        res = [
            *[UniformOperationWaitingNode(node, node.original_task, node.task, node.task_feature, node.base_operation_set, new_operation)
              for new_operation in cls._candidate_operations(node.task, node.task_feature)],
            *[ColorSelectionWaitingNode(node, node.original_task, node.task, node.task_feature, node.base_operation_set, color_selection)
              for color_selection in cls._candidate_color_selections(node.task)],
            *[ColorChannelSelectionOperationWaitingNode(node, node.original_task, node.task, node.task_feature, node.base_operation_set, color_channel_selection)
              for color_channel_selection in cls._candidate_color_channel_selection(node.task)],
        ]

        # first operation only
        if len(node.base_operation_set.operations) == 0:
            res.extend([PartitionSelectionWaitingNode(node, node.original_task, node.task, node.task_feature, node.base_operation_set, partition_selection)
                        for partition_selection in cls._candidate_partition_selection(node.task)])

        return res

    @staticmethod
    def _candidate_operations(task: Task, task_feature: TaskFeature):
        input_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_input_all_arr())))))

        candidates = []

        image_to_int_funcs = [*[CellNum(m) for m in BackGroundColorSelectionMode], UniqueColorNum(), UniqueColorNumMinusOne()]

        if task_feature.all_dim_height_increased:
            candidates += [DynamicResize(Axis.VERTICAL, f) for f in image_to_int_funcs]
            candidates += [Resize(Axis.VERTICAL, r) for r in range(2, 5)]
            candidates += [Padding(m, d, k) for m, d, k in product(PaddingMode, [Direction.TOP, Direction.BOTTOM], range(1, 4))]
            candidates += [DynamicPadding(m, d, f) for m, d, f in product(PaddingMode, [Direction.TOP, Direction.BOTTOM], image_to_int_funcs)]

        if task_feature.all_dim_width_increased:
            candidates += [DynamicResize(Axis.HORIZONTAL, f) for f in image_to_int_funcs]
            candidates += [Resize(Axis.HORIZONTAL, r) for r in range(2, 5)]
            candidates += [Padding(m, d, k) for m, d, k in product(PaddingMode, [Direction.LEFT, Direction.RIGHT], range(1, 4))]
            candidates += [DynamicPadding(m, d, f) for m, d, f in product(PaddingMode, [Direction.LEFT, Direction.RIGHT], image_to_int_funcs)]

        if task_feature.all_dim_height_decreased:
            candidates += [SmallResize(Axis.VERTICAL, r) for r in range(2, 5)]
            candidates += [DynamicSmallResize(Axis.VERTICAL, f) for f in image_to_int_funcs]

        if task_feature.all_dim_width_decreased:
            candidates += [SmallResize(Axis.HORIZONTAL, r) for r in range(2, 5)]
            candidates += [DynamicSmallResize(Axis.HORIZONTAL, f) for f in image_to_int_funcs]

        if task_feature.all_dim_height_decreased or task_feature.all_dim_width_decreased:
            candidates += [LineDeletion(c) for c in input_colors]

        candidates += [
            *[Flip(m) for m in FlipMode],
            *[Rotate(a) for a in [90, 180, 270]],
        ]

        return candidates

    @staticmethod
    def _candidate_color_selections(task: Task) -> List[ColorSelection]:
        input_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_input_all_arr())))))
        return [
            *[FixedSingleColorSelection(c) for c in input_colors],
            *[SingleColorSelection(m) for m in SingleColorSelectionMode],
            *[MultiColorSelection(m) for m in MultiColorSelectionMode],
        ]

    @staticmethod
    def _candidate_color_channel_selection(task: Task) -> List[ColorChannelSelection]:
        return [
            *[WithOutMostCommonColorChannelSelection(m) for m in BackGroundColorSelectionMode]
        ]

    @staticmethod
    def _candidate_partition_selection(task: Task) -> List[PartitionSelection]:
        input_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_input_all_arr())))))

        image_to_int_funcs = [*[CellNum(m) for m in BackGroundColorSelectionMode], UniqueColorNum(), UniqueColorNumMinusOne()]
        return [
            *[ColorNumIntegerDivisionPartition(axis=a) for a in Axis],
            *[IntegerDivisionPartition(axis=a, n_split=n) for a, n in product(Axis, range(2, 5))],
            *[DynamicIntegerDivisionPartition(axis=a, image_to_int_func=f) for a, f in product(Axis, image_to_int_funcs)],
            *[GeneralizedLinePartition(m) for m in BackGroundColorSelectionMode],
            *[LinePartition(line_color=c) for c in input_colors],
        ]


class ColorSelectionCompletedNodeProcessor:

    @classmethod
    def process(cls, node: ColorSelectionCompletedNode) -> List[MaskConversionWaitingNode]:
        return [MaskConversionWaitingNode(node, node.original_task, node.color_selected_task, node.color_selected_task_feature,
                                          node.base_operation_set, node.color_selection, mask_conversion)
                for mask_conversion in cls._candidate_mask_conversions()]

    @staticmethod
    def _candidate_mask_conversions() -> List[MaskConversion]:
        return [
            NoMaskConversion(),
            SquareObjectsSelection(),
            *[ObjectsTouchingEdgeSelection(tf, c) for tf, c in product(TrueOrFalse, PixelConnectivity)],
            *[ObjectsMaxMinSelection(tf, m, t, c) for tf, m, t, c in product(TrueOrFalse, MaxOrMin, ObjectFeature, PixelConnectivity)],
            OldObjectsMaxMinSelection(),
            *[SplitLineSelection(a) for a in Axis],
            *[DotExistLineSelection(a) for a in Axis],
            *[HolesSelection(c) for c in PixelConnectivity],
            *[ObjectInnerSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)],
            *[ContourSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)],
            *[ContourOuterSelection(c, h) for c, h in product(PixelConnectivity, HoleInclude)],
            *[ConnectDotSelection(a, e, f) for a, e, f in product(Axis, LineEdgeType, FillType)],
        ]


class MaskConversionCompletedNodeProcessor:

    @classmethod
    def process(cls, node: MaskConversionCompletedNode) -> List[MaskOperationSelectionWaitingNode]:
        return [MaskOperationSelectionWaitingNode(node, node.original_task, node.mask_converted_task, node.mask_converted_task_feature,
                                                  node.base_operation_set, node.color_selection, node.mask_conversion, mask_operation)
                for mask_operation in cls._candidate(node)]

    @staticmethod
    def _candidate(node: MaskConversionCompletedNode) -> List[MaskOperation]:
        # TODO use
        # color_mappings = set(chain.from_iterable(t.candidate_color_mapping() for t in task.train))

        output_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(node.mask_converted_task.get_output_all_arr())))))

        candidates = []

        if not node.mask_converted_task_feature.task_feature.same_dim_between_input_output:
            candidates += [MaskCoordsCrop()]

        candidates += [
            *[FixedColorMaskFill(c) for c in output_colors],
            *[SingleColorMaskFill(m) for m in SingleColorSelectionMode],
        ]

        return candidates


class ColorChannelSelectionCompletedNodeProcessor:

    @classmethod
    def process(cls, node: ColorChannelSelectionCompletedNode) -> List[ColorChannelMaskConversionWaitingNode]:
        return [ColorChannelMaskConversionWaitingNode(node, node.original_task, node.task, node.feature,
                                                      node.base_operation_set, node.color_channel_selection, mask_conversion)
                for mask_conversion in cls._candidate_mask_conversions()]

    @staticmethod
    def _candidate_mask_conversions() -> List[MaskConversion]:
        return [
            NoMaskConversion(),
            SquareObjectsSelection(),
            *[ObjectsTouchingEdgeSelection(tf, c) for tf, c in product(TrueOrFalse, PixelConnectivity)],
            *[ObjectsMaxMinSelection(tf, m, t, c) for tf, m, t, c in product(TrueOrFalse, MaxOrMin, ObjectFeature, PixelConnectivity)],
            OldObjectsMaxMinSelection(),
            *[SplitLineSelection(a) for a in Axis],
            *[DotExistLineSelection(a) for a in Axis],
            *[HolesSelection(c) for c in PixelConnectivity],
            *[ObjectInnerSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)],
            *[ContourSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)],
            *[ContourOuterSelection(c, h) for c, h in product(PixelConnectivity, HoleInclude)],
            *[ConnectDotSelection(a, e, f) for a, e, f in product(Axis, LineEdgeType, FillType)],
        ]


class ColorChannelMaskConversionCompletedNodeProcessor:

    @classmethod
    def process(cls, node: ColorChannelMaskConversionCompletedNode) -> List[ColorChannelMergeWaitingNode]:
        return [ColorChannelMergeWaitingNode(node, node.original_task, node.task, node.feature,
                                             node.base_operation_set, node.color_selection, node.mask_conversion, merge_operation)
                for merge_operation in cls._candidate()]

    @staticmethod
    def _candidate() -> List[ChannelMergeOperation]:
        return [ColorChannelOverrideOperation()]


class PartitionSelectionCompletedNodeProcessor:

    @classmethod
    def process(cls, node: PartitionSelectionCompletedNode) -> List[PartitionMergeWaitingNode]:
        return [PartitionMergeWaitingNode(node, node.original_task, node.task, node.feature,
                                          node.base_operation_set, node.partition_selection, c) for c in cls._candidate(node)]

    @staticmethod
    def _candidate(node: PartitionSelectionCompletedNode) -> List[PartitionMergeOperation]:
        output_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(node.task.get_output_all_arr())))))

        selections = [
            *[UniqueColorNumberSelection(m) for m in MaxOrMin],
            *[ColoredCellNumberSelection(m, bg) for m, bg in product(MaxOrMin, BackGroundColorSelectionMode)],
            *[SameShapeNumSelection(m) for m in MaxOrMin],
            *[SymmetrySelection(a, tf) for a, tf in product(AxisV2, TrueOrFalse)],
        ]

        return [
            *[AnySelectionMerge(m, c) for m, c in product(BackGroundColorSelectionMode, output_colors)],
            *[NotSelectionMerge(m, c) for m, c in product(BackGroundColorSelectionMode, output_colors)],
            *[AllSelectionMerge(m, c) for m, c in product(BackGroundColorSelectionMode, output_colors)],
            *[ModifiedXorSelectionMerge(m, c) for m, c in product(BackGroundColorSelectionMode, output_colors)],
            *[NaturalArrayOrderedOverrideMerge(m, c, a) for m, c, a in product(BackGroundColorSelectionMode, Corner, [Axis.VERTICAL, Axis.HORIZONTAL])],
            *[DiagonalArrayOrderedOverrideMerge(m, c, a) for m, c, a in product(BackGroundColorSelectionMode, Corner, [Axis.VERTICAL, Axis.HORIZONTAL])],
            *[SpiralArrayOrderedOverrideMerge(m, c, d) for m, c, d in product(BackGroundColorSelectionMode, Corner, SpiralDirection)],
            *[UniquelySelectedArrayExtraction(s) for s in selections],
            *[RestoreOnlySelectedArray(m, s) for m, s in product(BackGroundColorSelectionMode, selections)],
            *[ExtractOneValueFromPartitionedArray(m) for m in SingleColorSelectionMode],
            *[ExtractOneValueFromSelectedPartitionedArray(m, s) for m, s in product(SingleColorSelectionMode, selections)]
        ]
