from typing import Dict, Any, Iterable, List, Tuple

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.operation.color_selection import FixedSingleColorSelection
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import CompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.completed_node import UniformOperationCompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.node_tree import NodeTree
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor


class OperationSetEvaluator:
    # Evaluation function to choose three answers by ranking the OperationSet.
    # Smaller is better.

    def evaluate(self, operation_set: OperationSet) -> float:
        score_map = {
            FixedSingleColorSelection: 0.5,
        }

        return sum(score_map.get(e.__class__, 1) for e in operation_set.elements())


def get_alternative_operation_sets(raw_task: Task, last_completed_node: UniformOperationCompletedNode, visited_node_hashes: Dict[int, Dict[int, Any]], verbose: bool) -> Iterable[NodeTree]:
    if verbose:
        print('original_answer')
        print(NodeTree.of(last_completed_node).to_operation_set())
        print('===search other answers===')

    node_tree = NodeTree.of(last_completed_node)

    depth_alternative_nodes_pairs: List[Tuple[int, List[CompletedNode]]] = []
    for i, node in enumerate(node_tree.completed_nodes):
        if i == 0:
            # no alternative for root node
            continue
        if node.train_arr_hash() in visited_node_hashes:
            same_hash_node_dicts = visited_node_hashes[node.train_arr_hash()]
            alternative_nodes = [n for all_hash, n in same_hash_node_dicts.items() if all_hash != node.all_arr_hash()]
            depth_alternative_nodes_pairs.append((i, alternative_nodes))

    if verbose:
        print(f'alternative_nodes:')
        for i, alternative_nodes in depth_alternative_nodes_pairs:
            for n in alternative_nodes:
                print(f'node_depth: {i}, {n}')

    candidate_node_trees = [node_tree]
    for i, alternative_nodes in depth_alternative_nodes_pairs:
        for n in alternative_nodes:
            if len(candidate_node_trees) > 1000:
                break  # TODO Too many candidate_node_trees causes Memory Error.
            candidate_node_trees += [NodeTree.replaced_new_node_tree(t, i, n) for t in candidate_node_trees]

    if verbose:
        print('node_tree:')
        print(node_tree)
        print('candidate_node_trees:')
        for c in candidate_node_trees:
            print('===')
            print(c)

    # TODO unnecessary filter?
    candidate_node_trees = [t for t in candidate_node_trees if AnswerMatcher.is_train_all_match_if_operated(raw_task, t.to_operation_set())]
    candidate_node_trees = sorted(candidate_node_trees, key=lambda t: OperationSetEvaluator().evaluate(t.to_operation_set()))

    result_applied_tasks = []
    for t in candidate_node_trees:
        try:
            applied_task = TaskOperationSetExecutor().execute(raw_task, t.to_operation_set())
        except OperationInconsistencyException:
            continue

        if any(applied_task.test_arr_hash() == t.test_arr_hash() for t in result_applied_tasks):
            continue

        result_applied_tasks.append(applied_task)
        yield t
