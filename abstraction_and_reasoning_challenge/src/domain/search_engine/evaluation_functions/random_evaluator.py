import random

from typing import List

from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import WaitingNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.base import NodeEvaluator


class RandomNodeEvaluator(NodeEvaluator):

    def evaluate(self, node: WaitingNode):
        node.cache_pred_distance = random.uniform(0, 1) * node.depth()

    def evaluate_nodes(self, nodes: List[WaitingNode]):
        for n in nodes:
            self.evaluate(n)
