from collections import defaultdict

from dataclasses import dataclass
from operator import itemgetter
from typing import List, Dict, Union, Any

from abstraction_and_reasoning_challenge.param_config import DistanceEvaluatorParameter, NodeBaseSearchEngineParameter
from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import TaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import PartitionOperation, MultiColorChannelOperation, ColorOperation, UniformOperation
from abstraction_and_reasoning_challenge.src.domain.operation.uniform_operation import Flip, Rotate, Padding, Resize
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.base import NodeEvaluator, DepthSearchPattern
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import WaitingNode, CompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.waiting_node import UniformOperationWaitingNode, ColorSelectionWaitingNode, MaskConversionWaitingNode, MaskOperationSelectionWaitingNode, ColorChannelSelectionOperationWaitingNode, \
    ColorChannelMergeWaitingNode, \
    ColorChannelMaskConversionWaitingNode, PartitionSelectionWaitingNode, PartitionMergeWaitingNode


@dataclass
class EvaluatorGuideByOneDepthEval:
    dict_ranking_num = 10  # dict[(operation_elements): sorted_list[(complete_operation, float)...],]のlistの最大数

    def __init__(self, raw_task_eval: float, one_depth_eval_dict: Dict[Union[UniformOperation, ColorOperation, MultiColorChannelOperation, PartitionOperation], float], default_value_for_missing: float):
        self.default_value_for_missing = default_value_for_missing

        operation_element_eval_dict = defaultdict(list)
        for operation, eval_value in one_depth_eval_dict.items():
            eval_ratio = eval_value / raw_task_eval
            if isinstance(operation, UniformOperation):
                self._update_dict_value(operation_element_eval_dict, operation, operation, eval_ratio)
            elif isinstance(operation, ColorOperation):
                self._update_dict_value(operation_element_eval_dict, operation.color_selection, operation, eval_ratio)
                self._update_dict_value(operation_element_eval_dict, (operation.color_selection, operation.mask_conversions), operation, eval_ratio)
                self._update_dict_value(operation_element_eval_dict, (operation.color_selection, operation.mask_conversions, operation.mask_operation), operation, eval_ratio)
            elif isinstance(operation, MultiColorChannelOperation):
                self._update_dict_value(operation_element_eval_dict, operation.channel_selection, operation, eval_ratio)
                self._update_dict_value(operation_element_eval_dict, (operation.channel_selection, operation.mask_conversions), operation, eval_ratio)
                self._update_dict_value(operation_element_eval_dict, (operation.channel_selection, operation.mask_conversions, operation.channel_merge_operation), operation, eval_ratio)
            elif isinstance(operation, PartitionOperation):
                self._update_dict_value(operation_element_eval_dict, operation.partition_selection, operation, eval_ratio)
                self._update_dict_value(operation_element_eval_dict, (operation.partition_selection, operation.partition_merge_operation), operation, eval_ratio)

        self.operation_element_eval_dict = operation_element_eval_dict

    def get(self, operation_elements: Any, exclude_operations: list) -> float:
        d_val = self.operation_element_eval_dict[operation_elements]

        for complete_operation, eval_v in d_val:
            if complete_operation in exclude_operations:
                continue
            return eval_v
        return self.default_value_for_missing

    def evaluate(self, node: WaitingNode) -> float:

        exclude_operations = node.base_operation_set.operations

        if isinstance(node, UniformOperationWaitingNode):
            return self.get(node.next_operation, exclude_operations)
        elif isinstance(node, ColorSelectionWaitingNode):
            return self.get(node.next_selection, exclude_operations)
        elif isinstance(node, MaskConversionWaitingNode):
            return self.get((node.color_selection, node.next_mask_conversion), exclude_operations)
        elif isinstance(node, MaskOperationSelectionWaitingNode):
            return self.get((node.color_selection, node.mask_conversion, node.next_mask_operation), exclude_operations)
        elif isinstance(node, ColorChannelSelectionOperationWaitingNode):
            return self.get(node.next_color_channel_selection, exclude_operations)
        elif isinstance(node, ColorChannelMaskConversionWaitingNode):
            return self.get((node.color_channel_selection, node.next_mask_conversion), exclude_operations)
        elif isinstance(node, ColorChannelMergeWaitingNode):
            return self.get((node.color_channel_selection, node.mask_conversion, node.next_merge_operation), exclude_operations)
        elif isinstance(node, PartitionSelectionWaitingNode):
            return self.get(node.next_partition_selection, exclude_operations)
        elif isinstance(node, PartitionMergeWaitingNode):
            return self.get((node.partition_selection, node.next_partition_merge_operation), exclude_operations)
        else:
            raise NotImplementedError()

    def _update_dict_value(self, d: dict, operation_elements, complete_operation, eval_value: float):
        if eval_value >= self.default_value_for_missing:
            return

        d_val = d[operation_elements]

        if len(d_val) > self.dict_ranking_num:
            return

        if len(d_val) == self.dict_ranking_num and d_val[-1][1] < eval_value:
            return

        d_val.append((complete_operation, eval_value))
        d[operation_elements] = sorted(d_val, key=itemgetter(1))


@dataclass
class HandMadeNodeEvaluator(NodeEvaluator):
    pattern: DepthSearchPattern
    operation_element_prob_dict: Dict[str, float]
    node_search_engine_param: NodeBaseSearchEngineParameter
    dist_eval_param: DistanceEvaluatorParameter
    evaluator_guide: EvaluatorGuideByOneDepthEval = None

    def __post_init__(self):
        self.class_mapping = {
            UniformOperationWaitingNode: OperationWaitingNodeEvaluator(self.operation_element_prob_dict),
            ColorSelectionWaitingNode: ColorSelectionWaitingNodeEvaluator(self.operation_element_prob_dict),
            MaskConversionWaitingNode: MaskConversionWaitingNodeEvaluator(self.operation_element_prob_dict),
            MaskOperationSelectionWaitingNode: MaskOperationSelectionWaitingNodeEvaluator(self.operation_element_prob_dict),
            ColorChannelSelectionOperationWaitingNode: ColorChannelSelectionOperationWaitingNodeEvaluator(self.operation_element_prob_dict),
            ColorChannelMaskConversionWaitingNode: ColorChannelMaskConversionWaitingNodeEvaluator(self.operation_element_prob_dict),
            ColorChannelMergeWaitingNode: ColorChannelMergeWaitingNodeEvaluator(self.operation_element_prob_dict),
            PartitionSelectionWaitingNode: PartitionSelectionWaitingNodeEvaluator(self.operation_element_prob_dict),
            PartitionMergeWaitingNode: PartitionMergeWaitingNodeEvaluator(self.operation_element_prob_dict),
        }

        self.dist_evaluator = DistanceEvaluator(self.dist_eval_param)

    def evaluate_nodes(self, nodes: List[WaitingNode]):
        for n in nodes:
            self.evaluate(n)

    def evaluate(self, node: WaitingNode):
        evaluator = self.class_mapping[node.__class__]
        task_feature = evaluator.get_task_feature(node)
        base_distance = self.dist_evaluator.evaluate_task_feature(task_feature)
        element_including_prob = evaluator.get_element_inclusion_prob(node)
        one_depth_guide = self.evaluator_guide.evaluate(node) if self.evaluator_guide is not None else 1
        node.cache_pred_distance = self.calculate_final_distance(base_distance, element_including_prob, one_depth_guide, node.depth())

    def evaluate_base_distance_for_completed_node(self, node: CompletedNode):
        return self.dist_evaluator.evaluate_task_feature(node.task_feature)

    def calculate_final_distance(self, base_distance: float, element_inclusion_prob: float, one_depth_guide: float, depth: int) -> float:
        base_distance *= one_depth_guide ** 0.1  # TODO 適当すぎるかも？

        prob_cost = self.node_search_engine_param.element_inclusion_prob_factor * (1 - element_inclusion_prob)
        if self.pattern == DepthSearchPattern.BREADTH_FIRST:
            return base_distance ** (1 + depth * self.node_search_engine_param.breadth_first_exp_cost) + prob_cost + self.node_search_engine_param.breadth_first_cost * depth
        elif self.pattern == DepthSearchPattern.NORMAL:
            return base_distance ** (1 + depth * self.node_search_engine_param.normal_exp_cost) + prob_cost + self.node_search_engine_param.normal_first_cost * depth
        elif self.pattern == DepthSearchPattern.DEPTH_FIRST:
            return base_distance ** (1 + depth * self.node_search_engine_param.depth_first_exp_cost) + prob_cost + self.node_search_engine_param.depth_first_cost * depth
        else:
            raise NotImplementedError()


@dataclass
class DistanceEvaluator:
    dist_eval_param: DistanceEvaluatorParameter

    def evaluate_task_feature(self, task_feature: TaskFeature) -> float:
        return 0 \
               + self.dist_eval_param.same_h_w_dim_between_input_output * (0 if task_feature.same_height_dim_between_input_output else 1) \
               + self.dist_eval_param.same_h_w_dim_between_input_output * (0 if task_feature.same_width_dim_between_input_output else 1) \
               + self.dist_eval_param.all_dim_h_w_integer_multiple * (0 if task_feature.all_dim_height_integer_multiple else 1) \
               + self.dist_eval_param.all_dim_h_w_integer_multiple * (0 if task_feature.all_dim_width_integer_multiple else 1) \
               + self.dist_eval_param.mean_lack_color_num * task_feature.mean_lack_color_num \
               + self.dist_eval_param.mean_excess_color_num * task_feature.mean_excess_color_num \
               + self.dist_eval_param.mean_hit_and_miss_histogram_diff * task_feature.mean_hit_and_miss_histogram_diff \
               + self.dist_eval_param.mean_h_v_edge_sum_diff * (task_feature.mean_vertical_edge_sum_diff) \
               + self.dist_eval_param.mean_h_v_edge_sum_diff * (task_feature.mean_horizontal_edge_sum_diff) \
               + self.dist_eval_param.mean_h_v_edge_sum_diff_ratio * (task_feature.mean_vertical_edge_sum_diff_ratio) \
               + self.dist_eval_param.mean_h_v_edge_sum_diff_ratio * (task_feature.mean_horizontal_edge_sum_diff_ratio) \
               + self.dist_eval_param.mean_diff_color_cell_ratio * (task_feature.mean_diff_color_cell_ratio or 0) \
               + self.dist_eval_param.mean_diff_cell_where_no_need_to_change_count_ratio * (task_feature.mean_diff_cell_where_no_need_to_change_count_ratio or 0) \
               + self.dist_eval_param.mean_wrong_change_cell_where_need_to_change_count_ratio * (task_feature.mean_wrong_change_cell_where_need_to_change_count_ratio or 0)

    # + self.dist_eval_param.mean_h_v_diff_input_arr_line_num * (task_feature.mean_horizontal_diff_input_arr_line_num or 0) \
    # + self.dist_eval_param.mean_h_v_diff_input_arr_line_num * (task_feature.mean_vertical_diff_input_arr_line_num or 0) \
    # + self.dist_eval_param.mean_h_v_diff_output_arr_line_num * (task_feature.mean_horizontal_diff_output_arr_line_num or 0) \
    # + self.dist_eval_param.mean_h_v_diff_output_arr_line_num * (task_feature.mean_vertical_diff_output_arr_line_num or 0) \

    def evaluate_task_feature_element(self, task_feature: TaskFeature) -> List[float]:
        return [(0 if task_feature.same_height_dim_between_input_output else 1),
                (0 if task_feature.same_width_dim_between_input_output else 1),
                (0 if task_feature.all_dim_height_integer_multiple else 1),
                (0 if task_feature.all_dim_width_integer_multiple else 1),
                task_feature.mean_lack_color_num,
                task_feature.mean_excess_color_num,
                task_feature.mean_hit_and_miss_histogram_diff,
                (task_feature.mean_vertical_edge_sum_diff),
                (task_feature.mean_horizontal_edge_sum_diff),
                (task_feature.mean_vertical_edge_sum_diff_ratio),
                (task_feature.mean_horizontal_edge_sum_diff_ratio),
                (task_feature.mean_diff_color_cell_ratio or 0)]
        # + 10 * (task_feature.mean_horizontal_diff_input_arr_line_num or 0), \
        # + 10 * (task_feature.mean_horizontal_diff_output_arr_line_num or 0) ,\
        # + 10 * (task_feature.mean_vertical_diff_input_arr_line_num or 0), \
        # + 10 * (task_feature.mean_vertical_diff_output_arr_line_num or 0), \


@dataclass
class HandmadeNodeEvaluatorBase:
    operation_element_prob_dict: Dict[str, float]

    def get_task_feature(self, node) -> TaskFeature:
        raise NotImplementedError()

    def get_element_inclusion_prob(self, node) -> float:
        raise NotImplementedError()

    def calculate_dist_factor(self, node) -> float:
        raise NotImplementedError()


class OperationWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, operation_waiting_node: UniformOperationWaitingNode) -> TaskFeature:
        return operation_waiting_node.task_feature

    def get_element_inclusion_prob(self, operation_waiting_node: UniformOperationWaitingNode) -> float:
        return self.operation_element_prob_dict[operation_waiting_node.next_operation.__class__.__name__]

    def calculate_dist_factor(self, operation_waiting_node: UniformOperationWaitingNode) -> float:
        # TODO use height_integer_multiple?
        operation = operation_waiting_node.next_operation
        if isinstance(operation, (Flip, Rotate)):
            if operation_waiting_node.task_feature.same_dim_between_input_output:
                dist_factor = 0.8
            else:
                dist_factor = 1.2
        elif isinstance(operation, (Resize, Padding)):
            if operation_waiting_node.task_feature.same_dim_between_input_output:
                dist_factor = 1.2
            else:
                dist_factor = 0.8
        else:
            raise NotImplementedError(operation)
        return dist_factor


class ColorSelectionWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, color_selection_waiting_node: ColorSelectionWaitingNode) -> TaskFeature:
        return color_selection_waiting_node.task_feature

    def get_element_inclusion_prob(self, color_selection_waiting_node: ColorSelectionWaitingNode) -> float:
        return self.operation_element_prob_dict[color_selection_waiting_node.next_selection.__class__.__name__]

    def calculate_dist_factor(self, color_selection_waiting_node: ColorSelectionWaitingNode) -> float:
        return 1.0
        # TODO
        # color_channel_selection = color_selection_waiting_node.next_selection
        # if isinstance(color_channel_selection, FixedSingleColorSelection):
        #     fixed_color_include_num = sum(color_channel_selection in d.input_image_feature.colors for d in color_selection_waiting_node.task_feature.image_diff_features)
        #     if fixed_color_include_num == len(color_selection_waiting_node.task_feature.image_diff_features):
        #         return 0.8
        #     else:
        #         return 1.2
        # else:
        #     return 0.9


class MaskConversionWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, mask_conversion_waiting_node: MaskConversionWaitingNode) -> TaskFeature:
        return mask_conversion_waiting_node.color_selected_task_feature.task_feature

    def get_element_inclusion_prob(self, mask_conversion_waiting_node: MaskConversionWaitingNode) -> float:
        return self.operation_element_prob_dict[mask_conversion_waiting_node.next_mask_conversion.__class__.__name__]

    def calculate_dist_factor(self, mask_conversion_waiting_node: MaskConversionWaitingNode) -> float:
        return 1.0


class MaskOperationSelectionWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, mask_operation_waiting_node: MaskOperationSelectionWaitingNode) -> TaskFeature:
        return mask_operation_waiting_node.mask_converted_task_feature.task_feature

    def get_element_inclusion_prob(self, mask_operation_waiting_node: MaskOperationSelectionWaitingNode) -> float:
        return self.operation_element_prob_dict[mask_operation_waiting_node.next_mask_operation.__class__.__name__]

    def calculate_dist_factor(self, mask_operation_waiting_node: MaskOperationSelectionWaitingNode) -> float:
        return 1.0
        # TODO
        # ratios = list(filter(lambda r: r is not None, mask_operation_waiting_node.mask_converted_task.possible_improve_ratios))
        # if len(ratios) == 0:
        #     return 1
        # else:
        #     return 0.5 + np.average(ratios) / 2


class ColorChannelSelectionOperationWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, node: ColorChannelSelectionOperationWaitingNode) -> TaskFeature:
        return node.task_feature

    def get_element_inclusion_prob(self, node: ColorChannelSelectionOperationWaitingNode) -> float:
        return self.operation_element_prob_dict[node.next_color_channel_selection.__class__.__name__]


class ColorChannelMaskConversionWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, node: ColorChannelMaskConversionWaitingNode) -> TaskFeature:
        return node.task_feature

    def get_element_inclusion_prob(self, node: ColorChannelMaskConversionWaitingNode) -> float:
        return self.operation_element_prob_dict[node.next_mask_conversion.__class__.__name__]


class ColorChannelMergeWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, node: ColorChannelMergeWaitingNode) -> TaskFeature:
        return node.task_feature

    def get_element_inclusion_prob(self, node: ColorChannelMergeWaitingNode) -> float:
        return self.operation_element_prob_dict[node.next_merge_operation.__class__.__name__]


class PartitionSelectionWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, node: PartitionSelectionWaitingNode) -> TaskFeature:
        return node.task_feature

    def get_element_inclusion_prob(self, node: PartitionSelectionWaitingNode) -> float:
        return self.operation_element_prob_dict[node.next_partition_selection.__class__.__name__]


class PartitionMergeWaitingNodeEvaluator(HandmadeNodeEvaluatorBase):

    def get_task_feature(self, node: PartitionMergeWaitingNode) -> TaskFeature:
        return node.task_feature

    def get_element_inclusion_prob(self, node: PartitionMergeWaitingNode) -> float:
        return self.operation_element_prob_dict[node.next_partition_merge_operation.__class__.__name__]
