from enum import Enum, auto, unique

from typing import List

from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import WaitingNode


class NodeEvaluator:

    def evaluate(self, node: WaitingNode):
        raise NotImplementedError()

    def evaluate_nodes(self, nodes: List[WaitingNode]):
        raise NotImplementedError()


@unique
class DepthSearchPattern(Enum):
    BREADTH_FIRST = auto()
    NORMAL = auto()
    DEPTH_FIRST = auto()
