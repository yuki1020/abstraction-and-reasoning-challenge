import pickle

from category_encoders import OrdinalEncoder
from lightgbm import LGBMClassifier
from pandas import DataFrame
from typing import List

from abstraction_and_reasoning_challenge.config import PathConfig
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.base import WaitingNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.base import NodeEvaluator, DepthSearchPattern


class MLNodeEvaluator(NodeEvaluator):

    def __init__(self, pattern: DepthSearchPattern):
        self.pattern = pattern
        self.features = pickle.load(PathConfig.NODE_EVALUATOR_FEATURES.open(mode='rb'))
        self.categorical_features = pickle.load(PathConfig.NODE_EVALUATOR_CATEGORICAL_FEATURES.open(mode='rb'))
        self.sample_df = pickle.load(PathConfig.NODE_EVALUATOR_SAMPLE_DF.open(mode='rb'))
        self.model: LGBMClassifier = pickle.load(PathConfig.NODE_EVALUATOR_MODEL.open(mode='rb'))
        self.model.n_jobs = 1
        self.oe: OrdinalEncoder = pickle.load(PathConfig.NODE_EVALUATOR_ORDINAL_ENCODER.open(mode='rb'))

    def evaluate(self, node: WaitingNode) -> float:
        raise NotImplementedError()

    def evaluate_nodes(self, nodes: List[WaitingNode]):
        if len(nodes) == 0:
            return

        feature_dicts = [n.evaluation_features() for n in nodes]

        feature_dicts = [{
            **{k: v for k, v in d.items() if k in self.features},
            **{f: None for f in self.features if f not in d}
        } for d in feature_dicts]

        for d in feature_dicts:
            for c_f in self.categorical_features:
                d[c_f] = str(d[c_f])

        df = DataFrame(columns=self.features)
        df = df.append(feature_dicts)

        df[self.categorical_features] = self.oe.transform(df[self.categorical_features])
        df = df.fillna(-1)

        x = df[self.features]
        probs = self.model.predict_proba(x)[:, 0]

        for n, p in zip(nodes, probs):
            n.cache_pred_distance = self._add_cost(p, n.depth())

    def _add_cost(self, prob: float, depth: int) -> float:
        # Impose penalty. A* like algorithm.
        if self.pattern == DepthSearchPattern.BREADTH_FIRST:
            return prob ** (1 / (1 + (depth / 1))) + 0.3 * depth
        elif self.pattern == DepthSearchPattern.NORMAL:
            return prob ** (1 / (1 + (depth / 2))) + 0.1 * depth
        elif self.pattern == DepthSearchPattern.DEPTH_FIRST:
            return prob
        else:
            raise NotImplementedError()
