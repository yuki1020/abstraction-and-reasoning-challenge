from dataclasses import dataclass
from typing import List

from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import TaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.completed_node import UniformOperationCompletedNode
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node.node_tree import NodeTree
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.random_evaluator import RandomNodeEvaluator
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.completed_node_processor import CompletedNodeProcessor
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.waiting_node_processor import WaitingNodeProcessor
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.priority_queue.priority_queue import PriorityQueue
from abstraction_and_reasoning_challenge.src.utils import Timer


@dataclass
class RandomNodeTreeCreateEngine:
    MAX_NODE = 30000
    timeout_sec: int = 30
    node_evaluator = RandomNodeEvaluator()

    def search(self, task: Task, verbose: bool = False) -> List[NodeTree]:
        task_feature = TaskFeature.of(task)
        root_node = UniformOperationCompletedNode(None, task, task_feature, OperationSet([]))
        first_waiting_nodes = CompletedNodeProcessor.process(root_node)
        self.node_evaluator.evaluate_nodes(first_waiting_nodes)
        pq = PriorityQueue([*first_waiting_nodes])

        if verbose:
            print('first pq nodes')
            for n in pq.sorted_list():
                print(f'cost: {n.cache_pred_distance}, {n}')

        with Timer() as timer:
            for node_i in range(self.MAX_NODE):
                if len(pq) == 0:
                    # TODO What's the right thing to do?
                    raise NotImplementedError()

                waiting_node = pq.pop_min()

                completed_node = WaitingNodeProcessor().process(waiting_node)

                if completed_node is None:
                    if verbose:
                        print('skipped')
                    continue

                waiting_new_nodes = CompletedNodeProcessor.process(completed_node)
                self.node_evaluator.evaluate_nodes(waiting_new_nodes)

                for n in waiting_new_nodes:
                    pq.push(n)

                if timer.second() > self.timeout_sec:
                    break

        return [NodeTree.of(waiting_node.parent_completed_node) for waiting_node in pq.heap]
