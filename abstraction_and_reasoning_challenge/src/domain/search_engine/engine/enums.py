from enum import unique, auto, Enum


@unique
class EngineType(Enum):
    NODE_BASED_SEARCH_ENGINE = auto()
    TREE_BASED_SEARCH_ENGINE = auto()


@unique
class EngineSchedulePattern(Enum):
    DRY_RUN = auto()
    HAND_MADE = auto()
    ML = auto()
