from abstraction_and_reasoning_challenge.src.domain.search_engine.engine.enums import EngineType
from abstraction_and_reasoning_challenge.src.domain.search_engine.node_based_search_engine.node_base_search_engine import NodeBaseSearchEngine
from abstraction_and_reasoning_challenge.src.domain.search_engine.tree_based_search_engine.tree_base_search_engine import TreeBaseSearchEngine


def get_engine(engine_type: EngineType):
    if engine_type == EngineType.NODE_BASED_SEARCH_ENGINE:
        return NodeBaseSearchEngine()
    elif engine_type == EngineType.TREE_BASED_SEARCH_ENGINE:
        return TreeBaseSearchEngine()
    else:
        raise NotImplementedError()
