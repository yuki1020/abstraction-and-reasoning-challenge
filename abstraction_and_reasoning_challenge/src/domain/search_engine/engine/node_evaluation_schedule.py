from dataclasses import dataclass
from typing import List, Optional, Dict

from abstraction_and_reasoning_challenge.config import RunConfig
from abstraction_and_reasoning_challenge.src.domain.search_engine.engine.enums import EngineSchedulePattern
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.handmade_evaluator import NodeEvaluator, HandMadeNodeEvaluator
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.ml_evaluator import DepthSearchPattern, MLNodeEvaluator
from abstraction_and_reasoning_challenge.src.enums import RunMode


@dataclass
class NodeEvaluatorSchedule:
    start_sec: int
    evaluator: Optional[NodeEvaluator]


@dataclass
class NodeEvaluatorSchedules:
    schedules: List[NodeEvaluatorSchedule]

    def pop_evaluator(self) -> Optional[NodeEvaluator]:
        evaluator = self.schedules[0].evaluator
        self.schedules = self.schedules[1:]
        return evaluator

    def next_timing(self):
        return self.schedules[0].start_sec

    def timeout_sec(self):
        return self.schedules[-1].start_sec


def get_schedule(operation_element_prob_dict: Dict[str, float], node_search_engine_param, dist_eval_param) -> NodeEvaluatorSchedules:
    if RunConfig.RUN_MODE == RunMode.KERNEL:
        if RunConfig.ENGINE_SCHEDULE_PATTERN == EngineSchedulePattern.HAND_MADE:
            return NodeEvaluatorSchedules([
                NodeEvaluatorSchedule(0, HandMadeNodeEvaluator(DepthSearchPattern.BREADTH_FIRST, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(60 * 1, HandMadeNodeEvaluator(DepthSearchPattern.NORMAL, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(60 * 2, HandMadeNodeEvaluator(DepthSearchPattern.DEPTH_FIRST, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(60 * 3, None),
            ])
        if RunConfig.ENGINE_SCHEDULE_PATTERN == EngineSchedulePattern.DRY_RUN:
            return NodeEvaluatorSchedules([
                NodeEvaluatorSchedule(0, HandMadeNodeEvaluator(DepthSearchPattern.BREADTH_FIRST, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(3, None),
            ])
    else:
        if RunConfig.ENGINE_SCHEDULE_PATTERN == EngineSchedulePattern.HAND_MADE:
            return NodeEvaluatorSchedules([
                NodeEvaluatorSchedule(0, HandMadeNodeEvaluator(DepthSearchPattern.BREADTH_FIRST, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(20, HandMadeNodeEvaluator(DepthSearchPattern.NORMAL, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(40, HandMadeNodeEvaluator(DepthSearchPattern.DEPTH_FIRST, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(60, None),
            ])
        if RunConfig.ENGINE_SCHEDULE_PATTERN == EngineSchedulePattern.ML:
            return NodeEvaluatorSchedules([
                NodeEvaluatorSchedule(0, MLNodeEvaluator(DepthSearchPattern.BREADTH_FIRST)),
                NodeEvaluatorSchedule(20, MLNodeEvaluator(DepthSearchPattern.NORMAL)),
                NodeEvaluatorSchedule(40, MLNodeEvaluator(DepthSearchPattern.DEPTH_FIRST)),
                NodeEvaluatorSchedule(60, None),
            ])
        if RunConfig.ENGINE_SCHEDULE_PATTERN == EngineSchedulePattern.DRY_RUN:
            return NodeEvaluatorSchedules([
                NodeEvaluatorSchedule(0, HandMadeNodeEvaluator(DepthSearchPattern.BREADTH_FIRST, operation_element_prob_dict, node_search_engine_param, dist_eval_param)),
                NodeEvaluatorSchedule(3, None),
            ])
    raise NotImplementedError()
