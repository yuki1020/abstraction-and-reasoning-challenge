import random
from collections import defaultdict

import numpy as np
from dataclasses import dataclass
from deap.tools import selNSGA2
from itertools import product, chain
from typing import List, Dict, Union, Optional
from typing import Tuple

from abstraction_and_reasoning_challenge.config import RunConfig
from abstraction_and_reasoning_challenge.param_config import TreeBaseSearchEngineParameter, AllParameter
from abstraction_and_reasoning_challenge.src.domain.exceptions import MaxNodeExceededException, TimeoutException, OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.feature.task_feature import create_task_feature, TaskFeature
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet, UniformOperation, ColorOperation
from abstraction_and_reasoning_challenge.src.domain.operation.color_selection import FixedSingleColorSelection, SingleColorSelection, MultiColorSelection
from abstraction_and_reasoning_challenge.src.domain.operation.mask_conversion import HolesSelection, ConnectDotSelection, SquareObjectsSelection, NoMaskConversion, ContourSelection, SplitLineSelection, DotExistLineSelection, ObjectsMaxMinSelection, ObjectInnerSelection, \
    ContourOuterSelection
from abstraction_and_reasoning_challenge.src.domain.operation.mask_operation import MaskCoordsCrop, FixedColorMaskFill, SingleColorMaskFill
from abstraction_and_reasoning_challenge.src.domain.operation.uniform_operation import Flip, Rotate, Padding, Resize
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import FlipMode, Axis, Direction, PaddingMode, Color
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import LineEdgeType, FillType
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import SingleColorSelectionMode, MultiColorSelectionMode, PixelConnectivity, ObjectFeature, MaxOrMin, ImageEdgeType, HoleInclude
from abstraction_and_reasoning_challenge.src.domain.search_engine.evaluation_functions.handmade_evaluator import DistanceEvaluator
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task_result import AnsweredSearchResults, NotAnsweredSearchResult, AnsweredSearchResult
from abstraction_and_reasoning_challenge.src.ml.operation_element_inclusion_prediction import predict_operation_element_inclusion
from abstraction_and_reasoning_challenge.src.utils import Timer


# TODO not used class.


@dataclass
class OperationSetExecutionResultHolder:
    raw_task: Task
    cache: Dict[str, Tuple[Task, TaskFeature]]

    def get_result(self, operation_set: OperationSet) -> Tuple[Task, TaskFeature]:
        if str(operation_set) in self.cache:
            return self.cache[str(operation_set)]

        for i in reversed(range(1, len(operation_set.operations))):
            prev_o_s = OperationSet(operation_set.operations[:i])
            post_o_s = OperationSet(operation_set.operations[i:])
            assert len(prev_o_s.operations) + len(post_o_s.operations) == len(operation_set.operations)

            if str(prev_o_s) in self.cache:
                prev_task, _ = self.cache[str(prev_o_s)]
                post_o_s_applied_task = TaskOperationSetExecutor().execute(prev_task, post_o_s)
                post_o_s_applied_task_feature = create_task_feature(post_o_s_applied_task)
                self.cache[str(operation_set)] = (post_o_s_applied_task, post_o_s_applied_task_feature)
                return post_o_s_applied_task, post_o_s_applied_task_feature

        applied_task = TaskOperationSetExecutor().execute(self.raw_task, operation_set)
        applied_task_feature = create_task_feature(applied_task)
        self.cache[str(operation_set)] = (applied_task, applied_task_feature)
        return applied_task, applied_task_feature


@dataclass
class OperationSetMutator:
    # TODO uniform(0, 1) is redundant. There must be easier way to do it.
    # TODO I'd like to define procedure of probability. like albumentation.
    # TODO should increase max_depth dynamically?

    holder: OperationSetExecutionResultHolder
    operation_element_prob_dict: Dict[str, float]

    def mutate(self, operation_set: OperationSet):
        new_operations = []
        for o in operation_set.operations:
            task, task_feature = self.holder.get_result(OperationSet(new_operations))
            if random.uniform(0, 1) < TreeBaseSearchEngineParameter.operation_mutation_prob:
                new_operations.append(self.get_random_one_operation(task, task_feature))
            elif random.uniform(0, 1) < TreeBaseSearchEngineParameter.operation_component_mutation_prob:
                if isinstance(o, UniformOperation):
                    new_operations.append(self._uniform_operation_candidates(task_feature))
                elif isinstance(o, ColorOperation):
                    color_sel, add_sels, mask_ope = o.color_selection, o.mask_conversions, o.mask_operation
                    if random.uniform(0, 1) < 1 / 3:
                        color_sel = self._color_selection_candidates(task)
                    if random.uniform(0, 1) < 1 / 3:
                        add_sels = [self._mask_conversions()]
                    if random.uniform(0, 1) < 1 / 3:
                        mask_ope = self._mask_operation_candidates(task)
                    new_operations.append(ColorOperation(color_sel, add_sels, mask_ope))
                else:
                    raise NotImplementedError()
            elif random.uniform(0, 1) < TreeBaseSearchEngineParameter.operation_param_mutation_prob:
                if isinstance(o, UniformOperation):
                    new_operations.append(self._mutate_parameter(o, task))
                elif isinstance(o, ColorOperation):
                    color_sel, add_sels, mask_ope = o.color_selection, o.mask_conversions, o.mask_operation
                    if random.uniform(0, 1) < 1 / 3:
                        color_sel = self._mutate_parameter(color_sel, task)
                    if random.uniform(0, 1) < 1 / 3:
                        add_sels = [self._mutate_parameter(add_sels[0], task)]
                    if random.uniform(0, 1) < 1 / 3:
                        mask_ope = self._mutate_parameter(mask_ope, task)
                    new_operations.append(ColorOperation(color_sel, add_sels, mask_ope))
                else:
                    raise NotImplementedError()
            elif random.uniform(0, 1) < TreeBaseSearchEngineParameter.shrink_mutation_prob:
                continue
            else:
                new_operations.append(o)

        if len(new_operations) < TreeBaseSearchEngineParameter.max_depth:
            if random.uniform(0, 1) < TreeBaseSearchEngineParameter.extend_mutation_prob:
                temp_new_set = OperationSet(new_operations)
                task, task_feature = self.holder.get_result(temp_new_set)
                new_operations.append(self.get_random_one_operation(task, task_feature))

        return OperationSet(new_operations)

    def get_random_one_operation(self, task: Task, task_feature: TaskFeature):
        classes = [UniformOperation, ColorOperation]

        class_probs = [self.operation_element_prob_dict[c.__name__] for c in classes]
        total_prob = sum(class_probs)
        class_probs = [p / total_prob for p in class_probs]

        chosen_class = np.random.choice(classes, p=class_probs)

        if chosen_class == UniformOperation:
            operation = self._uniform_operation_candidates(task_feature)
        elif chosen_class == ColorOperation:
            color_sel = self._color_selection_candidates(task)
            add_sels = self._mask_conversions()
            mask_ope = self._mask_operation_candidates(task)
            operation = ColorOperation(color_sel, add_sels, mask_ope)
        else:
            raise NotImplementedError()

        return operation

    def _uniform_operation_candidates(self, task_feature: TaskFeature):
        classes = [Resize, Padding, Flip, Rotate]

        class_probs = [self.operation_element_prob_dict[c.__name__] for c in classes]
        total_prob = sum(class_probs)
        class_probs = [p / total_prob for p in class_probs]

        chosen_class = np.random.choice(classes, p=class_probs)

        if chosen_class == Resize:
            return random.choice([Resize(a, r) for a, r in product(Axis, range(2, 5))])
        elif chosen_class == Padding:
            return random.choice([Padding(m, d, k) for m, d, k in product(PaddingMode, Direction, range(1, 4))])
        elif chosen_class == Flip:
            return random.choice([Flip(m) for m in FlipMode])
        elif chosen_class == Rotate:
            return random.choice([Rotate(a) for a in [90, 180, 270]])
        else:
            raise NotImplementedError()

    def _color_selection_candidates(self, task: Task):
        classes = [FixedSingleColorSelection, SingleColorSelection, MultiColorSelection]

        class_probs = [self.operation_element_prob_dict[c.__name__] for c in classes]
        total_prob = sum(class_probs)
        class_probs = [p / total_prob for p in class_probs]

        chosen_class = np.random.choice(classes, p=class_probs)

        if chosen_class == FixedSingleColorSelection:
            input_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_input_all_arr())))))
            return random.choice([FixedSingleColorSelection(c) for c in input_colors])
        elif chosen_class == SingleColorSelection:
            return random.choice([SingleColorSelection(m) for m in SingleColorSelectionMode])
        elif chosen_class == MultiColorSelection:
            return random.choice([MultiColorSelection(m) for m in MultiColorSelectionMode])
        else:
            raise NotImplementedError()

    def _mask_conversions(self):
        classes = [NoMaskConversion, SquareObjectsSelection, ObjectsMaxMinSelection, SplitLineSelection, DotExistLineSelection,
                   HolesSelection, ObjectInnerSelection, ContourSelection, ContourOuterSelection, ConnectDotSelection]

        class_probs = [self.operation_element_prob_dict[c.__name__] for c in classes]
        total_prob = sum(class_probs)
        class_probs = [p / total_prob for p in class_probs]

        chosen_class = np.random.choice(classes, p=class_probs)

        if chosen_class == NoMaskConversion:
            return NoMaskConversion()
        elif chosen_class == SquareObjectsSelection:
            return SquareObjectsSelection()
        elif chosen_class == ObjectsMaxMinSelection:
            return random.choice([ObjectsMaxMinSelection(m, t, c) for m, t, c in product(MaxOrMin, ObjectFeature, PixelConnectivity)])
        elif chosen_class == SplitLineSelection:
            return random.choice([SplitLineSelection(a) for a in Axis])
        elif chosen_class == DotExistLineSelection:
            return random.choice([DotExistLineSelection(a) for a in Axis])
        elif chosen_class == HolesSelection:
            return random.choice([HolesSelection(c) for c in PixelConnectivity])
        elif chosen_class == ObjectInnerSelection:
            return random.choice([ObjectInnerSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)])
        elif chosen_class == ContourSelection:
            return random.choice([ContourSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)])
        elif chosen_class == ContourOuterSelection:
            return random.choice([ContourOuterSelection(c, h) for c, h in product(PixelConnectivity, HoleInclude)])
        elif chosen_class == ConnectDotSelection:
            return random.choice([ConnectDotSelection(a, e, f) for a, e, f in product(Axis, LineEdgeType, FillType)])
        else:
            raise NotImplementedError()

    def _mask_operation_candidates(self, task: Task):
        classes = [MaskCoordsCrop, FixedColorMaskFill, SingleColorMaskFill]

        class_probs = [self.operation_element_prob_dict[c.__name__] for c in classes]
        total_prob = sum(class_probs)
        class_probs = [p / total_prob for p in class_probs]

        chosen_class = np.random.choice(classes, p=class_probs)

        if chosen_class == MaskCoordsCrop:
            return MaskCoordsCrop()
        elif chosen_class == FixedColorMaskFill:
            output_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_output_all_arr())))))
            return random.choice([FixedColorMaskFill(c) for c in output_colors])
        elif chosen_class == SingleColorMaskFill:
            return random.choice([SingleColorMaskFill(m) for m in SingleColorSelectionMode])
        else:
            raise NotImplementedError()

    def _mutate_parameter(self, operation_element, task: Task):
        # TODO Should mutate one property of operation_element.
        if isinstance(operation_element, Resize):
            return random.choice([Resize(Axis.VERTICAL, r) for r in range(2, 5)])
        elif isinstance(operation_element, Padding):
            return random.choice([Padding(m, d, k) for m, d, k in product(PaddingMode, [Direction.TOP, Direction.BOTTOM], range(1, 4))])
        elif isinstance(operation_element, Flip):
            return random.choice([Flip(m) for m in FlipMode])
        elif isinstance(operation_element, Rotate):
            return random.choice([Rotate(a) for a in [90, 180, 270]])
        elif isinstance(operation_element, FixedSingleColorSelection):
            input_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_input_all_arr())))))
            return random.choice([FixedSingleColorSelection(c) for c in input_colors])
        elif isinstance(operation_element, SingleColorSelection):
            return random.choice([SingleColorSelection(m) for m in SingleColorSelectionMode])
        elif isinstance(operation_element, MultiColorSelection):
            return random.choice([MultiColorSelection(m) for m in MultiColorSelectionMode])
        elif isinstance(operation_element, NoMaskConversion):
            return random.choice([NoMaskConversion()])
        elif isinstance(operation_element, SquareObjectsSelection):
            return random.choice([SquareObjectsSelection()])
        elif isinstance(operation_element, ObjectsMaxMinSelection):
            return random.choice([ObjectsMaxMinSelection(m, t, c) for m, t, c in product(MaxOrMin, ObjectFeature, PixelConnectivity)])
        elif isinstance(operation_element, SplitLineSelection):
            return random.choice([SplitLineSelection(a) for a in Axis])
        elif isinstance(operation_element, DotExistLineSelection):
            return random.choice([DotExistLineSelection(a) for a in Axis])
        elif isinstance(operation_element, HolesSelection):
            return random.choice([HolesSelection(c) for c in PixelConnectivity])
        elif isinstance(operation_element, ObjectInnerSelection):
            return random.choice([ObjectInnerSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)])
        elif isinstance(operation_element, ContourSelection):
            return random.choice([ContourSelection(c, e) for c, e in product(PixelConnectivity, ImageEdgeType)])
        elif isinstance(operation_element, ContourOuterSelection):
            return random.choice([ContourOuterSelection(c, h) for c, h in product(PixelConnectivity, HoleInclude)])
        elif isinstance(operation_element, ConnectDotSelection):
            return random.choice([ConnectDotSelection(a, e, f) for a, e, f in product(Axis, LineEdgeType, FillType)])
        elif isinstance(operation_element, MaskCoordsCrop):
            return random.choice([MaskCoordsCrop()])
        elif isinstance(operation_element, FixedColorMaskFill):
            output_colors = list(map(lambda v: Color.of(v), set(chain.from_iterable(chain.from_iterable(task.get_output_all_arr())))))
            return random.choice([FixedColorMaskFill(c) for c in output_colors])
        elif isinstance(operation_element, SingleColorMaskFill):
            return random.choice([SingleColorMaskFill(m) for m in SingleColorSelectionMode])
        else:
            raise NotImplementedError(operation_element)


@dataclass
class Individual:
    operation_set: OperationSet
    distance: float
    task_feature: TaskFeature

    def __str__(self):
        return f'depth: {len(self.operation_set.operations)}, dist: {self.distance:.5f}, ope: {self.operation_set}'


@dataclass
class Population:
    strategy: str
    individuals: List[Individual]

    def show(self):
        self.sort()
        for i in self.individuals:
            print(i)

    def sort(self):
        random.shuffle(self.individuals)
        self.individuals = sorted(self.individuals, key=lambda i: i.distance)

    def get_elite(self):
        # TODO Lack of consideration when there were multiple elites.
        self.sort()
        return self.individuals[0]

    def get_dist0_if_exists(self) -> Optional[OperationSet]:
        elite = min(self.individuals, key=lambda i: i.distance)
        if elite.distance == 0:
            return elite.operation_set
        else:
            return None

    def mutate(self, mutator, holder, evaluator):
        # 変異
        self.sort()
        mutated_individuals = [self.get_elite()]
        for i in self.individuals[1:]:
            for _ in range(1000000000000):
                try:
                    mutated_operation_set = mutator.mutate(i.operation_set)
                    # チェックする。
                    task, task_feature = holder.get_result(mutated_operation_set)
                    break
                except OperationInconsistencyException:
                    continue

            applied_task, applied_task_feature = holder.get_result(mutated_operation_set)
            mutation_distance = evaluator.evaluate_task_feature(applied_task_feature)
            mutated_individuals.append(Individual(mutated_operation_set, mutation_distance, applied_task_feature))

        self.individuals = mutated_individuals

    def select(self):
        if self.strategy == 'simple':
            self.individuals = self.select_simple()
        elif self.strategy == 'nsga2':
            self.individuals = self.select_nsga2()
        else:
            raise NotImplementedError()

    def select_nsga2(self):
        raw_len = len(self.individuals)
        selected = selNSGA2(self.individuals, raw_len)

        simple_selection = self.select_simple(include_elite=True)

        return selected + simple_selection[:raw_len - len(selected)]

    def select_simple(self, include_elite: bool = True):
        # 選択
        self.sort()
        if include_elite:
            next_individuals = [self.get_elite()]
        else:
            next_individuals = []
        # score = 1 / distance # TODO Handle 0 division
        score_sum = sum(map(lambda i: 1 / i.distance, self.individuals))
        score_ratios = [1 / i.distance / score_sum for i in self.individuals]
        score_roulette = np.cumsum(score_ratios)

        for _ in range(len(self.individuals) - 1):
            roulette_prob_hit = random.uniform(0, 1)
            for i, roulette_prob in enumerate(score_roulette):
                if roulette_prob_hit < roulette_prob:
                    next_individuals.append(self.individuals[i])
                    break

        return next_individuals


@dataclass
class TreeBaseSearchEngine:
    time_out: int = 60  # TODO

    def get_first_individual(self, evaluator, mutator, holder, task, root_task_feature):
        try:
            operation = mutator.get_random_one_operation(task, root_task_feature)
            operation_set = OperationSet([operation])
            _, task_feature = holder.get_result(operation_set)
            distance = evaluator.evaluate_task_feature(task_feature)
            return Individual(operation_set, distance, task_feature)
        except OperationInconsistencyException:
            return self.get_first_individual(evaluator, mutator, holder, task, root_task_feature)

    def search(self, task: Task, params: AllParameter, verbose: bool = False) -> Union[AnsweredSearchResults, NotAnsweredSearchResult]:
        evaluator = DistanceEvaluator(params.distance_evaluator_param)
        holder = OperationSetExecutionResultHolder(task, {})
        root_operation_set = OperationSet([])
        _, root_task_feature = holder.get_result(root_operation_set)

        if RunConfig.USE_ML_GUIDE:
            operation_element_prob_dict = predict_operation_element_inclusion()
        else:
            operation_element_prob_dict = defaultdict(lambda: 1)

        if verbose:
            print(operation_element_prob_dict)

        mutator: OperationSetMutator = OperationSetMutator(holder, operation_element_prob_dict)

        individuals = [self.get_first_individual(evaluator, mutator, holder, task, root_task_feature) for _ in range(TreeBaseSearchEngineParameter.population_num)]

        population = Population('simple', individuals)
        with Timer() as timer:

            for i in range(10000000):

                if verbose:
                    print(f'============== generation: {i} population')
                    population.show()

                population.mutate(mutator, holder, evaluator)

                if verbose:
                    print(f'============== generation: {i}, mutation population')
                    population.show()

                answer_operation_set = population.get_dist0_if_exists()
                if answer_operation_set is not None:
                    if AnswerMatcher.is_train_all_match_if_operated(task, answer_operation_set):
                        return AnsweredSearchResults(task, [AnsweredSearchResult(answer_operation_set)], timer.second(), i)
                    else:
                        raise NotImplementedError()

                population.select()

                if timer.second() > self.time_out:
                    return NotAnsweredSearchResult(task, TimeoutException(), timer.second(), i)

            return NotAnsweredSearchResult(task, MaxNodeExceededException(), timer.second(), i)
