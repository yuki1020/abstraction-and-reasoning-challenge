import optuna
from itertools import chain
from joblib import Parallel, delayed
from optuna import Trial
from pathlib import Path
from tqdm import tqdm
from typing import List, Union, Optional

from abstraction_and_reasoning_challenge.config import PathConfig
from abstraction_and_reasoning_challenge.config import RunConfig
from abstraction_and_reasoning_challenge.param_config import TreeBaseSearchEngineParameter, AllParameter, NodeBaseSearchEngineParameter
from abstraction_and_reasoning_challenge.src.answer_storage.answer_storage import update_answer_storage
from abstraction_and_reasoning_challenge.src.domain.search_engine.engine.engine_loader import get_engine
from abstraction_and_reasoning_challenge.src.domain.search_engine.engine.enums import EngineType
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.domain.task.task import Task
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task_result import NotAnsweredSearchResult, AnsweredSearchResults, summary_engine_results
from abstraction_and_reasoning_challenge.src.submit.submit_util import create_submission, save_submission_df
from abstraction_and_reasoning_challenge.src.visualize.visualize import plot_task


def optimize_node_base_search(tasks: List[Task]):
    assert RunConfig.ENGINE_TYPE == EngineType.NODE_BASED_SEARCH_ENGINE

    def objective(trial: Trial):
        param = AllParameter(
            # distance_evaluator_param=DistanceEvaluatorParameter(
            #     same_h_w_dim_between_input_output=trial.suggest_loguniform('same_h_w_dim_between_input_output', 100, 10000),
            #     all_dim_h_w_integer_multiple=trial.suggest_loguniform('all_dim_h_w_integer_multiple', 10, 1000),
            #     mean_lack_color_num=trial.suggest_loguniform('mean_lack_color_num', 1, 100),
            #     mean_excess_color_num=trial.suggest_loguniform('mean_excess_color_num', 1, 100),
            #     mean_hit_and_miss_histogram_diff=trial.suggest_loguniform('mean_hit_and_miss_histogram_diff', 1, 100),
            #     mean_h_v_diff_input_arr_line_num=trial.suggest_loguniform('mean_h_v_diff_input_arr_line_num', 1, 100),
            #     mean_h_v_diff_output_arr_line_num=trial.suggest_loguniform('mean_h_v_diff_output_arr_line_num', 1, 100),
            #     mean_h_v_edge_sum_diff=trial.suggest_discrete_uniform('mean_h_v_edge_sum_diff', 0, 2, 0.5),
            #     mean_h_v_edge_sum_diff_ratio=trial.suggest_discrete_uniform('mean_h_v_edge_sum_diff_ratio', 0, 2, 0.5),
            # mean_diff_cell_where_no_need_to_change_count_ratio=trial.suggest_loguniform('mean_diff_cell_where_no_need_to_change_count_ratio', 1, 100000),
            # ),

            node_base_engine_param=NodeBaseSearchEngineParameter(
                # breadth_first_cost=trial.suggest_loguniform('breadth_first_cost', 1000, 100000),
                normal_first_cost=trial.suggest_loguniform('normal_first_cost', 10, 1000),
                depth_first_cost=trial.suggest_loguniform('depth_first_cost', 0.1, 10),
                # breadth_first_exp_cost=trial.suggest_loguniform('exp_cost', 0.001, 3),
                # normal_exp_cost=trial.params['exp_cost'],
                # depth_first_exp_cost=trial.params['exp_cost'],
                pq_pop_mins_or_as_least_n=trial.suggest_int('pq_pop_mins_or_as_least_n', 1, 10),
                #     element_inclusion_prob_factor=trial.suggest_loguniform('element_inclusion_prob_factor', 0.001, 10000000),
            )
        )

        print(trial.params)

        engine_results = solve_tasks(tasks, param, add_answer_storage=True)
        answered_results = [r for r in engine_results if isinstance(r, AnsweredSearchResults)]
        true_results = [r for r in engine_results if r.final_test_correct()]

        all_len = len(engine_results)
        true_len = len(true_results)
        false_len = len(answered_results) - len(true_results)
        none_len = len(engine_results) - len(answered_results)

        print(trial.params)
        print(f'true: {true_len}, false: {false_len}, none: {none_len}, all: {all_len}')
        return all_len - true_len - false_len / 2

    study = optuna.create_study()
    study.optimize(objective, n_trials=1000)

    print(study.best_params)


def optimize_tree_base_search(tasks: List[Task]):
    assert RunConfig.ENGINE_TYPE == EngineType.TREE_BASED_SEARCH_ENGINE

    def objective(trial: Trial):
        all_parameter = AllParameter(
            tree_base_engine_param=TreeBaseSearchEngineParameter(
                population_num=trial.suggest_int('population_num', 20, 80),
                max_depth=trial.suggest_int('max_depth', 6, 10),
                operation_mutation_prob=trial.suggest_loguniform('operation_mutation_prob', 0.01, 0.5),
                operation_component_mutation_prob=trial.suggest_loguniform('operation_component_mutation_prob', 0.005, 0.5),
                operation_param_mutation_prob=trial.suggest_loguniform('operation_param_mutation_prob', 0.001, 0.5),
                extend_mutation_prob=trial.suggest_loguniform('extend_mutation_prob', 0.01, 1),
                shrink_mutation_prob=trial.suggest_loguniform('shrink_mutation_prob', 0.001, 0.1),
            ))
        print(trial.params)

        engine_results = solve_tasks(tasks, all_parameter, add_answer_storage=True)
        answered_results = [r for r in engine_results if isinstance(r, AnsweredSearchResults)]
        true_results = [r for r in engine_results if r.final_test_correct()]

        all_len = len(engine_results)
        true_len = len(true_results)
        false_len = len(answered_results) - len(true_results)
        none_len = len(engine_results) - len(answered_results)

        print(f'true: {true_len}, false: {false_len}, none: {none_len}, all: {all_len}')
        return all_len - true_len - false_len / 2

    study = optuna.create_study()
    study.optimize(objective, n_trials=1000)

    print(study.best_params)


def solve_tasks(tasks: List[Task],
                params: AllParameter,
                output_summary_path: Optional[Path] = None,
                save_submission: bool = False,
                copy_wrong_answers_root_tag: Optional[str] = None,
                add_answer_storage: bool = False,
                verbose: bool = False) \
        -> List[Union[AnsweredSearchResults, NotAnsweredSearchResult]]:
    print('===== start parallel solve tasks =====\n\n')
    if RunConfig.N_JOB == 1 or len(tasks) == 1:
        engine_results = [solve_task(task, params, verbose) for task in tqdm(tasks, miniters=0, mininterval=None, maxinterval=None)]
    else:
        # with Pool(processes=RunConfig.N_JOB) as pool:
        #     args = ((task, verbose) for task in tqdm(tasks, miniters=0, mininterval=None, maxinterval=None))
        #     engine_results = pool.starmap(solve_task, args)

        # 'multiprocessing' or 'threading'
        engine_results = Parallel(n_jobs=RunConfig.N_JOB, backend='multiprocessing') \
            (delayed(solve_task)(task, params, verbose) for task in tqdm(tasks, miniters=0, mininterval=None, maxinterval=None))

    print('===== end parallel solve tasks =====\n\n')

    summary = summary_engine_results(engine_results)
    print(summary)

    if output_summary_path:
        output_summary_path.write_text(summary)

    if save_submission:
        print('start save submission')
        submission_df = create_submission(engine_results)
        save_submission_df(submission_df)

    if add_answer_storage:
        storage_elements = list(chain.from_iterable([r.to_answer_storage_elements() for r in engine_results if isinstance(r, AnsweredSearchResults)]))
        update_answer_storage(storage_elements)

    if copy_wrong_answers_root_tag:
        print('start copy wrong answers')
        for r in engine_results:
            if not r.final_test_correct():
                plot_task(r.task, show=False, save_path=PathConfig.WRONG_ANSWERS_ROOT / copy_wrong_answers_root_tag / f'{r.task.name}.png')

    return engine_results


def solve_task(task: Task, params: AllParameter, verbose: bool = False) -> Union[AnsweredSearchResults, NotAnsweredSearchResult]:
    try:
        engine = get_engine(RunConfig.ENGINE_TYPE)
        engine_result = engine.search(task, params, verbose)
    except Exception as e:
        print(f'unknown error {task.name}')
        raise e

    if isinstance(engine_result, NotAnsweredSearchResult):
        return engine_result
    elif isinstance(engine_result, AnsweredSearchResults):
        # calculate operation_set-executed task.
        for result in engine_result.results:
            applied_task = TaskOperationSetExecutor().execute(task, result.operation_set)
            result.test_output_arr = [io.input_arr for io in applied_task.test]
            result.test_correct = AnswerMatcher.is_train_test_all_match_if_operated(task, result.operation_set)

        engine_result.results = sorted(engine_result.results, key=lambda r: r.test_correct, reverse=True)
        print(engine_result.summary())
        return engine_result
    else:
        raise NotImplementedError()
