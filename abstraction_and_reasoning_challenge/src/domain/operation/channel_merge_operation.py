from operator import itemgetter

import numpy as np
from typing import List, Tuple

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import ChannelMergeOperation
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color


class ColorChannelOverrideOperation(ChannelMergeOperation):

    def __call__(self, arr: np.ndarray, original_color_mask_paris: List[Tuple[Color, np.ndarray]], color_mask_pairs: List[Tuple[Color, np.ndarray]]) -> np.ndarray:
        diff_mask_paris = [(c1, np.logical_and(np.logical_xor(o_m, c_m), c_m))
                           for (c1, o_m), (c2, c_m) in zip(sorted(original_color_mask_paris, key=itemgetter(0)),
                                                           sorted(color_mask_pairs, key=itemgetter(0)))]  # TODO should groupby color?

        check_mask = np.full_like(diff_mask_paris[0][1], fill_value=False)

        # If duplicated, InconsistencyException
        for _, m in diff_mask_paris:
            if check_mask[m].any():
                raise OperationInconsistencyException('failed channel merge')
            check_mask[m] = True

        for c, m in diff_mask_paris:
            arr[m] = c

        return arr
