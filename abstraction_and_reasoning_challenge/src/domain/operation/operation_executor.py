from copy import deepcopy

import numpy as np
from typing import Union, Optional, List, Tuple

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import UniformOperation, OperationSet, MaskOperation, MaskConversion, ColorSelection, ColorOperation, MultiColorChannelOperation, ColorChannelSelection, ChannelMergeOperation, PartitionOperation, PartitionSelection, \
    PartitionMergeOperation
from abstraction_and_reasoning_challenge.src.domain.operation.mask_conversion import NoMaskConversion
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color


class OperationSetExecutor:

    @classmethod
    def apply_operation_set(cls, arrays: List[np.ndarray], operation_set: OperationSet) -> List[np.ndarray]:
        for o in operation_set.operations:
            arrays = cls.apply_operation(arrays, o)

        return arrays

    @classmethod
    def apply_operation(cls, arrays: List[np.ndarray], operation: Union[UniformOperation, ColorOperation, MultiColorChannelOperation]) -> List[np.ndarray]:
        if isinstance(operation, UniformOperation):
            return cls.apply_uniform_operation(arrays, operation)
        elif isinstance(operation, ColorOperation):
            masks = cls.apply_color_selection(arrays, operation.color_selection)
            masks = cls.apply_mask_conversion(masks, operation.mask_conversions)
            return cls.apply_mask_operation(arrays, masks, operation.mask_operation)
        elif isinstance(operation, MultiColorChannelOperation):
            original_color_mask_pairs_list = cls.apply_channel_selection(arrays, operation.channel_selection)
            color_mask_pairs_list = deepcopy(original_color_mask_pairs_list)
            color_mask_pairs_list = cls.apply_color_channel_mask_conversion(color_mask_pairs_list, operation.mask_conversions)
            return cls.apply_channel_merge(arrays, original_color_mask_pairs_list, color_mask_pairs_list, operation.channel_merge_operation)
        elif isinstance(operation, PartitionOperation):
            partitioned_arrays_original_location_masks = cls.apply_partition_selection(arrays, operation.partition_selection)
            return cls.apply_partition_merge_operation(arrays, partitioned_arrays_original_location_masks, operation.partition_merge_operation)
        else:
            raise NotImplementedError()

    @classmethod
    def apply_uniform_operation(cls, arrays: List[np.ndarray], operation: UniformOperation) -> List[np.ndarray]:
        new_arrays = [cls._apply_uniform_operation(a, operation) for a in arrays]

        if all(np.array_equal(n, r) for n, r in zip(new_arrays, arrays)):
            raise OperationInconsistencyException(f'no effect. {operation}')
        return new_arrays

    @classmethod
    def _apply_uniform_operation(cls, arr: np.ndarray, operation: UniformOperation) -> np.ndarray:
        cls._check_arr(arr, operation)

        temp_arr = deepcopy(arr)
        new_arr = operation(temp_arr)
        cls._check_arr(new_arr, operation)
        return new_arr

    @classmethod
    def apply_color_selection(cls, arrays: List[np.ndarray], selection: ColorSelection) -> List[np.ndarray]:
        return [cls._apply_color_selection(a, selection) for a in arrays]

    @classmethod
    def _apply_color_selection(cls, arr: np.ndarray, selection: ColorSelection) -> np.ndarray:
        cls._check_arr(arr, None)

        temp_arr = deepcopy(arr)
        mask = selection(temp_arr)
        cls._check_mask(mask, selection)
        return mask

    @classmethod
    def apply_channel_selection(cls, arrays: List[np.ndarray], channel_selection: ColorChannelSelection) -> List[List[Tuple[Color, np.ndarray]]]:
        return [cls._apply_channel_selection(a, channel_selection) for a in arrays]

    @classmethod
    def _apply_channel_selection(cls, arr: np.ndarray, channel_selection: ColorChannelSelection) -> List[Tuple[Color, np.ndarray]]:
        cls._check_arr(arr, None)

        temp_arr = deepcopy(arr)
        color_mask_pairs = channel_selection(temp_arr)

        for c, m in color_mask_pairs:
            cls._check_mask(m, channel_selection)
        return color_mask_pairs

    @classmethod
    def apply_color_channel_mask_conversion(cls, color_mask_pairs_list: List[List[Tuple[Color, np.ndarray]]], mask_conversion: MaskConversion) -> List[List[Tuple[Color, np.ndarray]]]:
        new_color_mask_pairs_list = [cls._apply_color_channel_mask_conversion(p, mask_conversion) for p in color_mask_pairs_list]
        # TODO imple
        # if not isinstance(mask_conversion, NoMaskConversion):
        #     if all(np.array_equal(n, r) for n, r in zip(new_color_mask_pairs_list, color_mask_pairs_list)):
        #         raise OperationInconsistencyException(mask_conversion)
        return new_color_mask_pairs_list

    @classmethod
    def _apply_color_channel_mask_conversion(cls, color_mask_pairs: List[Tuple[Color, np.ndarray]], mask_conversion: MaskConversion) -> List[Tuple[Color, np.ndarray]]:
        for c, m in color_mask_pairs:
            cls._check_mask(m, None)

        temp_color_mask_pairs = deepcopy(color_mask_pairs)
        temp_color_mask_pairs = [(c, mask_conversion(m)) for c, m in temp_color_mask_pairs]

        for c, m in temp_color_mask_pairs:
            cls._check_mask(m, mask_conversion)

        return temp_color_mask_pairs

    @classmethod
    def apply_channel_merge(cls, arrays: List[np.ndarray], original_color_mask_pairs_list: List[List[Tuple[Color, np.ndarray]]], color_mask_pairs_list: List[List[Tuple[Color, np.ndarray]]], merge_operation: ChannelMergeOperation) -> List[np.ndarray]:
        new_arrays = [cls._apply_channel_merge(arr, o_p, p, merge_operation) for arr, o_p, p in zip(arrays, original_color_mask_pairs_list, color_mask_pairs_list)]

        if all(np.array_equal(n, r) for n, r in zip(new_arrays, arrays)):
            raise OperationInconsistencyException(f'no effect. {merge_operation}')
        return new_arrays

    @classmethod
    def _apply_channel_merge(cls, arr: np.ndarray, original_color_mask_pairs: List[Tuple[Color, np.ndarray]], color_mask_pairs: List[Tuple[Color, np.ndarray]], merge_operation: ChannelMergeOperation) -> np.ndarray:
        cls._check_arr(arr, None)
        for c, m in color_mask_pairs:
            cls._check_mask(m, None)

        temp_arr = deepcopy(arr)
        temp_original_color_mask_pairs = deepcopy(original_color_mask_pairs)
        temp_color_mask_pairs = deepcopy(color_mask_pairs)

        new_arr = merge_operation(temp_arr, temp_original_color_mask_pairs, temp_color_mask_pairs)
        cls._check_arr(new_arr, merge_operation)
        return new_arr

    @classmethod
    def apply_mask_conversion(cls, masks: List[np.ndarray], mask_conversion: MaskConversion) -> List[np.ndarray]:
        new_masks = [cls._mask_conversion(m, mask_conversion) for m in masks]
        if not isinstance(mask_conversion, NoMaskConversion):
            if all(np.array_equal(n, r) for n, r in zip(new_masks, masks)):
                raise OperationInconsistencyException(f'no effect. {mask_conversion}')
        return new_masks

    @classmethod
    def _mask_conversion(cls, mask: np.ndarray, mask_conversion: MaskConversion) -> np.ndarray:
        cls._check_mask(mask, None)

        temp_mask = deepcopy(mask)
        applied_mask = mask_conversion(temp_mask)
        cls._check_mask(applied_mask, mask_conversion)
        return applied_mask

    @classmethod
    def apply_mask_operation(cls, arrays: List[np.ndarray], masks: List[np.ndarray], mask_operation: MaskOperation) -> List[np.ndarray]:
        new_arrays = [cls._apply_mask_operation(a, m, mask_operation) for a, m in zip(arrays, masks)]
        if all(np.array_equal(n, r) for n, r in zip(new_arrays, arrays)):
            raise OperationInconsistencyException(f'no effect. {mask_operation}')
        return new_arrays

    @classmethod
    def _apply_mask_operation(cls, arr: np.ndarray, mask: np.ndarray, mask_operation: MaskOperation) -> np.ndarray:
        cls._check_arr(arr, None)
        cls._check_mask(mask, None)

        temp_arr, temp_mask = deepcopy(arr), deepcopy(mask)
        applied_arr = mask_operation(temp_arr, temp_mask)
        cls._check_arr(applied_arr, mask_operation)
        return applied_arr

    @staticmethod
    def _check_arr(arr: np.ndarray, operation: Optional[UniformOperation]):
        assert isinstance(arr, np.ndarray), f'operation: {operation}, type: {type(arr)}'
        assert arr.dtype == np.uint8, f'operation: {operation}, dtype: {arr.dtype}'
        assert arr.size != 0, f'operation: {operation}, operation_result: \n{arr}'
        assert 0 <= np.min(arr) <= np.max(arr) <= 10, f'operation: {operation}, operation_result: \n{arr}'
        assert len(arr.shape) == 2, f'operation: {operation}, operation_result: \n{arr}'

    @staticmethod
    def _check_mask(mask: np.ndarray, operation: Union[ColorSelection, MaskConversion, None]):
        assert isinstance(mask, np.ndarray), f'selection: {operation}, type: {type(mask)}'
        assert mask.dtype == bool, f'selection: {operation}, dtype: {mask.dtype}'
        assert len(mask.shape) == 2, f'selection: {operation}, result: \n{mask}'

    @classmethod
    def apply_partition_selection(cls, arrays: List[np.ndarray], partition_selection: PartitionSelection) -> List[Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]]:
        return [cls._apply_partition_selection(a, partition_selection) for a in arrays]

    @classmethod
    def _apply_partition_selection(cls, arr: np.ndarray, partition_selection: PartitionSelection) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        cls._check_arr(arr, None)
        temp_arr = deepcopy(arr)
        return partition_selection(temp_arr)

    @classmethod
    def apply_partition_merge_operation(cls, arrays: List[np.ndarray], partitioned_arrays_original_location_masks: List[Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]], partition_merge_operation: PartitionMergeOperation):
        return [cls._apply_partition_merge_operation(a, p, partition_merge_operation) for a, p in zip(arrays, partitioned_arrays_original_location_masks)]

    @classmethod
    def _apply_partition_merge_operation(cls, arr: np.ndarray, partitioned_arrays_original_location_masks: Tuple[List[List[np.ndarray]], List[List[np.ndarray]]], partition_merge_operation: PartitionMergeOperation):
        partitioned_arrays, original_location_masks = partitioned_arrays_original_location_masks

        cls._check_arr(arr, None)
        temp_arr = deepcopy(arr)
        temp_partitioned_arrays = deepcopy(partitioned_arrays)
        temp_original_location_masks = deepcopy(original_location_masks)

        res_arr = partition_merge_operation(temp_arr, temp_partitioned_arrays, temp_original_location_masks)
        cls._check_arr(res_arr, partition_merge_operation)

        return res_arr
