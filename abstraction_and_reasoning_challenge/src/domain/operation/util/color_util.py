from operator import itemgetter

import numpy as np
from typing import Tuple, List

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color, SingleColorSelectionMode, BackGroundColorSelectionMode


class ColorSelectionUtil:

    def select_single_color(self, arr: np.ndarray, mode: SingleColorSelectionMode) -> Color:
        if mode == SingleColorSelectionMode.MOST_COMMON:
            color_counts = self.get_color_counts(arr)
            if len(color_counts) <= 0:
                raise OperationInconsistencyException('color <= 0')
            try:
                if color_counts[-1][1] == color_counts[-2][1]:  # Two maximums.
                    raise OperationInconsistencyException('duplicated max color')
            except IndexError:
                pass
            return Color.of(color_counts[-1][0])

        elif mode == SingleColorSelectionMode.SECOND_MOST_COMMON:
            color_counts = self.get_color_counts(arr)
            if len(color_counts) <= 1:
                raise OperationInconsistencyException('color <= 1')
            if color_counts[-1][1] == color_counts[-2][1]:  # Two maximums.
                raise OperationInconsistencyException('duplicated max color')
            try:
                if color_counts[-2][1] == color_counts[-3][1]:  # Two 2nd maximums.
                    raise OperationInconsistencyException('duplicated 2nd max color')
            except IndexError:
                pass
            return Color.of(color_counts[-2][0])
        elif mode == SingleColorSelectionMode.LEAST_COMMON:
            color_counts = self.get_color_counts(arr)
            if len(color_counts) <= 1:
                raise OperationInconsistencyException('color <= 1')
            if color_counts[0][1] == color_counts[1][1]:  # Two minimum.
                raise OperationInconsistencyException('duplicated 2nd max color')

            return Color.of(color_counts[0][0])
        else:
            raise NotImplementedError()

    def get_background_color(self, arr: np.ndarray, mode: BackGroundColorSelectionMode) -> Color:
        if mode == BackGroundColorSelectionMode.BLACK:
            return Color.BLACK
        elif mode == BackGroundColorSelectionMode.MOST_COMMON:
            return self.select_single_color(arr, SingleColorSelectionMode.MOST_COMMON)
        else:
            raise NotImplementedError()

    def get_color_counts(self, arr: np.ndarray) -> List[Tuple[int, int]]:
        color_counts = [(color, count) for color, count in enumerate(np.bincount(arr.ravel(), minlength=10)) if count != 0]
        return sorted(color_counts, key=itemgetter(1))

    def get_colors(self, arr: np.ndarray) -> List[Color]:
        return sorted(set(arr.ravel().tolist()))

    def select_multi_color(self):
        # TODO imple
        raise
