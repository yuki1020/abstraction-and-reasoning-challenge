# noinspection PyUnresolvedReferences
from dataclasses import dataclass

# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.base import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.channel_merge_operation import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.color_channel_selection import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.color_selection import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.mask_conversion import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.mask_operation import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.partition_merge_operation import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.partition_selection import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.partitioned_array_selection import *
# noinspection PyUnresolvedReferences
from abstraction_and_reasoning_challenge.src.domain.operation.uniform_operation import *


def str_to_operation_set(s: str) -> OperationSet:
    # DSL string -> DSL object
    return eval(s)


def str_to_AnswerStorageElement(s: str):
    # noinspection PyUnresolvedReferences
    from abstraction_and_reasoning_challenge.src.answer_storage.answer_storage import AnswerStorageElement
    return eval(s)
