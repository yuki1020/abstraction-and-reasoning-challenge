from enum import IntEnum, Enum, unique, auto

import numpy as np
from scipy.ndimage import generate_binary_structure


# DSL Enums

class StrNameIntEnum(IntEnum):
    def __str__(self):
        return self.name

    def __repr__(self):
        return str(f'{self.__class__.__name__}.{self.name}')


class StrNameEnum(Enum):
    def __str__(self):
        return self.name

    def __repr__(self):
        return str(f'{self.__class__.__name__}.{self.name}')


@unique
class Color(StrNameIntEnum):
    BLACK = 0
    BLUE = 1
    RED = 2
    GREEN = 3
    YELLOW = 4
    GRAY = 5
    MAGENTA = 6
    ORANGE = 7
    SKY = 8
    BROWN = 9

    @classmethod
    def prepare(cls):
        cls.mapping = {c.value: c for c in Color}

    @classmethod
    def of(cls, value: int) -> 'Color':
        try:
            return cls.mapping[value]
        except AttributeError:
            cls.mapping = {c.value: c for c in Color}
            return cls.mapping[value]


@unique
class BackGroundColorSelectionMode(StrNameEnum):
    BLACK = auto()
    MOST_COMMON = auto()


@unique
class SingleColorSelectionMode(StrNameEnum):
    MOST_COMMON = auto()
    SECOND_MOST_COMMON = auto()
    LEAST_COMMON = auto()


@unique
class MultiColorSelectionMode(StrNameEnum):
    # ANY_WITHOUT_FIXED_COLOR = auto()  # TODO should define?
    ANY_WITHOUT_MOST_COMMON = auto()  # TODO ANY_WITHOUT_TOP2_MOST_COMMON
    ANY_WITHOUT_LEAST_COMMON = auto()


@unique
class FlipMode(StrNameEnum):
    UD = auto()
    LR = auto()
    UL_DR = auto()
    UR_DL = auto()


@unique
class Axis(StrNameEnum):
    VERTICAL = auto()
    HORIZONTAL = auto()
    BOTH = auto()


@unique
class AxisV2(StrNameEnum):
    VERTICAL = auto()
    HORIZONTAL = auto()
    VERTICAL_HORIZONTAL = auto()
    MAIN_DIAGONAL = auto()
    ANTI_DIAGONAL = auto()
    BOTH_DIAGONAL = auto()


@unique
class Direction(StrNameEnum):
    TOP = auto()
    BOTTOM = auto()
    RIGHT = auto()
    LEFT = auto()


@unique
class Corner(StrNameEnum):
    TOP_LEFT = auto()
    TOP_RIGHT = auto()
    BOTTOM_RIGHT = auto()
    BOTTOM_LEFT = auto()


@unique
class SpiralDirection(StrNameEnum):
    CLOCKWISE = auto()
    ANTICLOCKWISE = auto()


@unique
class LineEdgeType(StrNameEnum):
    EdgeExclude = auto()
    EdgeInclude = auto()


@unique
class FillType(StrNameEnum):
    NotOverride = auto()
    Override = auto()


@unique
class PaddingMode(StrNameEnum):
    REPEAT = auto()
    MIRROR_1 = auto()  # line-symmetric at the edge
    MIRROR_2 = auto()  # line-symmetric at the edge-pixel-line
    EDGE = auto()  # Extend the constant value at the edge


@unique
class MaxOrMin(StrNameEnum):
    MAX = max
    MIN = min

    @property
    def func(self):
        return self.value


@unique
class ObjectFeature(StrNameEnum):
    AREA = auto()
    # PERIMETER_LEN = auto() # TODO difficult to implement?
    HORIZONTAL_LEN = auto()
    VERTICAL_LEN = auto()


@unique
class PixelConnectivity(StrNameEnum):
    FOUR_DIRECTION = 1
    EIGHT_DIRECTION = 2

    @property
    def value_for_skimage(self) -> int:
        return self.value

    @property
    def structure_for_skimage(self) -> np.ndarray:
        if self == PixelConnectivity.EIGHT_DIRECTION:
            return generate_binary_structure(2, 2)
        if self == PixelConnectivity.FOUR_DIRECTION:
            return generate_binary_structure(2, 1)

        raise NotImplementedError()


@unique
class ImageEdgeType(StrNameEnum):
    EDGE_EXCLUDE = auto()
    EDGE_INCLUDE = auto()


@unique
class HoleInclude(StrNameEnum):
    INCLUDE = auto()
    EXCLUDE = auto()


@unique
class TrueOrFalse(StrNameEnum):
    TRUE = auto()
    FALSE = auto()
