import numpy as np
from dataclasses import dataclass
from operator import itemgetter
from typing import Tuple, List

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import PartitionSelection
from abstraction_and_reasoning_challenge.src.domain.operation.image_to_int import ImageToIntFunc
from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Axis, Color, BackGroundColorSelectionMode


@dataclass(frozen=True)
class LinePartition(PartitionSelection):
    line_color: Color

    def __call__(self, arr: np.ndarray) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        if arr.size == 1:
            raise OperationInconsistencyException('size == 1')

        if 1 in arr.shape and len(np.unique(arr)) == 1:
            raise OperationInconsistencyException('can not separate')

        color_hit: np.ndarray = arr == self.line_color
        line_v_indices = np.where(color_hit.all(axis=1))[0]
        line_h_indices = np.where(color_hit.all(axis=0))[0]

        if len(line_v_indices) == len(line_h_indices) == 0:
            raise OperationInconsistencyException('not line found')

        if 1 in np.diff(line_v_indices) or 1 in np.diff(line_h_indices):
            raise OperationInconsistencyException('line duplicated')

        partitioned_arrays = []
        partitioned_masks = []
        for start_v_i, end_v_i in zip([0] + list(line_v_indices + 1), list(line_v_indices) + [arr.shape[0]]):
            if start_v_i == end_v_i:
                continue
            partitioned_temp_arrays = []
            partitioned_temp_masks = []
            for start_h_i, end_h_i in zip([0] + list(line_h_indices + 1), list(line_h_indices) + [arr.shape[1]]):
                if start_h_i == end_h_i:
                    continue
                partitioned_temp_arrays.append(arr[start_v_i:end_v_i, start_h_i:end_h_i])

                mask = np.full_like(arr, fill_value=False, dtype=bool)
                mask[start_v_i:end_v_i, start_h_i:end_h_i] = True
                partitioned_temp_masks.append(mask)

            partitioned_arrays.append(partitioned_temp_arrays)
            partitioned_masks.append(partitioned_temp_masks)

        return partitioned_arrays, partitioned_masks


@dataclass(frozen=True)
class GeneralizedLinePartition(PartitionSelection):
    bg_selection_mode: BackGroundColorSelectionMode

    def __call__(self, arr: np.ndarray) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        if arr.size == 1:
            raise OperationInconsistencyException('size == 1')

        if 1 in arr.shape and len(np.unique(arr)) == 1:
            raise OperationInconsistencyException('can not separate')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        colors = [Color.of(c) for c in np.unique(arr)]

        color_lines = []
        for c in colors:
            if c == bg:
                continue
            color_hit: np.ndarray = arr == c
            line_v_indices = np.where(color_hit.all(axis=1))[0]
            line_h_indices = np.where(color_hit.all(axis=0))[0]
            color_lines.append((c, len(line_v_indices) + len(line_h_indices)))

        if len(color_lines) == 0:
            raise OperationInconsistencyException('not colored')

        target_color = max(color_lines, key=itemgetter(1))[0]

        return LinePartition(target_color)(arr)


@dataclass(frozen=True)
class IntegerDivisionPartition(PartitionSelection):
    axis: Axis
    n_split: int

    def __call__(self, arr: np.ndarray) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        if self.axis == Axis.HORIZONTAL:
            if arr.shape[1] % self.n_split != 0:
                raise OperationInconsistencyException('can not divide')

            masks = []
            partition_len = arr.shape[1] // self.n_split
            for i in range(self.n_split):
                mask = np.full_like(arr, fill_value=False, dtype=bool)
                start_i, end_i = i * partition_len, (i + 1) * partition_len
                mask[:, start_i:end_i] = True
                masks.append(mask)
            masks = [masks]

            partitioned_arrays = np.split(arr, self.n_split, axis=1)
            partitioned_arrays = [partitioned_arrays]

        elif self.axis == Axis.VERTICAL:
            if arr.shape[0] % self.n_split != 0:
                raise OperationInconsistencyException('can not divide')

            masks = []
            partition_len = arr.shape[0] // self.n_split
            for i in range(self.n_split):
                mask = np.full_like(arr, fill_value=False, dtype=bool)
                start_i, end_i = i * partition_len, (i + 1) * partition_len
                mask[start_i:end_i, :] = True
                masks.append(mask)
            masks = [[m] for m in masks]

            partitioned_arrays = np.split(arr, self.n_split, axis=0)
            partitioned_arrays = [[a] for a in partitioned_arrays]
        elif self.axis == Axis.BOTH:
            if arr.shape[0] % self.n_split != 0 or arr.shape[1] % self.n_split != 0:
                raise OperationInconsistencyException('can not divide')

            masks = []
            v_partition_len = arr.shape[0] // self.n_split
            h_partition_len = arr.shape[1] // self.n_split
            for i in range(self.n_split):
                temp_masks = []
                v_start_i, v_end_i = i * v_partition_len, (i + 1) * v_partition_len
                for j in range(self.n_split):
                    mask = np.full_like(arr, fill_value=False, dtype=bool)
                    h_start_i, h_end_i = j * h_partition_len, (j + 1) * h_partition_len
                    mask[v_start_i:v_end_i, h_start_i:h_end_i] = True
                    temp_masks.append(mask)
                masks.append(temp_masks)

            partitioned_arrays = np.split(arr, self.n_split, axis=0)
            partitioned_arrays = [np.split(a, self.n_split, axis=1) for a in partitioned_arrays]

        else:
            raise NotImplementedError()

        return partitioned_arrays, masks


@dataclass(frozen=True)
class DynamicIntegerDivisionPartition(PartitionSelection):
    axis: Axis
    image_to_int_func: ImageToIntFunc

    def __call__(self, arr: np.ndarray) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        num = self.image_to_int_func(arr)
        if num == 0:
            raise OperationInconsistencyException('0_division')

        return IntegerDivisionPartition(self.axis, num)(arr)


@dataclass(frozen=True)
class ColorNumIntegerDivisionPartition(PartitionSelection):
    axis: Axis

    def __call__(self, arr: np.ndarray) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        color_num = len(np.unique(arr))
        color_num = color_num - 1  # bg

        if color_num == 0:
            raise OperationInconsistencyException('not colored')

        return IntegerDivisionPartition(self.axis, color_num)(arr)
