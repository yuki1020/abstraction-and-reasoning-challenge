import numpy as np
from dataclasses import dataclass

from abstraction_and_reasoning_challenge.src.domain.operation.base import MaskOperation
from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color, SingleColorSelectionMode


@dataclass(frozen=True)
class FixedColorMaskFill(MaskOperation):
    color: Color

    def __call__(self, arr: np.ndarray, mask: np.ndarray) -> np.ndarray:
        arr[mask] = self.color
        return arr


@dataclass(frozen=True)
class SingleColorMaskFill(MaskOperation):
    single_color_selection_mode: SingleColorSelectionMode

    def __call__(self, arr: np.ndarray, mask: np.ndarray) -> np.ndarray:
        color = ColorSelectionUtil().select_single_color(arr, self.single_color_selection_mode)
        arr[mask] = color
        return arr


@dataclass(frozen=True)
class MaskCoordsCrop(MaskOperation):

    def __call__(self, arr: np.ndarray, mask: np.ndarray) -> np.ndarray:
        # TODO raise OperationInconsistencyException?
        if not mask.any():
            return arr

        coords = np.argwhere(mask)

        x_min, y_min = coords.min(axis=0)
        x_max, y_max = coords.max(axis=0)

        return arr[x_min:x_max + 1, y_min:y_max + 1]
