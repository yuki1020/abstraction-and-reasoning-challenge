import numpy as np
from dataclasses import dataclass
from itertools import product
from typing import List, Tuple

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import PartitionMergeOperation
from abstraction_and_reasoning_challenge.src.domain.operation.partitioned_array_selection import PartitionedArraySelection
from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color, Direction, Corner, SpiralDirection, Axis, BackGroundColorSelectionMode, SingleColorSelectionMode


@dataclass(frozen=True)
class AnySelectionMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    fill_color: Color

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        result_mask = np.full_like(partitioned_arrays[0][0], fill_value=False, dtype=bool)
        for horizontal_arrays in partitioned_arrays:
            for a in horizontal_arrays:
                result_mask[a != bg] = True

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)
        result_arr[result_mask] = self.fill_color

        return result_arr


@dataclass(frozen=True)
class NotSelectionMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    fill_color: Color

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        result_mask = np.full_like(partitioned_arrays[0][0], fill_value=True, dtype=bool)
        for horizontal_arrays in partitioned_arrays:
            for a in horizontal_arrays:
                result_mask[a != bg] = False

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)
        result_arr[result_mask] = self.fill_color

        return result_arr


@dataclass(frozen=True)
class AllSelectionMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    fill_color: Color

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        result_mask = np.full_like(partitioned_arrays[0][0], fill_value=True, dtype=bool)
        for horizontal_arrays in partitioned_arrays:
            for a in horizontal_arrays:
                result_mask[a == bg] = False

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)
        result_arr[result_mask] = self.fill_color

        return result_arr


@dataclass(frozen=True)
class ModifiedXorSelectionMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    fill_color: Color

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        any_result_mask = np.full_like(partitioned_arrays[0][0], fill_value=False, dtype=bool)
        for horizontal_arrays in partitioned_arrays:
            for a in horizontal_arrays:
                any_result_mask[a != bg] = True

        all_result_mask = np.full_like(partitioned_arrays[0][0], fill_value=True, dtype=bool)
        for horizontal_arrays in partitioned_arrays:
            for a in horizontal_arrays:
                all_result_mask[a == bg] = False

        # modified xor
        result_mask = any_result_mask
        result_mask[all_result_mask] = False

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)
        result_arr[result_mask] = self.fill_color

        return result_arr


@dataclass(frozen=True)
class NaturalArrayOrderedOverrideMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    start_corner: Corner
    first_axis: Axis

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)

        h, w = len(partitioned_arrays), len(partitioned_arrays[0])
        for i, j in self.natural_array(h, w, self.start_corner, self.first_axis):
            array = partitioned_arrays[i][j]
            result_arr[array != bg] = array[array != bg]

        return result_arr

    def natural_array(self, h: int, w: int, start_corner: Corner, first_axis: Axis) -> List[Tuple[int, int]]:
        start_ind = get_index(start_corner, h, w)

        vertical_start_ind, horizontal_start_ind = start_ind
        vertical_end_ind = h - 1 if vertical_start_ind == 0 else 0
        horizontal_end_ind = w - 1 if horizontal_start_ind == 0 else 0
        vertical_step = +1 if vertical_start_ind == 0 else -1
        horizontal_step = +1 if horizontal_start_ind == 0 else -1

        index_orders = []
        if first_axis == Axis.HORIZONTAL:
            for i in range_closed(vertical_start_ind, vertical_end_ind, vertical_step):
                for j in range_closed(horizontal_start_ind, horizontal_end_ind, horizontal_step):
                    index_orders.append((i, j))
        elif first_axis == Axis.VERTICAL:
            for j in range_closed(horizontal_start_ind, horizontal_end_ind, horizontal_step):
                for i in range_closed(vertical_start_ind, vertical_end_ind, vertical_step):
                    index_orders.append((i, j))
        else:
            raise NotImplementedError()

        assert len(set(index_orders)) == len(index_orders) == h * w, index_orders
        return index_orders


@dataclass(frozen=True)
class DiagonalArrayOrderedOverrideMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    start_corner: Corner
    first_axis: Axis

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)

        h, w = len(partitioned_arrays), len(partitioned_arrays[0])
        for i, j in self.diagonal_array(h, w, self.start_corner, self.first_axis):
            array = partitioned_arrays[i][j]
            result_arr[array != bg] = array[array != bg]

        return result_arr

    def diagonal_array(self, h: int, w: int, start_corner: Corner, first_axis: Axis) -> List[Tuple[int, int]]:
        start_ind = get_index(start_corner, h, w)

        vertical_start_ind, horizontal_start_ind = start_ind
        vertical_end_ind = h - 1 if vertical_start_ind == 0 else 0
        horizontal_end_ind = w - 1 if horizontal_start_ind == 0 else 0
        vertical_step = +1 if vertical_start_ind == 0 else -1
        horizontal_step = +1 if horizontal_start_ind == 0 else -1

        index_orders = []
        if first_axis == Axis.HORIZONTAL:
            for i in range_closed(vertical_start_ind, vertical_end_ind, vertical_step):
                for h_num, j in enumerate(range_closed(horizontal_start_ind, horizontal_end_ind, horizontal_step)):
                    index_orders.append(((i + h_num) % h, j))
        elif first_axis == Axis.VERTICAL:
            for j in range_closed(horizontal_start_ind, horizontal_end_ind, horizontal_step):
                for v_num, i in enumerate(range_closed(vertical_start_ind, vertical_end_ind, vertical_step)):
                    index_orders.append((i, (j + v_num) % w))
        else:
            raise NotImplementedError()
        assert len(set(index_orders)) == len(index_orders) == h * w, index_orders
        return index_orders


@dataclass(frozen=True)
class SpiralArrayOrderedOverrideMerge(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    start_corner: Corner
    spiral_direction: SpiralDirection

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        shape = partitioned_arrays[0][0].shape
        if not all(a.shape == shape for h_a in partitioned_arrays for a in h_a):
            raise OperationInconsistencyException('not same shape')

        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        result_arr = np.full_like(partitioned_arrays[0][0], fill_value=bg)

        h, w = len(partitioned_arrays), len(partitioned_arrays[0])
        for i, j in self.spiral(h, w, self.start_corner, self.spiral_direction):
            array = partitioned_arrays[i][j]
            result_arr[array != bg] = array[array != bg]

        return result_arr

    def spiral(self, h: int, w: int, start_corner: Corner, spiral_direction: SpiralDirection) -> List[Tuple[int, int]]:
        start_ind = get_index(start_corner, h, w)

        index_orders = [start_ind]

        current_ind = start_ind
        while True:
            if (start_corner in [Corner.TOP_LEFT, Corner.TOP_RIGHT, Corner.BOTTOM_RIGHT] and spiral_direction == SpiralDirection.CLOCKWISE) \
                    or (start_corner == Corner.BOTTOM_LEFT and spiral_direction == SpiralDirection.ANTICLOCKWISE):
                if valid_index((current_ind[0], current_ind[1] + 1), h, w, index_orders):
                    direction = Direction.RIGHT
                elif valid_index((current_ind[0] + 1, current_ind[1]), h, w, index_orders):
                    direction = Direction.BOTTOM
                elif valid_index((current_ind[0], current_ind[1] - 1), h, w, index_orders):
                    direction = Direction.LEFT
                elif valid_index((current_ind[0] - 1, current_ind[1]), h, w, index_orders):
                    direction = Direction.TOP
                else:
                    break
            else:
                if valid_index((current_ind[0] - 1, current_ind[1]), h, w, index_orders):
                    direction = Direction.TOP
                elif valid_index((current_ind[0], current_ind[1] - 1), h, w, index_orders):
                    direction = Direction.LEFT
                elif valid_index((current_ind[0] + 1, current_ind[1]), h, w, index_orders):
                    direction = Direction.BOTTOM
                elif valid_index((current_ind[0], current_ind[1] + 1), h, w, index_orders):
                    direction = Direction.RIGHT
                else:
                    break

            while True:
                if direction == Direction.RIGHT:
                    next_ind = (current_ind[0], current_ind[1] + 1)
                elif direction == Direction.BOTTOM:
                    next_ind = (current_ind[0] + 1, current_ind[1])
                elif direction == Direction.LEFT:
                    next_ind = (current_ind[0], current_ind[1] - 1)
                elif direction == Direction.TOP:
                    next_ind = (current_ind[0] - 1, current_ind[1])
                else:
                    raise NotImplementedError()

                if valid_index(next_ind, h, w, index_orders):
                    index_orders.append(next_ind)
                    current_ind = next_ind
                else:
                    break

        assert len(set(index_orders)) == len(index_orders) == h * w, index_orders
        return index_orders


@dataclass(frozen=True)
class UniquelySelectedArrayExtraction(PartitionMergeOperation):
    array_selection: PartitionedArraySelection

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        selections = self.array_selection(arr, partitioned_arrays)

        results = []
        for h_arrays, h_flags in zip(partitioned_arrays, selections):
            for array, flag in zip(h_arrays, h_flags):
                if flag:
                    results.append(array)

        if len(set(map(lambda a: a.tostring(), results))) == 1:
            return results[0]
        else:
            raise OperationInconsistencyException('not unique')


@dataclass(frozen=True)
class RestoreOnlySelectedArray(PartitionMergeOperation):
    bg_selection_mode: BackGroundColorSelectionMode
    array_selection: PartitionedArraySelection

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        selections = self.array_selection(arr, partitioned_arrays)

        for h_arrays, h_flags, h_locations in zip(partitioned_arrays, selections, original_location_masks):
            for array, flag, location in zip(h_arrays, h_flags, h_locations):
                if flag:
                    arr[location] = array.ravel()
                else:
                    arr[location] = bg

        return arr


@dataclass(frozen=True)
class ExtractOneValueFromPartitionedArray(PartitionMergeOperation):
    selection_mode: SingleColorSelectionMode

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        h, w = len(partitioned_arrays), len(partitioned_arrays[0])
        result_arr = np.zeros_like(partitioned_arrays[0][0], shape=(h, w))

        for i, j in product(range(h), range(w)):
            array = partitioned_arrays[i][j]
            extracted_value = ColorSelectionUtil().select_single_color(array, self.selection_mode)
            result_arr[i][j] = extracted_value

        return result_arr


@dataclass(frozen=True)
class ExtractOneValueFromSelectedPartitionedArray(PartitionMergeOperation):
    selection_mode: SingleColorSelectionMode
    array_selection: PartitionedArraySelection

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        h, w = len(partitioned_arrays), len(partitioned_arrays[0])
        result_arr = np.zeros_like(partitioned_arrays[0][0], shape=(h, w))

        selections = self.array_selection(arr, partitioned_arrays)

        for i, (h_arrays, h_flags) in enumerate(zip(partitioned_arrays, selections)):
            for j, (array, flag) in enumerate(zip(h_arrays, h_flags)):
                if flag:
                    extracted_value = ColorSelectionUtil().select_single_color(array, self.selection_mode)
                    result_arr[i][j] = extracted_value

        return result_arr


def range_closed(start, stop, step):
    direction = 1 if (step > 0) else -1
    return range(start, stop + direction, step)


def get_index(corner: Corner, h: int, w: int) -> Tuple[int, int]:
    if corner == Corner.TOP_LEFT:
        return 0, 0
    elif corner == Corner.TOP_RIGHT:
        return 0, w - 1
    elif corner == Corner.BOTTOM_RIGHT:
        return h - 1, w - 1
    elif corner == Corner.BOTTOM_LEFT:
        return h - 1, 0
    else:
        raise NotImplementedError()


def valid_index(ind2d: Tuple[int, int], h: int, w: int, black_list: List[Tuple[int, int]]) -> bool:
    if ind2d in black_list:
        return False
    if ind2d[0] < 0 or h <= ind2d[0]:
        return False
    if ind2d[1] < 0 or w <= ind2d[1]:
        return False
    return True
