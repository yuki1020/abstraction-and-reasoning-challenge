import numpy as np
from dataclasses import dataclass

from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import BackGroundColorSelectionMode


class ImageToIntFunc:
    def __call__(self, arr: np.ndarray) -> int:
        raise NotImplementedError()


@dataclass(frozen=True)
class CellNum(ImageToIntFunc):
    bg_selection_mode: BackGroundColorSelectionMode

    def __call__(self, arr: np.ndarray) -> int:
        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)
        return (arr != bg).sum()


class UniqueColorNum(ImageToIntFunc):
    def __call__(self, arr: np.ndarray) -> int:
        return len(np.unique(arr))


class UniqueColorNumMinusOne(ImageToIntFunc):
    def __call__(self, arr: np.ndarray) -> int:
        return len(np.unique(arr)) - 1  # background color


