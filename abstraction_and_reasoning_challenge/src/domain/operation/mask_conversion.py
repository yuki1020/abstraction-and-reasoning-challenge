from operator import itemgetter

import cv2
import numpy as np
from dataclasses import dataclass
from itertools import groupby
from scipy.ndimage import binary_fill_holes, binary_erosion, binary_dilation
from skimage.measure import label

from abstraction_and_reasoning_challenge.src.domain.operation.base import MaskConversion
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Axis, LineEdgeType, FillType, PixelConnectivity, MaxOrMin, ObjectFeature, ImageEdgeType, HoleInclude, TrueOrFalse


@dataclass(frozen=True)
class NoMaskConversion(MaskConversion):
    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        return color_mask


@dataclass(frozen=True)
class SplitLineSelection(MaskConversion):
    axis: Axis

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        result_mask = np.full_like(color_mask, fill_value=False)
        if self.axis in [Axis.VERTICAL, Axis.BOTH]:
            vertical_line_hits = color_mask.all(axis=0)
            result_mask[:, vertical_line_hits] = True

        if self.axis in [Axis.HORIZONTAL, Axis.BOTH]:
            horizontal_line_hits = color_mask.all(axis=1)
            result_mask[horizontal_line_hits] = True

        return result_mask


@dataclass(frozen=True)
class DotExistLineSelection(MaskConversion):
    axis: Axis

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        result_mask = np.full_like(color_mask, fill_value=False)
        if self.axis in [Axis.VERTICAL, Axis.BOTH]:
            vertical_line_hits = color_mask.any(axis=0)
            result_mask[:, vertical_line_hits] = True

        if self.axis in [Axis.HORIZONTAL, Axis.BOTH]:
            horizontal_line_hits = color_mask.any(axis=1)
            result_mask[horizontal_line_hits] = True

        return result_mask


@dataclass(frozen=True)
class ObjectsTouchingEdgeSelection(MaskConversion):
    # TODO Direction or Axis property?
    true_or_false: TrueOrFalse
    connectivity: PixelConnectivity

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        label_array, max_label_index = label(color_mask, connectivity=self.connectivity.value_for_skimage,
                                             background=False, return_num=True)

        if max_label_index == 0:
            return np.full_like(color_mask, order='C', fill_value=False)

        target_indices = [i for i in range(1, max_label_index + 1) if self._is_target(label_array == i)]

        return np.isin(label_array, target_indices)

    def _is_target(self, arr: np.ndarray) -> bool:
        top_line = arr[0]
        bottom_line = arr[-1]
        left_line = arr[:, 0]
        right_line = arr[:, -1]

        if self.true_or_false == TrueOrFalse.TRUE:
            return any([top_line.any(), bottom_line.any(), left_line.any(), right_line.any()])
        else:
            return not any([top_line.any(), bottom_line.any(), left_line.any(), right_line.any()])


@dataclass(frozen=True)
class ObjectsMaxMinSelection(MaskConversion):
    """ Create a mask with max/min feature objects """
    true_or_false: TrueOrFalse
    max_or_min: MaxOrMin
    object_feature: ObjectFeature
    connectivity: PixelConnectivity

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        label_array, max_label_index = label(color_mask, connectivity=self.connectivity.value_for_skimage,
                                             background=False, return_num=True)

        if max_label_index == 0:
            return np.full_like(color_mask, order='C', fill_value=False)

        label_indices = list(range(1, max_label_index + 1))
        label_index_feature_value_pairs = list(map(lambda l: (l, self._calculate_object_feature(label_array, l)), label_indices))

        target_feature_value = self.max_or_min.func(label_index_feature_value_pairs, key=itemgetter(1))[1]
        if self.true_or_false == TrueOrFalse.TRUE:
            target_indices = [l_i for l_i, f in label_index_feature_value_pairs if f == target_feature_value]
        else:
            target_indices = [l_i for l_i, f in label_index_feature_value_pairs if f != target_feature_value]

        return np.isin(label_array, target_indices)

    def _calculate_object_feature(self, label_array: np.ndarray, label_index: int) -> int:
        if self.object_feature == ObjectFeature.AREA:
            return self._label_array_to_area(label_array, label_index)
        if self.object_feature == ObjectFeature.HORIZONTAL_LEN:
            return self._label_array_to_horizontal_len(label_array, label_index)
        if self.object_feature == ObjectFeature.VERTICAL_LEN:
            return self._label_array_to_vertical_len(label_array, label_index)
        else:
            raise NotImplementedError()

    def _label_array_to_area(self, label_array: np.ndarray, label_index: int) -> int:
        label_hit = label_array == label_index
        return label_hit.sum()

    def _label_array_to_horizontal_len(self, label_array: np.ndarray, label_index: int) -> int:
        label_hit = label_array == label_index
        horizontal_label_hit = label_hit.any(axis=0)
        coords = np.where(horizontal_label_hit)[0]
        return max(coords) - min(coords)

    def _label_array_to_vertical_len(self, label_array: np.ndarray, label_index: int) -> int:
        label_hit = label_array == label_index
        vertical_label_hit = label_hit.any(axis=1)
        coords = np.where(vertical_label_hit)[0]
        return max(coords) - min(coords)


@dataclass(frozen=True)
class OldObjectsMaxMinSelection(MaskConversion):
    # similar to ObjectsMaxMinSelection

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        # TODO should variate hierarchy?
        contours, hierarchy = cv2.findContours(np.ascontiguousarray(color_mask).astype(np.uint8),
                                               cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        if len(contours) == 0:
            return np.full_like(color_mask, order='C', fill_value=False)

        max_area_contour = max(contours, key=lambda c: cv2.contourArea(c))

        mask = np.full_like(color_mask, order='C', fill_value=False)

        mask = cv2.drawContours(mask.astype(np.uint8), max_area_contour, contourIdx=-1, color=1)
        if isinstance(mask, cv2.UMat):  # mask sometimes becomes cv2.UMat class... I don't know why.
            mask = mask.get()

        return mask.astype(bool)


@dataclass(frozen=True)
class SquareObjectsSelection(MaskConversion):
    """ Create a mask with only square objects """

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        color_mask = color_mask.astype(np.uint8)
        max_square_len = min(color_mask.shape)
        # TODO
        # if max_square_len == 1:
        #     return arr

        square_hit = np.full_like(color_mask, fill_value=False, dtype=bool)
        for l in range(1, max_square_len):
            hit_and_miss_kernel = self._square_hit_and_miss_kenel(l)
            filter_kernel = self._filter_kenel(l)
            temp_square_hit = cv2.morphologyEx(color_mask, cv2.MORPH_HITMISS, hit_and_miss_kernel, anchor=(1, 1))
            temp_square_hit = cv2.filter2D(temp_square_hit, -1, filter_kernel, anchor=(l - 1, l - 1), borderType=cv2.BORDER_CONSTANT)

            square_hit = np.logical_or(square_hit, temp_square_hit.astype(bool))

        return square_hit

    def _square_hit_and_miss_kenel(self, l: int) -> np.ndarray:
        kernel = np.full((l + 2, l + 2), fill_value=1, dtype=np.int8)
        kernel[0, :] = -1
        kernel[-1, :] = -1
        kernel[:, 0] = -1
        kernel[:, -1] = -1
        return kernel

    def _filter_kenel(self, l: int) -> np.ndarray:
        return np.full((l, l), fill_value=1, dtype=np.int8)


@dataclass(frozen=True)
class HolesSelection(MaskConversion):
    """ Select only the empty hole inside. """

    connectivity: PixelConnectivity

    # TODO Lack of consideration of the edges of the image?

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        filled = binary_fill_holes(color_mask, structure=self.connectivity.structure_for_skimage)
        return filled ^ color_mask


@dataclass(frozen=True)
class ObjectInnerSelection(MaskConversion):
    connectivity: PixelConnectivity
    image_edge_type: ImageEdgeType

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        if self.image_edge_type == ImageEdgeType.EDGE_EXCLUDE:
            border_value = 0
        elif self.image_edge_type == ImageEdgeType.EDGE_INCLUDE:
            border_value = 1
        else:
            raise NotImplementedError()

        return binary_erosion(color_mask, structure=self.connectivity.structure_for_skimage, border_value=border_value)


@dataclass(frozen=True)
class ContourSelection(MaskConversion):
    """ Create a contour mask """

    connectivity: PixelConnectivity
    image_edge_type: ImageEdgeType

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        inner_mask = ObjectInnerSelection(self.connectivity, self.image_edge_type)(color_mask)
        return np.logical_xor(inner_mask, color_mask)


@dataclass(frozen=True)
class ContourOuterSelection(MaskConversion):
    """ Create a mask one pixel outside the contour """

    connectivity: PixelConnectivity
    hole_include: HoleInclude

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        if self.hole_include == HoleInclude.INCLUDE:
            dilated = binary_dilation(color_mask, structure=self.connectivity.structure_for_skimage, border_value=0)
            return np.logical_xor(dilated, color_mask)
        elif self.hole_include == HoleInclude.EXCLUDE:
            dilated = binary_dilation(color_mask, structure=self.connectivity.structure_for_skimage, border_value=0)
            holes = HolesSelection(self.connectivity)(color_mask)
            return np.logical_and(np.logical_xor(dilated, color_mask), ~holes)
        else:
            raise NotImplementedError()


@dataclass(frozen=True)
class ConnectDotSelection(MaskConversion):
    # TODO This function spends much time.
    axis: Axis
    edge_type: LineEdgeType
    fill_type: FillType

    def __call__(self, color_mask: np.ndarray) -> np.ndarray:
        result_mask = np.full_like(color_mask, fill_value=False)
        coords = np.argwhere(color_mask)
        if self.axis in [Axis.HORIZONTAL, Axis.BOTH]:

            # Calculate the min and max coordinates of the horizontal
            horizontal_group = {k: itemgetter(0, -1)(tuple(map(itemgetter(1), g))) for k, g in groupby(coords, key=itemgetter(0))}

            # filter
            horizontal_group = {k: (v[0], v[1]) for k, v in horizontal_group.items() if (v[1] - v[0]) >= 2}

            # maskを計算
            for y, (x_min, x_max) in horizontal_group.items():
                if self.edge_type == LineEdgeType.EdgeInclude:
                    pass
                elif self.edge_type == LineEdgeType.EdgeExclude:
                    x_min += 1
                    x_max -= 1
                else:
                    raise NotImplementedError()

                result_mask[y, x_min:x_max + 1] = True

        if self.axis in [Axis.VERTICAL, Axis.BOTH]:

            # Calculate the min and max coordinates of the vertical
            vertical_group = {k: itemgetter(0, -1)(tuple(map(itemgetter(0), g))) for k, g in groupby(sorted(coords, key=itemgetter(1)), key=itemgetter(1))}

            # calculate mask
            vertical_group = {k: (v[0], v[1]) for k, v in vertical_group.items() if (v[1] - v[0]) >= 2}

            # generate mask
            for x, (y_min, y_max) in vertical_group.items():
                if self.edge_type == LineEdgeType.EdgeInclude:
                    pass
                elif self.edge_type == LineEdgeType.EdgeExclude:
                    y_min += 1
                    y_max -= 1
                else:
                    raise NotImplementedError()

                result_mask[y_min:y_max + 1, x] = True

        if self.fill_type == FillType.NotOverride:
            result_mask = np.logical_xor(result_mask, color_mask)
        return result_mask
