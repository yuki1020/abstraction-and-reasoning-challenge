import numpy as np
from dataclasses import dataclass
from itertools import chain
from typing import List, Union, Tuple

from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color


@dataclass(frozen=True)
class UniformOperation:
    def __call__(self, arr: np.ndarray) -> np.ndarray:
        raise NotImplementedError()


@dataclass(frozen=True)
class ColorSelection:
    def __call__(self, arr: np.ndarray) -> np.ndarray:
        raise NotImplementedError()


@dataclass(frozen=True)
class MaskConversion:
    def __call__(self, mask: np.ndarray) -> np.ndarray:
        raise NotImplementedError()


@dataclass(frozen=True)
class MaskOperation:
    def __call__(self, arr: np.ndarray, mask: np.ndarray) -> np.ndarray:
        raise NotImplementedError()


@dataclass(frozen=True)
class ColorChannelSelection:
    def __call__(self, arr: np.ndarray) -> List[Tuple[Color, np.ndarray]]:
        raise NotImplementedError()


@dataclass(frozen=True)
class ChannelMergeOperation:
    def __call__(self, arr: np.ndarray, original_color_mask_pairs: List[Tuple[Color, np.ndarray]], color_mask_pairs: List[Tuple[Color, np.ndarray]]) -> np.ndarray:
        raise NotImplementedError()


@dataclass(frozen=True)
class PartitionSelection:
    # array -> (2d_partitioned_array, 2d_original_location_mask)
    def __call__(self, arr: np.ndarray) -> Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
        raise NotImplementedError()


@dataclass(frozen=True)
class PartitionMergeOperation:
    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]], original_location_masks: List[List[np.ndarray]]) -> np.ndarray:
        raise NotImplementedError()


@dataclass(frozen=True)
class ColorOperation:
    color_selection: ColorSelection
    mask_conversions: MaskConversion
    mask_operation: MaskOperation


@dataclass(frozen=True)
class MultiColorChannelOperation:
    channel_selection: ColorChannelSelection
    mask_conversions: MaskConversion
    channel_merge_operation: ChannelMergeOperation


@dataclass(frozen=True)
class PartitionOperation:
    partition_selection: 'PartitionSelection'
    partition_merge_operation: 'PartitionMergeOperation'


@dataclass(frozen=True)
class OperationSet:
    operations: List[Union[UniformOperation, ColorOperation, MultiColorChannelOperation, PartitionOperation]]

    def __str__(self):
        return repr(self)

    def types(self):
        results = []
        for o in self.operations:
            if isinstance(o, UniformOperation):
                results.append(UniformOperation)
            elif isinstance(o, ColorOperation):
                results.append(ColorOperation)
            elif isinstance(o, MultiColorChannelOperation):
                results.append(MultiColorChannelOperation)
            elif isinstance(o, PartitionOperation):
                results.append(PartitionOperation)
            else:
                raise NotImplementedError()
        return results

    def elements(self) -> List[Union[UniformOperation, ColorSelection, MaskConversion, MaskOperation, PartitionOperation]]:
        res = []
        for o in self.operations:
            if isinstance(o, UniformOperation):
                res.append(o)
            elif isinstance(o, ColorOperation):
                res.append(o.color_selection)
                res.append(o.mask_conversions)
                res.append(o.mask_operation)
            elif isinstance(o, MultiColorChannelOperation):
                res.append(o.channel_selection)
                res.append(o.mask_conversions)
                res.append(o.channel_merge_operation)
            elif isinstance(o, PartitionOperation):
                res.append(o.partition_selection)
                res.append(o.partition_merge_operation)
            else:
                raise NotImplementedError()
        return res


def get_all_operation_classes():
    return [UniformOperation, ColorOperation, MultiColorChannelOperation, PartitionOperation]


def get_all_operation_element_classes():
    classes = [UniformOperation, ColorSelection, MaskConversion, MaskOperation, ColorChannelSelection, ChannelMergeOperation, PartitionSelection, PartitionMergeOperation]
    return chain.from_iterable([c.__subclasses__() for c in classes])
