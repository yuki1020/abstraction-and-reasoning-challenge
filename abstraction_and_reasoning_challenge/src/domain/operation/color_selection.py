import numpy as np
from dataclasses import dataclass

from abstraction_and_reasoning_challenge.src.domain.operation.base import ColorSelection
from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color, SingleColorSelectionMode, MultiColorSelectionMode


@dataclass(frozen=True)
class FixedSingleColorSelection(ColorSelection):
    color: Color

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        return arr == self.color


@dataclass(frozen=True)
class SingleColorSelection(ColorSelection):
    single_color_selection_mode: SingleColorSelectionMode

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        color = ColorSelectionUtil().select_single_color(arr, self.single_color_selection_mode)
        return arr == color


@dataclass(frozen=True)
class MultiColorSelection(ColorSelection):
    multi_color_selection_mode: MultiColorSelectionMode

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if self.multi_color_selection_mode == MultiColorSelectionMode.ANY_WITHOUT_MOST_COMMON:
            most_common_color = ColorSelectionUtil().select_single_color(arr, SingleColorSelectionMode.MOST_COMMON)
            return arr != most_common_color
        elif self.multi_color_selection_mode == MultiColorSelectionMode.ANY_WITHOUT_LEAST_COMMON:
            least_common_color = ColorSelectionUtil().select_single_color(arr, SingleColorSelectionMode.LEAST_COMMON)
            return arr != least_common_color
        else:
            raise NotImplementedError()
