import numpy as np
from dataclasses import dataclass
from typing import List, Tuple

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import ColorChannelSelection
from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color, BackGroundColorSelectionMode


@dataclass(frozen=True)
class WithOutMostCommonColorChannelSelection(ColorChannelSelection):
    bg_selection_mode: BackGroundColorSelectionMode

    def __call__(self, arr: np.ndarray) -> List[Tuple[Color, np.ndarray]]:
        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        colors = ColorSelectionUtil().get_colors(arr)

        results = [(c, arr == c) for c in colors if c != bg]

        if len(results) <= 1:
            raise OperationInconsistencyException('can not devide')

        return results
