from collections import Counter

import numpy as np
from dataclasses import dataclass
from functools import partial
from itertools import chain
from typing import List, Any

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.uniform_operation import Flip
from abstraction_and_reasoning_challenge.src.domain.operation.util.color_util import ColorSelectionUtil
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import MaxOrMin, BackGroundColorSelectionMode, Color, AxisV2, FlipMode, TrueOrFalse


@dataclass(frozen=True)
class PartitionedArraySelection:
    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]]) -> List[List[bool]]:
        raise NotImplementedError()


@dataclass(frozen=True)
class UniqueColorNumberSelection(PartitionedArraySelection):
    max_or_min: MaxOrMin

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]]) -> List[List[bool]]:
        color_nums = _apply(self._color_num, partitioned_arrays)

        if self.max_or_min == MaxOrMin.MAX:
            target_color_num = max(map(max, color_nums))
        elif self.max_or_min == MaxOrMin.MIN:
            target_color_num = min(map(min, color_nums))
        else:
            raise NotImplementedError()

        return _apply(lambda n: n == target_color_num, color_nums)

    def _color_num(self, array: np.ndarray):
        return len(np.unique(array))


@dataclass(frozen=True)
class ColoredCellNumberSelection(PartitionedArraySelection):
    max_or_min: MaxOrMin
    bg_selection_mode: BackGroundColorSelectionMode

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]]) -> List[List[bool]]:
        bg = ColorSelectionUtil().get_background_color(arr, self.bg_selection_mode)

        colored_cell_nums = _apply(partial(self._colored_cell_nums, bg=bg), partitioned_arrays)

        if self.max_or_min == MaxOrMin.MAX:
            target_color_num = max(map(max, colored_cell_nums))
        elif self.max_or_min == MaxOrMin.MIN:
            target_color_num = min(map(min, colored_cell_nums))
        else:
            raise NotImplementedError()

        return _apply(lambda n: n == target_color_num, colored_cell_nums)

    def _colored_cell_nums(self, array: np.ndarray, bg: Color):
        return (array != bg).sum()


@dataclass(frozen=True)
class SameShapeNumSelection(PartitionedArraySelection):
    max_or_min: MaxOrMin

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]]) -> List[List[bool]]:
        np_strings = _apply(lambda n: n.tostring(), partitioned_arrays)

        c = Counter(_flatten(np_strings))

        most_commons = c.most_common()
        if len(most_commons) < 2:
            raise OperationInconsistencyException('can not select')

        if self.max_or_min == MaxOrMin.MAX:
            if most_commons[0][1] == most_commons[1][1]:
                raise OperationInconsistencyException('duplicated max')
            target = most_commons[0][0]
        elif self.max_or_min == MaxOrMin.MIN:
            if most_commons[-1][1] == most_commons[-2][1]:
                raise OperationInconsistencyException('duplicated min')
            target = most_commons[-1][0]
        else:
            raise NotImplementedError()

        return _apply(lambda n: n == target, np_strings)


@dataclass(frozen=True)
class SymmetrySelection(PartitionedArraySelection):
    axis: AxisV2
    true_or_false: TrueOrFalse

    def __call__(self, arr: np.ndarray, partitioned_arrays: List[List[np.ndarray]]) -> List[List[bool]]:
        return _apply(partial(self._is_symmetry, axis=self.axis, true_or_false=self.true_or_false), partitioned_arrays)

    def _is_symmetry(self, array: np.ndarray, axis: AxisV2, true_or_false: TrueOrFalse) -> bool:
        if axis == AxisV2.VERTICAL:
            res = np.array_equal(array, Flip(FlipMode.UD)(array))
        elif axis == AxisV2.HORIZONTAL:
            res = np.array_equal(array, Flip(FlipMode.LR)(array))
        elif axis == AxisV2.VERTICAL_HORIZONTAL:
            res = self._is_symmetry(array, AxisV2.VERTICAL, TrueOrFalse.TRUE) and self._is_symmetry(array, AxisV2.HORIZONTAL, TrueOrFalse.TRUE)
        elif axis == AxisV2.MAIN_DIAGONAL:
            res = np.array_equal(array, Flip(FlipMode.UL_DR)(array))
        elif axis == AxisV2.ANTI_DIAGONAL:
            res = np.array_equal(array, Flip(FlipMode.UR_DL)(array))
        elif axis == AxisV2.BOTH_DIAGONAL:
            res = self._is_symmetry(array, AxisV2.MAIN_DIAGONAL, TrueOrFalse.TRUE) and self._is_symmetry(array, AxisV2.ANTI_DIAGONAL, TrueOrFalse.TRUE)
        else:
            raise NotImplementedError()

        if true_or_false == TrueOrFalse.TRUE:
            return res
        else:
            return not res


def _apply(func, partitioned_arrays: List[List[np.ndarray]]) -> List[List[Any]]:
    results = []
    for h_arrays in partitioned_arrays:
        temp_masks = []
        for array in h_arrays:
            temp_masks.append(func(array))
        results.append(temp_masks)
    return results


def _flatten(partitioned: List[List[Any]]) -> List[Any]:
    return list(chain.from_iterable(partitioned))
