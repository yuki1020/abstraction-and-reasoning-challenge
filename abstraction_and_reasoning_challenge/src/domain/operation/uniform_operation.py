import numpy as np
from dataclasses import dataclass
from matplotlib import pyplot as plt
from scipy.ndimage import maximum_filter
from skimage.filters import threshold_minimum, try_all_threshold

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import UniformOperation
from abstraction_and_reasoning_challenge.src.domain.operation.image_to_int import ImageToIntFunc
from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import FlipMode, Color, PaddingMode, Direction, Axis


# Operations resulting in a uniform change throughout the image (independent on pixels).

@dataclass(frozen=True)
class Padding(UniformOperation):
    padding_mode: PaddingMode
    direction: Direction
    k: int

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if self.padding_mode == PaddingMode.REPEAT:
            np_pad_mode = 'wrap'
        elif self.padding_mode == PaddingMode.MIRROR_1:
            np_pad_mode = 'symmetric'
        elif self.padding_mode == PaddingMode.MIRROR_2:
            np_pad_mode = 'reflect'
        elif self.padding_mode == PaddingMode.EDGE:
            np_pad_mode = 'edge'
        else:
            raise ValueError(self.padding_mode)

        h, w = arr.shape
        if self.padding_mode == PaddingMode.MIRROR_2:
            h, w = h - 1, w - 1

        if self.direction == Direction.TOP:
            pad_width = ((self.k * h, 0), (0, 0))
        elif self.direction == Direction.BOTTOM:
            pad_width = ((0, self.k * h), (0, 0))
        elif self.direction == Direction.LEFT:
            pad_width = ((0, 0), (self.k * w, 0))
        elif self.direction == Direction.RIGHT:
            pad_width = ((0, 0), (0, self.k * w))
        else:
            raise ValueError(self.direction)

        return np.pad(arr, pad_width, mode=np_pad_mode)


@dataclass(frozen=True)
class DynamicPadding(UniformOperation):
    padding_mode: PaddingMode
    direction: Direction
    image_to_int_func: ImageToIntFunc

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        num = self.image_to_int_func(arr)
        if num == 0:
            raise OperationInconsistencyException('0_padding')

        return Padding(self.padding_mode, self.direction, num)(arr)


@dataclass(frozen=True)
class Resize(UniformOperation):
    axis: Axis
    ratio: int

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if self.axis == Axis.VERTICAL:
            return np.repeat(arr, self.ratio, axis=0)
        elif self.axis == Axis.HORIZONTAL:
            return np.repeat(arr, self.ratio, axis=1)
        elif self.axis == Axis.BOTH:
            temp = np.repeat(arr, self.ratio, axis=0)
            return np.repeat(temp, self.ratio, axis=1)
        else:
            raise ValueError(self.axis)


@dataclass(frozen=True)
class DynamicResize(UniformOperation):
    axis: Axis
    image_to_int_func: ImageToIntFunc

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        num = self.image_to_int_func(arr)
        if num == 0:
            raise OperationInconsistencyException('0_resize')

        return Resize(self.axis, num)(arr)


@dataclass(frozen=True)
class SmallResize(UniformOperation):
    axis: Axis
    ratio: int

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if self.axis == Axis.VERTICAL:
            if arr.shape[0] % self.ratio != 0:
                raise OperationInconsistencyException('can not divide')
            return arr[::self.ratio, :]
        elif self.axis == Axis.HORIZONTAL:
            if arr.shape[1] % self.ratio != 0:
                raise OperationInconsistencyException('can not divide')
            return arr[:, ::self.ratio]
        else:
            raise ValueError(self.axis)


@dataclass(frozen=True)
class DynamicSmallResize(UniformOperation):
    axis: Axis
    image_to_int_func: ImageToIntFunc

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        num = self.image_to_int_func(arr)
        if num == 0:
            raise OperationInconsistencyException('0_resize')

        return SmallResize(self.axis, num)(arr)


@dataclass(frozen=True)
class Flip(UniformOperation):
    flip_mode: FlipMode

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if self.flip_mode == FlipMode.UD:
            return np.flipud(arr)
        elif self.flip_mode == FlipMode.LR:
            return np.fliplr(arr)
        elif self.flip_mode == FlipMode.UL_DR:
            return arr.T
        elif self.flip_mode == FlipMode.UR_DL:
            return np.flipud(np.flipud(arr.T))
        else:
            raise ValueError(self.flip_mode)


@dataclass(frozen=True)
class Rotate(UniformOperation):
    angle: int

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if self.angle not in [90, 180, 270]:
            raise ValueError(self.angle)

        return np.rot90(arr, self.angle // 90)


@dataclass(frozen=True)
class LineDeletion(UniformOperation):
    line_color: Color

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        if arr.size == 1:
            raise OperationInconsistencyException('size == 1')

        if 1 in arr.shape:
            raise OperationInconsistencyException('can not separate')

        color_hit: np.ndarray = arr == self.line_color
        line_v_indices = np.where(color_hit.all(axis=1))[0]
        line_h_indices = np.where(color_hit.all(axis=0))[0]

        if len(line_v_indices) == len(line_h_indices) == 0:
            raise OperationInconsistencyException('not line found')

        arr = np.delete(arr, line_h_indices, axis=1)
        arr = np.delete(arr, line_v_indices, axis=0)

        if 0 in arr.shape:
            raise OperationInconsistencyException('0 size')

        return arr


@dataclass(frozen=True)
class FFTCompletion(UniformOperation):

    # TODO GIVE UP implement.
    #  This is just a poc for "SYMMETRY" or "REPEAT" pattern tasks.
    #  If you're interested in this function, let me know. I'll translate it.

    def __call__(self, arr: np.ndarray) -> np.ndarray:
        revs = []
        for color in Color:

            print(color)
            color_hit = arr == color

            if color == Color.BLACK or not color_hit.any():
                revs.append(np.full_like(color_hit, fill_value=False))
                continue

            rev_arr_int = self.complete_symmetric(color_hit)
            rev_arr_int = self.complete_symmetric(rev_arr_int)
            rev_arr_int = self.complete_symmetric(rev_arr_int)
            revs.append(rev_arr_int)

        for color, rev in zip(Color, revs):
            arr[rev] = color
        return arr

    def complete_symmetric(self, hit_arr, verbose=False):
        h, w = hit_arr.shape

        if verbose:
            print(hit_arr)
        f = np.fft.fftshift(np.fft.fft2(hit_arr))
        if verbose:
            print(f)
        amp = np.abs(f)
        if verbose:
            print(amp)
        amp = amp / h / w * 2
        if verbose:
            print(amp)
            print(f'sum {amp.sum()}')
            print(f'mean {amp.mean()}')
            print(f'max {amp.max()}')
        # TODO detect peakのパラメータ調整
        flags = np.array(self.detect_not_peaks_mask(amp))
        if verbose:
            print(flags)
        f[flags] = 0
        filtered_amp = amp.copy()
        filtered_amp[f == 0] = 0
        # F3_abs = np.abs(f)  # 複素数を絶対値に変換
        # F3_abs_amp = F3_abs / h / w * 2  # 交流成分はデータ数で割って2倍
        # F3_abs_amp[0] = F3_abs_amp[0] / 2  # 直流成分（今回は扱わないけど）は2倍不要
        F3_ifft = np.fft.ifft2(np.fft.ifftshift(f))  # IFFT
        F3_ifft_real = F3_ifft.real  # 実数部の取得
        # TODO 2値化アルゴリズム検討
        rev_arr_int = F3_ifft_real > threshold_minimum(F3_ifft_real)
        if verbose:
            fig, ax = try_all_threshold(F3_ifft_real, figsize=(10, 8), verbose=False)
            plt.show()

            # visualize
            plt.subplot(171)
            plt.imshow(hit_arr, cmap='gray')
            plt.title('Input Image'), plt.xticks([]), plt.yticks([])
            plt.subplot(172)
            plt.hist(amp.ravel(), bins=100)
            plt.title('Input Image'), plt.xticks([]), plt.yticks([])
            plt.subplot(173)
            plt.imshow(amp, cmap='gray')
            plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
            plt.subplot(174)
            plt.imshow(filtered_amp, cmap='gray')
            plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
            plt.subplot(175)
            plt.hist(F3_ifft_real.ravel(), bins=100)
            plt.subplot(176)
            plt.imshow(rev_arr_int, cmap='gray')
            plt.title('rev'), plt.xticks([]), plt.yticks([])
            plt.subplot(177)
            plt.imshow(rev_arr_int | hit_arr, cmap='gray')
            plt.title('and'), plt.xticks([]), plt.yticks([])
            plt.show()
        return rev_arr_int

    def detect_not_peaks_mask(self, image, filter_size=3, order=0.05):
        local_max = maximum_filter(image, footprint=np.ones((filter_size, filter_size)), mode='constant')
        detected_peaks = np.ma.array(image, mask=~(image == local_max))

        # 小さいピーク値を排除（最大ピーク値のorder倍のピークは排除）
        temp = np.ma.array(detected_peaks, mask=~(detected_peaks >= detected_peaks.max() * order))
        return temp.mask
