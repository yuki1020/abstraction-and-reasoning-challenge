import numpy as np

from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.task.task_operation_executor import TaskOperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task import Task


class AnswerMatcher:

    @staticmethod
    def is_match_arr(arr1: np.ndarray, arr2: np.ndarray) -> bool:
        return np.array_equal(arr1, arr2)

    @classmethod
    def is_train_all_match_if_operated(cls, task: Task, operation_set: OperationSet) -> bool:
        try:
            applied_task = TaskOperationSetExecutor().execute(task, operation_set)
            return all(cls.is_match_arr(io.input_arr, io.output_arr) for io in applied_task.train)
        except OperationInconsistencyException:
            return False

    @classmethod
    def is_train_test_all_match_if_operated(cls, task: Task, operation_set: OperationSet) -> bool:
        try:
            applied_task = TaskOperationSetExecutor().execute(task, operation_set)
            return all(cls.is_match_arr(io.input_arr, io.output_arr) for io in applied_task.train + applied_task.test)
        except OperationInconsistencyException:
            return False

    # TODO ？
    @classmethod
    def is_train_all_match(cls, task: Task) -> bool:
        return all(map(lambda io: cls.is_match_arr(io.input_arr, io.output_arr), task.train))
