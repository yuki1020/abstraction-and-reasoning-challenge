from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet, MaskOperation, MaskConversion, ColorSelection, ColorChannelSelection, ChannelMergeOperation, PartitionSelection, PartitionMergeOperation
from abstraction_and_reasoning_challenge.src.domain.operation.operation_executor import OperationSetExecutor
from abstraction_and_reasoning_challenge.src.domain.task.task import Task, ColorSelectedTask, MaskConvertedTask, InputOutput, ColorChannelSelectedTask, ColorChannelMaskConvertedTask, PartitionSelectionTask


class TaskOperationSetExecutor:

    def execute(self, task: Task, operation_set: OperationSet) -> Task:
        arrays = OperationSetExecutor.apply_operation_set([io.input_arr for io in task.train + task.test], operation_set)

        return Task(task.name,
                    tuple([InputOutput(a, io.output_arr) for a, io in zip(arrays[:len(task.train)], task.train)]),
                    tuple([InputOutput(a, io.output_arr) for a, io in zip(arrays[len(task.train):], task.test)]))


class ColorSelectionExecutor:

    @staticmethod
    def execute(task: Task, color_selection: ColorSelection) -> ColorSelectedTask:
        masks = OperationSetExecutor.apply_color_selection([io.input_arr for io in task.train + task.test], color_selection)

        return ColorSelectedTask(task.name, task.train, task.test, masks[:len(task.train)], masks[len(task.train):])


class MaskConversionExecutor:

    @staticmethod
    def execute(task: ColorSelectedTask, mask_conversion: MaskConversion) -> MaskConvertedTask:
        masks = OperationSetExecutor.apply_mask_conversion(task.train_masks + task.test_masks, mask_conversion)

        return MaskConvertedTask(task.name, task.train, task.test, masks[:len(task.train_masks)], masks[len(task.train_masks):])


class MaskOperationExecutor:

    @staticmethod
    def execute(task: MaskConvertedTask, mask_operation: MaskOperation) -> Task:
        new_arrays = OperationSetExecutor.apply_mask_operation(
            [io.input_arr for io in task.train + task.test], task.train_masks + task.test_masks, mask_operation)
        train_io = tuple([InputOutput(n, io.output_arr) for n, io, in zip(new_arrays[:len(task.train)], task.train)])
        test_io = tuple([InputOutput(n, io.output_arr) for n, io, in zip(new_arrays[len(task.train):], task.train)])

        return Task(task.name, train_io, test_io)


class ColorChannelSelectionExecutor:

    @staticmethod
    def execute(task: Task, color_channel_selection: ColorChannelSelection) -> ColorChannelSelectedTask:
        color_mask_pairs_list = OperationSetExecutor.apply_channel_selection([io.input_arr for io in task.train + task.test], color_channel_selection)

        return ColorChannelSelectedTask(task.name, task.train, task.test,
                                        color_mask_pairs_list[:len(task.train)],
                                        color_mask_pairs_list[len(task.train):])


class ColorChannelMaskConversionSelectionExecutor:

    @staticmethod
    def execute(task: ColorChannelSelectedTask, mask_conversion: MaskConversion) -> ColorChannelMaskConvertedTask:
        color_mask_pairs_list = OperationSetExecutor.apply_color_channel_mask_conversion(
            task.train_color_mask_pairs + task.test_color_mask_pairs, mask_conversion)

        return ColorChannelMaskConvertedTask(task.name, task.train, task.test,
                                             task.train_color_mask_pairs,
                                             color_mask_pairs_list[:len(task.train_color_mask_pairs)],
                                             task.test_color_mask_pairs,
                                             color_mask_pairs_list[len(task.train_color_mask_pairs):])


class ColorChannelMergeExecutor:

    @staticmethod
    def execute(task: ColorChannelMaskConvertedTask, merge_operation: ChannelMergeOperation) -> Task:
        new_arrays = OperationSetExecutor.apply_channel_merge([io.input_arr for io in task.train + task.test],
                                                              task.train_original_color_mask_pairs + task.test_original_color_mask_pairs,
                                                              task.train_color_mask_pairs + task.test_color_mask_pairs,
                                                              merge_operation)

        train_io = tuple([InputOutput(n, io.output_arr) for n, io, in zip(new_arrays[:len(task.train)], task.train)])
        test_io = tuple([InputOutput(n, io.output_arr) for n, io, in zip(new_arrays[len(task.train):], task.train)])

        return Task(task.name, train_io, test_io)


class PartitionSelectionExecutor:

    @staticmethod
    def execute(task: Task, partition_selection: PartitionSelection) -> PartitionSelectionTask:
        array_mask_list = OperationSetExecutor.apply_partition_selection([io.input_arr for io in task.train + task.test], partition_selection)

        return PartitionSelectionTask(task.name, task.train, task.test, array_mask_list[:len(task.train)], array_mask_list[len(task.train):])


class PartitionMergeExecutor:

    @staticmethod
    def execute(task: PartitionSelectionTask, partition_merge_operation: PartitionMergeOperation) -> Task:
        new_arrays = OperationSetExecutor.apply_partition_merge_operation([io.input_arr for io in task.train + task.test],
                                                                          task.train_partitioned_arrays_original_location_masks + task.test_partitioned_arrays_original_location_masks,
                                                                          partition_merge_operation)

        train_io = tuple([InputOutput(n, io.output_arr) for n, io, in zip(new_arrays[:len(task.train)], task.train)])
        test_io = tuple([InputOutput(n, io.output_arr) for n, io, in zip(new_arrays[len(task.train):], task.train)])

        return Task(task.name, train_io, test_io)
