from collections import Counter

import numpy as np
from dataclasses import dataclass
from typing import Optional, List, Union, Tuple

from abstraction_and_reasoning_challenge.src.answer_storage.answer_storage import AnswerStorageElement
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.task.task import Task


@dataclass
class AnsweredSearchResult:
    operation_set: OperationSet
    test_output_arr: Tuple[np.ndarray] = None
    test_correct: Optional[bool] = None


@dataclass
class AnsweredSearchResults:
    task: Task
    results: List[AnsweredSearchResult]
    zero_depth_search_time: float
    spent_time: float
    searched_total_node: int

    def summary(self):
        summary_elements = [f'{self.task.name}_{i}, '
                            f'correct: {str(r.test_correct):>5}, '
                            f'node: {self.searched_total_node:>6}, ' \
                            f'zero_depth_sec: {int(self.zero_depth_search_time):>5}, sec: {int(self.spent_time):>5}, '
                            f'depth: {len(r.operation_set.operations)}, operation_set: {r.operation_set}'
                            for i, r in enumerate(self.results)]

        return '\n'.join(summary_elements)

    def final_test_correct(self):
        return any(map(lambda r: r.test_correct, self.results))

    def to_answer_storage_elements(self) -> List[AnswerStorageElement]:
        return [AnswerStorageElement(self.task.name, r.test_correct, len(r.operation_set.operations), r.operation_set) for r in self.results]


@dataclass
class NotAnsweredSearchResult:
    task: Task
    exception: Exception
    spent_time: float
    searched_total_node: int

    def final_test_correct(self):
        return None

    def summary(self):
        return f'{self.task.name}__, ' \
               f'correct:  None, ' \
               f'node: {self.searched_total_node:>6}, sec: {int(self.spent_time):>5}, ' \
               f'exception: {self.exception.__class__.__name__}'


def summary_engine_results(results: List[Union[AnsweredSearchResults, NotAnsweredSearchResult]]):
    if len(results) == 0:
        return '0 result'
    counts = Counter(r.final_test_correct() for r in results)
    total_spent_time = np.sum([r.spent_time for r in results]) / 60
    mean_spent_time = np.sum([r.spent_time for r in results]) / len(results)
    max_spent_time = np.max([r.spent_time for r in results])

    result_message = f'--- stats --- \n' \
                     f'correct_count: {counts} \n' \
                     f'total_spent_time: {total_spent_time} min \n' \
                     f'mean_spent_time: {mean_spent_time} sec \n' \
                     f'max_spent_time: {max_spent_time} sec \n\n'

    result_message += '--- answered --- \n'
    result_message += '\n'.join(r.summary() for r in results if isinstance(r, AnsweredSearchResults))
    result_message += '\n--- all --- \n'
    result_message += '\n'.join(r.summary() for r in results)

    return result_message
