import numpy as np
from dataclasses import dataclass
from itertools import chain, product
from typing import List, Optional, Tuple

from abstraction_and_reasoning_challenge.src.domain.operation.util.enums import Color


@dataclass()
class InputOutput:
    input_arr: np.ndarray
    output_arr: Optional[np.ndarray]

    @staticmethod
    def of(json_dict: dict) -> 'InputOutput':
        return InputOutput(np.array(json_dict['input'], dtype=np.uint8),
                           np.array(json_dict['output'], dtype=np.uint8) if 'output' in json_dict else None)

    def get_all_arr(self) -> List[np.ndarray]:
        if self.output_arr is None:
            return [self.input_arr]
        else:
            return [self.input_arr, self.output_arr]

    def candidate_color_mapping(self) -> List[Tuple[Color, Color]]:
        input_colors = list(np.unique(self.input_arr)) + [Color.ANY_WITHOUT_MOST, Color.MOST, Color.SECOND_MOST, Color.LEAST]
        output_colors = np.unique(self.output_arr)
        return [(Color.of(i), Color.of(o)) for i, o in product(input_colors, output_colors) if i != o]


@dataclass(frozen=True)
class Task:
    name: str
    train: Tuple[InputOutput]
    test: Tuple[InputOutput]

    @staticmethod
    def of(name: str, json_dict: dict) -> 'Task':
        return Task(name,
                    tuple(InputOutput.of(io) for io in json_dict['train']),
                    tuple(InputOutput.of(io) for io in json_dict['test']))

    def get_all_arr(self) -> List[np.ndarray]:
        return self.get_train_all_arr() + self.get_test_all_arr()

    def get_train_all_arr(self) -> List[np.ndarray]:
        return list(chain.from_iterable(map(lambda io: io.get_all_arr(), self.train)))

    def get_test_all_arr(self) -> List[np.ndarray]:
        return list(chain.from_iterable(map(lambda io: io.get_all_arr(), self.test)))

    def get_input_all_arr(self) -> List[np.ndarray]:
        return list(map(lambda io: io.input_arr, self.train + self.test))

    def get_output_all_arr(self) -> List[np.ndarray]:
        return list(filter(lambda arr: arr is not None, map(lambda io: io.output_arr, self.train + self.test)))

    def test_arr_hash(self) -> int:
        return hash(self.__class__.__name__ +
                    '_'.join(map(lambda io: str(io.input_arr), self.test)))


@dataclass(frozen=True)
class ColorSelectedTask(Task):
    train_masks: List[np.ndarray]
    test_masks: List[np.ndarray]


@dataclass(frozen=True)
class MaskConvertedTask(Task):
    train_masks: List[np.ndarray]
    test_masks: List[np.ndarray]


@dataclass(frozen=True)
class ColorChannelSelectedTask(Task):
    train_color_mask_pairs: List[List[Tuple[Color, np.ndarray]]]
    test_color_mask_pairs: List[List[Tuple[Color, np.ndarray]]]


@dataclass(frozen=True)
class ColorChannelMaskConvertedTask(Task):
    train_original_color_mask_pairs: List[List[Tuple[Color, np.ndarray]]]
    train_color_mask_pairs: List[List[Tuple[Color, np.ndarray]]]
    test_original_color_mask_pairs: List[List[Tuple[Color, np.ndarray]]]
    test_color_mask_pairs: List[List[Tuple[Color, np.ndarray]]]


@dataclass(frozen=True)
class PartitionSelectionTask(Task):
    train_partitioned_arrays_original_location_masks: List[Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]]
    test_partitioned_arrays_original_location_masks: List[Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]]
