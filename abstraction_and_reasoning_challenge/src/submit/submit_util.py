import numpy as np
from pandas import DataFrame
from typing import List, Union

from abstraction_and_reasoning_challenge.config import RunConfig, PathConfig, KernelPathConfig
from abstraction_and_reasoning_challenge.src.domain.task.task_result import NotAnsweredSearchResult, AnsweredSearchResults
from abstraction_and_reasoning_challenge.src.enums import RunMode


def create_submission(engine_results: List[Union[AnsweredSearchResults, NotAnsweredSearchResult]]):
    submission_df = DataFrame(columns=['output_id', 'output'])

    for result in engine_results:
        test_arr_num = len(result.task.test)
        for i in range(test_arr_num):
            if isinstance(result, AnsweredSearchResults):
                answers = [r.test_output_arr[i] for r in result.results]
                answers += [None for _ in range(3 - len(answers))]
            elif isinstance(result, NotAnsweredSearchResult):
                answers = [None] * 3
            else:
                raise NotImplementedError()
            output_str = ' '.join(map(lambda a: parse_str(a), answers)) + ' '
            d = {
                'output_id': f'{result.task.name}_{i}',
                'output': output_str
            }
            submission_df = submission_df.append([d])
    return submission_df


def parse_str(arr: np.ndarray) -> str:
    if arr is None:
        return '|0|'

    return '|' + '|'.join(map(lambda row: ''.join(str(v) for v in row), arr)) + '|'


def save_submission_df(submission_df: DataFrame):
    if RunConfig.RUN_MODE == RunMode.KERNEL:
        submission_df.to_csv(KernelPathConfig.SUBMISSION, index=False)
    else:
        PathConfig.OUTPUT_SUBMISSION.parent.mkdir(parents=True, exist_ok=True)
        submission_df.to_csv(PathConfig.OUTPUT_SUBMISSION, index=False)
