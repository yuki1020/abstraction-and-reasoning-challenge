import numpy as np
import pandas as pd
import shutil
import time
from typing import Iterable, Union, Optional, List

from abstraction_and_reasoning_challenge.config import RunConfig, PathConfig
from abstraction_and_reasoning_challenge.src.enums import RunMode


def setup_df_display_options():
    np.set_printoptions(threshold=10000)
    np.set_printoptions(linewidth=10000)
    pd.set_option('display.max_columns', 1000)
    pd.set_option('display.max_rows', 1000)
    pd.set_option('display.width', 800)
    pd.set_option('display.max_colwidth', 300)


class Timer:
    def __init__(self):
        pass

    def __enter__(self):
        self.start_sec = time.perf_counter()
        return self

    def second(self):
        return time.perf_counter() - self.start_sec

    def __exit__(self, *exc):
        return


def mean(values: List[float]) -> float:
    return sum(values) / len(values)


def nan_mean(val_iter: Iterable[Union[int, float]]) -> Optional[float]:
    nan_filtered = [v for v in val_iter if v is not None]
    if not nan_filtered:
        return None
    return mean(nan_filtered)


def initialize_path():
    if RunConfig.RUN_MODE in [RunMode.LOCAL_RUN_ALL, RunMode.LOCAL_RUN]:
        shutil.rmtree(PathConfig.WRONG_ANSWERS_ROOT, ignore_errors=True)
        PathConfig.OUTPUT_SUBMISSION.unlink() if PathConfig.OUTPUT_SUBMISSION.exists() else None
