from dataclasses import dataclass
from itertools import groupby
from typing import Set, List, Tuple

from abstraction_and_reasoning_challenge.config import PathConfig
from abstraction_and_reasoning_challenge.src.domain.exceptions import OperationInconsistencyException
from abstraction_and_reasoning_challenge.src.domain.operation.base import OperationSet
from abstraction_and_reasoning_challenge.src.domain.operation.util.str_to_operation_set_obj import str_to_AnswerStorageElement
from abstraction_and_reasoning_challenge.src.domain.task.answer_matcher import AnswerMatcher
from abstraction_and_reasoning_challenge.src.loader.task_loader import TaskLoader


@dataclass
class AnswerStorageElement:
    task_name: str
    correct: bool
    depth: int
    operation_set: OperationSet

    def __post_init__(self):
        self.depth = len(self.operation_set.operations)

    def validate(self):
        task = TaskLoader().get_task(self.task_name)
        try:
            a = AnswerMatcher.is_train_test_all_match_if_operated(task, self.operation_set)
            if a != self.correct:
                print(f'{self.task_name} correct inconsistency. {self.correct}_{a}')
                return False
        except OperationInconsistencyException as e:
            print(f'{self.task_name} OperationInconsistencyException')
            return False

        return True

    def __hash__(self):
        return hash(repr(self))


@dataclass
class AnswerStorage:
    elements: Set[AnswerStorageElement]

    def validate(self):
        self.elements = set(filter(lambda e: e.validate(), self.elements))

    def add(self, element: AnswerStorageElement):
        self.elements.add(element)

    def get_text(self) -> str:
        return '\n'.join(repr(e) for e in sorted(self.elements, key=lambda e: (e.task_name, not e.correct, e.depth)))

    def get_only_correct_answer_storage(self) -> 'AnswerStorage':
        return AnswerStorage({e for e in self.elements if e.correct})

    def get_task_grouped_elements(self) -> List[Tuple[str, List[AnswerStorageElement]]]:
        elements = list(self.elements)
        elements = sorted(elements, key=lambda e: e.task_name)
        return [(k, list(g)) for k, g in groupby(elements, key=lambda e: e.task_name)]


def load_answer_storage() -> AnswerStorage:
    if not PathConfig.OPERATION_ANSWER_STORAGE.exists():
        return AnswerStorage(set())

    elements: List[AnswerStorageElement] = []
    with open(str(PathConfig.OPERATION_ANSWER_STORAGE), mode='r', encoding='utf-8') as f:
        for l in f.readlines():
            try:
                elements.append(str_to_AnswerStorageElement(l))
            except:
                pass
    storage = AnswerStorage(set(elements))
    storage.validate()
    return storage


def save_answer_storage(storage: AnswerStorage):
    PathConfig.OPERATION_ANSWER_STORAGE.unlink()
    with open(str(PathConfig.OPERATION_ANSWER_STORAGE), mode='w', encoding='utf-8') as f:
        f.write(storage.get_text())


def update_answer_storage(elements: List[AnswerStorageElement], verbose: bool = False):
    if verbose:
        print('load_answer storage')
    storage = load_answer_storage()
    if verbose:
        print(storage.get_text())

        print('add answer storage')
    for e in elements:
        e.validate()
        storage.add(e)
    if verbose:
        print('save answer storage')
        print(storage.get_text())
    save_answer_storage(storage)
