import matplotlib.pyplot as plt
import numpy as np

np.random.seed(0)


# TODO GIVE UP implement.
#  This is just a poc for "SYMMETRY" or "REPEAT" pattern tasks.
#  If you're interested in this function, let me know. I'll translate it.


def run():
    # 簡単な信号の作成(正弦波 + ノイズ)
    N = 30  # サンプル数
    dt = 1  # サンプリング周期(sec)
    freq = 0.2  # 周波数(10Hz)
    amp = 1  # 振幅

    t = np.arange(0, N * dt, dt)  # 時間軸
    # f = amp * np.sin(2 * np.pi * freq * t) + np.random.randn(N) * 0.0001  # 信号
    f = np.array([0, 0, 1, 1, 0, 1] * 5)

    f[3] = 0
    f[10] = 1

    print(t)
    print(f)

    # 高速フーリエ変換(FFT)
    F = np.fft.fft(f)
    F_abs = np.abs(F)  # 複素数を絶対値に変換

    print(F_abs)
    F_abs_amp = F_abs / N * 2  # 振幅をもとの信号に揃える(交流成分2倍)
    F_abs_amp[0] = F_abs_amp[0] / 2  # 振幅をもとの信号に揃える(直流成分非2倍)

    print(F_abs)

    # 周波数軸のデータ作成
    fq = np.linspace(0, 1.0 / dt, N)  # 周波数軸　linspace(開始,終了,分割数)

    # フィルタリング②（振幅強度でカット）＊＊＊＊＊＊
    F3 = np.copy(F)  # FFT結果コピー
    ac = 0.2  # 振幅強度の閾値

    print('a')
    val = F_abs_amp[(F_abs_amp < ac)].sum()

    F3[(F_abs_amp < ac)] = 0  # 振幅が閾値未満はゼロにする（ノイズ除去）
    # F3[(F_abs_amp >= ac)] = F3[(F_abs_amp >= ac)] + val  # TODO ためしにやってみる

    F3_abs = np.abs(F3)  # 複素数を絶対値に変換
    F3_abs_amp = F3_abs / N * 2  # 交流成分はデータ数で割って2倍
    F3_abs_amp[0] = F3_abs_amp[0] / 2  # 直流成分（今回は扱わないけど）は2倍不要
    F3_ifft = np.fft.ifft(F3)  # IFFT
    F3_ifft_real = F3_ifft.real  # 実数部の取得

    # グラフ表示
    fig = plt.figure(figsize=(12, 12))

    # グラフ表示
    # オリジナル信号
    fig.add_subplot(321)
    plt.xlabel('time(sec)', fontsize=14)
    plt.ylabel('signal', fontsize=14)
    plt.plot(t, f)

    # オリジナル信号 ->FFT
    fig.add_subplot(322)
    plt.xlabel('freqency(Hz)', fontsize=14)
    plt.ylabel('amplitude', fontsize=14)
    plt.plot(fq, F_abs_amp)

    # オリジナル信号 ->FFT ->振幅強度filter ->IFFT
    fig.add_subplot(325)
    plt.xlabel('time(sec)', fontsize=14)
    plt.ylabel('signal(amp.filter)', fontsize=14)
    plt.plot(t, F3_ifft_real)

    # オリジナル信号 ->FFT ->振幅強度filter
    fig.add_subplot(326)
    plt.xlabel('freqency(Hz)', fontsize=14)
    plt.ylabel('amplitude(amp.filter)', fontsize=14)
    plt.plot(fq, F3_abs_amp)

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    run()
