from dataclasses import dataclass
from typing import Optional


@dataclass
class DistanceEvaluatorParameter:
    same_h_w_dim_between_input_output: float = 1500
    all_dim_h_w_integer_multiple: float = 650
    mean_lack_color_num: float = 30
    mean_excess_color_num: float = 50
    mean_hit_and_miss_histogram_diff: float = 50
    mean_h_v_diff_input_arr_line_num: float = 40
    mean_h_v_diff_output_arr_line_num: float = 60
    mean_h_v_edge_sum_diff: float = 2
    mean_h_v_edge_sum_diff_ratio: float = 0.5
    mean_diff_color_cell_ratio: int = 1  # 基準
    mean_diff_cell_where_no_need_to_change_count_ratio: float = 100
    mean_wrong_change_cell_where_need_to_change_count_ratio: float = 100


@dataclass
class NodeBaseSearchEngineParameter:
    breadth_first_cost: float = 3500
    normal_first_cost: float = 400
    depth_first_cost: float = 1.2
    breadth_first_exp_cost: float = 0
    normal_exp_cost: float = 0
    depth_first_exp_cost: float = 0
    element_inclusion_prob_factor: float = 0
    pq_pop_mins_or_as_least_n: int = 20


@dataclass
class TreeBaseSearchEngineParameter:
    population_num: int = 26
    max_depth: int = 8
    operation_mutation_prob: float = 0.19
    operation_component_mutation_prob: float = 0.1
    operation_param_mutation_prob: float = 0.0048
    extend_mutation_prob: float = 0.044
    shrink_mutation_prob: float = 0.0012


@dataclass
class AllParameter:
    distance_evaluator_param: DistanceEvaluatorParameter = DistanceEvaluatorParameter()
    node_base_engine_param: Optional[NodeBaseSearchEngineParameter] = NodeBaseSearchEngineParameter()
    tree_base_engine_param: Optional[TreeBaseSearchEngineParameter] = TreeBaseSearchEngineParameter()
